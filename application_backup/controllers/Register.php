<?php
class Register extends CI_Controller{
	
	function __construct(){
		parent::__construct();
		$this->load->model('M_login');
		$this->load->model('M_user','m_user');
	}

	function index(){
		$this->load->view('frontend/register_view');
	}
	function verifyEmail(){
		$this->load->view('frontend/emailverify_view');
	}

	function check_otp(){
		$kodeOTP=htmlspecialchars($this->input->post('xotp',TRUE),ENT_QUOTES);
		$checkedOTP = $this->m_user->check_otp($kodeOTP);

		if($checkedOTP === TRUE){
			$this->m_user->update_status_user($kodeOTP);
			echo $this->session->set_flashdata("msg","<div class='alert alert-info'>Sukses, Registrasi berhasil silahkan login akun</div>");
			redirect('login');
		}else{
			echo $this->session->set_flashdata("msg","<div class='alert alert-danger'>Gagal, Kode OTP tidak sesuai</div>");
			redirect('register/verifyEmail');
		}
	}
	function register(){
		$nama=htmlspecialchars($this->input->post('name',TRUE),ENT_QUOTES);
		$gender=htmlspecialchars($this->input->post('gender',TRUE),ENT_QUOTES);
		$telp=htmlspecialchars($this->input->post('phone',TRUE),ENT_QUOTES);
		$email=htmlspecialchars($this->input->post('email',TRUE),ENT_QUOTES);
		$password1=htmlspecialchars($this->input->post('pass1',TRUE),ENT_QUOTES);
		$password2=htmlspecialchars($this->input->post('pass2',TRUE),ENT_QUOTES);

		$checkEmail = $this->m_user->checkEmail($email);
		if($checkEmail === TRUE){
			echo $this->session->set_flashdata("msg","<div class='alert alert-danger'>Email has been taken, try another email</div>");
			redirect('register');
		}else{
			if($password2 != $password1){
				echo $this->session->set_flashdata("msg","<div class='alert alert-danger'>You must input same passwords</div>");
				redirect('register');
			}else{
				$userid=rand(123456,999999);
				$otp=rand(123456,999999);
				$config = Array(
					'protocol' => 'smtp',
					'smtp_host' => 'ssl://mail.potrello.id',
					'smtp_port' => 465,
					'smtp_user' => 'otpcode.email@potrello.id',
					'smtp_pass' => 'email123123',
					'mailtype'  => 'html',
					'charset' => 'utf-8',);
					$this->load->library('email');
					$this->email->initialize($config);
					$this->email->from('otpcode.email@potrello.id'.'Potrello');
					$this->email->to($email);
					$this->email->subject("Verifikasi Email");
					$this->email->message("
					<html>
						<head>
							<title>Verifikasi Email Potrello</title>
						</head>
						<body>
						<h2>HATI HATI PENIPUAN, JANGAN SEBARKAN KODE OTP KE SIAPAPUN</h2>
						<p>Dear <b>$nama</b> berikut adalah kode OTP anda : <b>$otp</b> </p>
						
						<p>This email is sent automatically sent through the system, it is expected not to reply to this email, for further questions, please contact our customer service, Thankyou</p>
						</body>
					</html>
					");
				// if($this->email->send()){
					$this->session->set_userdata('email', $email);
					$data=array(
					'user_id' => $userid,
					'user_nama' => $nama,
					'user_gender' => $gender,
					'user_telp' => $telp,
					'user_email' => $email,
					'user_password' => md5($password1),
					'user_otp' => $otp);
				$OtpCode=array(
					'register_user_email' => $email,
					'register_otp_code' => $otp);
				$this->db->insert('user',$data);
				$this->db->insert('register',$OtpCode);
				echo $this->session->set_flashdata("msg","<div class='alert alert-info'>Kode OTP dikirimkan ke email anda</div>");
				redirect('register/verifyEmail');
				// }else{
				// 	echo $this->session->set_flashdata("msg","<div class='alert alert-info'>Register Gagal! Pastikan koneksi internet anda stabil</div>");
				// 	redirect('register');
				// }				
			}
			
		}
		
	}
	function kirim_ulangOTP(){
		$email=htmlspecialchars($this->input->post('xemail',TRUE),ENT_QUOTES);

				$otp=rand(123456,999999);

				$config = Array(
					'protocol' => 'smtp',
					'smtp_host' => 'ssl://mail.potrello.id',
					'smtp_port' => 465,
					'smtp_user' => 'otpcode.email@potrello.id',
					'smtp_pass' => 'email123123',
					'mailtype'  => 'html',
					'charset' => 'utf-8',);
					$this->load->library('email');
					$this->email->initialize($config);
					$this->email->from('otpcode.email@potrello.id','Potrello');
					$this->email->to($email);
					$this->email->subject("Verifikasi Email Kode OTP Potrello");
					$this->email->message("
					<html>
						<head>
							<title>Verifikasi Email Potrello</title>
						</head>
						<body>
						<h2>HATI HATI PENIPUAN, JANGAN SEBARKAN KODE OTP KE SIAPAPUN</h2>
						<p>Dear <b>$nama</b> berikut adalah kode OTP anda : <b>$otp</b> </p>
						<p>Jika anda belum memasang password, gunakan kode OTP sebagai Password anda</p>

						<p>This email is sent automatically sent through the system, it is expected not to reply to this email, for further questions, please contact our customer service, Thankyou</p>
						</body>
					</html>
					");

				if($this->email->send()){
					$data = array(
						'user_otp' => $otp,
						'user_password' => md5($otp)
					);	
					$this->db->where('user_email', $email);
					$this->db->update('user', $data);
			
					$OtpCode=array(
						'register_user_email' => $email,
						'register_otp_code' => $otp);
					$this->db->insert('register',$OtpCode);
					
					
					$this->session->set_userdata('email', $email);
					echo $this->session->set_flashdata("msg","<div class='alert alert-info'>Kode OTP dikirimkan ke email anda</div>");
					redirect('register/verifyEmail');
				}else{	
					echo $this->session->set_flashdata("msg","<div class='alert alert-info'>Kirim OTP Gagal! Pastikan koneksi internet anda stabil</div>");
					redirect('login');
				
				}

				
		}
	 }