<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login_FB extends CI_Controller {
    function __construct() {
        parent::__construct();
        
        
        //Load user model
        $this->load->model('M_user','m_user');
    }
    
    public function index(){
        $userData = array();
        
        // Check if user is logged in
        $this->facebook->login_url();


            // Get user facebook profile details
        $fbUser = $this->facebook->request('get', '/me?fields=id,first_name,last_name,email,link,gender,picture');
        
    }

    public function logout() {
        // Remove local Facebook session
        $this->facebook->destroy_session();
        // Remove user data from session
        $this->session->unset_userdata('userData');
        // Redirect to login page
        redirect('user_authentication');
    }
}