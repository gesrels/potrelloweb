<?php
class Login extends CI_Controller{
	
	function __construct(){
        parent::__construct();
        $this->load->model('M_loginuser','m_loginuser');
        $this->load->model('M_user','m_user');
        $this->load->library('Facebook');
	}

	function index(){
		$this->load->view('frontend/login_view');
	}
	function auth(){
        $email=htmlspecialchars($this->input->post('email',TRUE),ENT_QUOTES);
        $password=htmlspecialchars($this->input->post('pass',TRUE),ENT_QUOTES);
        $e=$email;
        $p=$password;
        $cuser=$this->m_loginuser->cekuser($e,$p);
        if($cuser->num_rows() > 0){
            $cverify = $this->m_loginuser->cekVerify($e,$p);
            if($cverify === TRUE){
                
                $otp=rand(123456,999999);

				$config = Array(
					'protocol' => 'smtp',
					'smtp_host' => 'ssl://mail.potrello.id',
					'smtp_port' => 465,
					'smtp_user' => 'otpcode.email@potrello.id',
					'smtp_pass' => 'email123123',
					'mailtype'  => 'html',
					'charset' => 'utf-8',);
					$this->load->library('email');
					$this->email->initialize($config);
					$this->email->from('otpcode.email@potrello.id','Potrello');
					$this->email->to($email);
					$this->email->subject("Verifikasi Email Kode OTP Potrello");
					$this->email->message("
					<html>
						<head>
							<title>Verifikasi Email Potrello</title>
						</head>
						<body>
						<h2>HATI HATI PENIPUAN, JANGAN SEBARKAN KODE OTP KE SIAPAPUN</h2>
						<p>Dear <b>$nama</b> berikut adalah kode OTP anda : <b>$otp</b> </p>

						<p>This email is sent automatically sent through the system, it is expected not to reply to this email, for further questions, please contact our customer service, Thankyou</p>
						</body>
					</html>
					");

				if($this->email->send()){
					$data = array(
						'user_otp' => $otp,
						'user_password' => md5($password)
					);	
					$this->db->where('user_email', $email);
					$this->db->update('user', $data);
					$OtpCode=array(
						'register_user_email' => $email,
						'register_otp_code' => $otp);
                    $this->db->insert('register',$OtpCode);
                    
                    $this->session->set_userdata('email',$email);
                    $this->session->set_userdata('password',$password);

					echo $this->session->set_flashdata("msg","<div class='alert alert-info'>Kode OTP dikirimkan ke email anda</div>");
					redirect('register/verifyEmail');
				}else{	
					echo $this->session->set_flashdata("msg","<div class='alert alert-danger'>Kirim OTP Gagal! Pastikan koneksi internet anda stabil</div>");
					redirect('login');
				}
            }else{
                $this->session->set_userdata('masuk',true);
                $this->session->set_userdata('email',$e);
                $xcuser=$cuser->row_array();
                $iduser=$xcuser['user_id'];
                $user_nama=$xcuser['user_nama'];
                $user_email=$xcuser['user_email'];
                $user_telp=$xcuser['user_telp'];
                $user_password=$xcuser['user_password'];
                $user_ratingToken=$xcuser['user_rating_token'];
                $user_intro=$xcuser['user_intro'];
                $user_otp=$xcuser['user_otp'];

                $this->session->set_userdata('iduser',$iduser);
                $this->session->set_userdata('email',$user_email);
                $this->session->set_userdata('nama',$user_nama);
                $this->session->set_userdata('telp',$user_telp);
                $this->session->set_userdata('password',$user_password);
                $this->session->set_userdata('rating',$user_ratingToken);
                $this->session->set_userdata('isIntro',$user_intro);
                $this->session->set_userdata('saldo',$user_saldo);


                if($user_password === md5($user_otp)){
                    $this->session->set_userdata('passOTP','1');
                }else{
                    $this->session->set_userdata('passOTP','0');
                }
                redirect('login/berhasillogin');
            }
        }
        redirect('login/gagallogin');
        
        
    }
    function berhasillogin(){
        redirect('user/home');
    }
    function underFix(){
        $this->load->view('frontend/underFix_view');
    }
    function gagallogin(){
        $url=base_url('login');
        echo $this->session->set_flashdata('msg','<div class="alert alert-warning" role="alert"><button type="button" class="close" data-dismiss="alert"><i class="fa fa-close"></i></button> Email atau Password salah! Silahkan coba lagi</div>');
        redirect($url);
    }
    function logout(){
        $this->session->sess_destroy();
        $url=base_url('login');
        redirect($url);
    }

    function login_facebook(){
        // Check if user is logged in
        if($this->facebook->is_authenticated()){
            // Get user facebook profile details
            $fbUser = $this->facebook->request('get', '/me?fields=id,first_name,last_name,email,link,gender,picture');

            // Preparing data for database insertion
            $userData['oauth_provider'] = 'facebook';
            $userData['oauth_uid']    = !empty($fbUser['id'])?$fbUser['id']:'';;
            $userData['first_name']    = !empty($fbUser['first_name'])?$fbUser['first_name']:'';
            $userData['last_name']    = !empty($fbUser['last_name'])?$fbUser['last_name']:'';
            $userData['email']        = !empty($fbUser['email'])?$fbUser['email']:'';
            $userData['gender']        = !empty($fbUser['gender'])?$fbUser['gender']:'';
            
            // Insert or update user data
            $userID = $this->user->checkUser($userData);
            
            // Check user data insert or update status
            if(!empty($userID)){
                $data['userData'] = $userData;
                $this->session->set_userdata('userData', $userData);
            }else{
               $data['userData'] = array();
               $data['authUrl'] =  $this->facebook->login_url();
            }
            
            // Get logout URL
            $data['logoutURL'] = $this->facebook->logout_url();
        }else{
            // Get login URL
            $data['authURL'] =  $this->facebook->login_url();
        }
    }
}
        //     // Get user facebook profile details
        //     if($this->facebook->is_authenticated()){
        //     $fbUser = $this->facebook->request('get', '/me?fields=id,first_name,email,gender');

        //     // Preparing data for database insertion
        //     $user_nama    = !empty($fbUser['first_name'])?$fbUser['first_name']:'';            
        //     $user_email   = !empty($fbUser['email'])?$fbUser['email']:'';
        //     $user_gender  = !empty($fbUser['gender'])?$fbUser['gender']:'';
            
        //     $checkEmail = $this->m_user->checkEmail($user_email);
        //     if($checkEmail === TRUE){
        //         echo $this->session->set_flashdata("msg","<div class='alert alert-danger'>Email has been taken, try another email</div>");
        //         redirect('login');
        //     }else{
        //         $userid=rand(123456,999999);
		// 		$data=array(
		// 			'user_id' => $userid,
		// 			'user_nama' => $user_nama,
		// 			'user_gender' => $user_gender,
        //             'user_email' => $user_email
        //                         );
        //         $this->session->set_userdata('masuk',true);
        //         $this->session->set_userdata('iduser',$userid);
        //         $this->session->set_userdata('email',$user_email);
        //         $this->session->set_userdata('nama',$user_nama);
        //         $this->session->set_userdata('facebook',true);
        //     }
        // }
