<?php
class gopay extends CI_Controller{
	function __construct(){
		parent::__construct();
		if($this->session->userdata('masuk') !=TRUE){
            $url=base_url('administrator');
            redirect($url);
        };
		$this->load->model('M_gopay','m_gopay');
		$this->load->library('upload');
	}


	function index(){
		if($this->session->userdata('akses')=='1'){
			$x['data']=$this->m_gopay->get_all_gopay();
			$this->load->view('admin/v_gopay',$x);
		}else{
			redirect('administrator');
		}
	}

	function update_gopay(){
		if($this->session->userdata('akses')=='1'){
			$kode=strip_tags($this->input->post('xkode'));
			$gopay=strip_tags($this->input->post('xgopay2'));
			$string=preg_replace('/[^a-zA-Z0-9 \&%|{.}=,?!*()"-_+$@;<>\']/', '', $gopay);
			$trim=trim($string);
			$add_slash=strtolower(str_replace(" ", "-", $trim));
			$slug=$add_slash;

			$this->m_gopay->update_gopay($kode,$gopay,$slug);
			echo $this->session->set_flashdata('msg','info');
			redirect('admin/gopay');
		}else{
			redirect('administrator');
		}
	}
	function hapus_gopay(){
		if($this->session->userdata('akses')=='1'){
			$kode=strip_tags($this->input->post('kode'));
			$this->m_gopay->hapus_gopay($kode);
			echo $this->session->set_flashdata('msg','success-hapus');
			redirect('admin/gopay');
		}else{
			redirect('administrator');
		}
	}

}