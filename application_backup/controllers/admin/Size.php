<?php
class size extends CI_Controller{
	function __construct(){
		parent::__construct();
		if($this->session->userdata('masuk') !=TRUE){
            $url=base_url('administrator');
            redirect($url);
        };
		$this->load->model('M_size','m_size');
		$this->load->library('upload');
	}


	function index(){
		if($this->session->userdata('akses')=='1'){
			$x['data']=$this->m_size->get_all_size();
			$this->load->view('admin/v_size',$x);
		}else{
			redirect('administrator');
		}
	}

	function simpan_size(){
		if($this->session->userdata('akses')=='1'){
            $size=strip_tags($this->input->post('xsize'));
            $harga=strip_tags($this->input->post('xharga'));
            $ukuran=strip_tags($this->input->post('xukuran'));
            $keterangan=strip_tags($this->input->post('xketerangan'));

			$string=preg_replace('/[^a-zA-Z0-9 \&%|{.}=,?!*()"-_+$@;<>\']/', '', $size);
			$trim=trim($string);
			$add_slash=strtolower(str_replace(" ", "-", $trim));
			$slug=$add_slash;
			$this->m_size->simpan_size($size,$harga,$ukuran,$keterangan,$slug);
			echo $this->session->set_flashdata('msg','success');
			redirect('admin/size');
		}else{
			redirect('administrator');
		}
	}

	function update_size(){
		if($this->session->userdata('akses')=='1'){
			$kode=strip_tags($this->input->post('xkode'));
            $size=strip_tags($this->input->post('xsize2'));
            $harga=strip_tags($this->input->post('xharga'));
            $ukuran=strip_tags($this->input->post('xukuran'));
            $keterangan=strip_tags($this->input->post('xketerangan'));
			$string=preg_replace('/[^a-zA-Z0-9 \&%|{.}=,?!*()"-_+$@;<>\']/', '', $size);
			$trim=trim($string);
			$add_slash=strtolower(str_replace(" ", "-", $trim));
			$slug=$add_slash;

			$this->m_size->update_size($kode,$size,$harga,$ukuran,$keterangan,$slug);
			echo $this->session->set_flashdata('msg','info');
			redirect('admin/size');
		}else{
			redirect('administrator');
		}
	}
	function hapus_size(){
		if($this->session->userdata('akses')=='1'){
			$kode=strip_tags($this->input->post('kode'));
			$this->m_size->hapus_size($kode);
			echo $this->session->set_flashdata('msg','success-hapus');
			redirect('admin/size');
		}else{
			redirect('administrator');
		}
	}

}