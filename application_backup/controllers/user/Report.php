<?php
class report extends CI_Controller{
	
	function __construct(){
		parent::__construct();
        $this->load->model('M_pengunjung','m_pengunjung');
        $this->load->model('M_order','m_order');

        $this->m_pengunjung->count_visitor();
        $this->load->library('upload');
	}

	function index(){
        //$x['data']=$this->m_fasilities->get_all_falilitas();
        $x['order']=$this->m_order->get_order_byid_profile();
        $this->session->set_userdata('isIntro', '');
		$this->load->view('user/report_view',$x);
	}

	function kirim(){
		$nama=htmlspecialchars($this->input->post('name',TRUE),ENT_QUOTES);
		$email=htmlspecialchars($this->input->post('email',TRUE),ENT_QUOTES);
		$subject=htmlspecialchars($this->input->post('subject',TRUE),ENT_QUOTES);
        $pesan=htmlspecialchars($this->input->post('message',TRUE),ENT_QUOTES);
        
        $config['upload_path'] = './assets/images/'; //path folder
        $config['allowed_types'] = 'jpg|png|jpeg|bmp'; //type yang dapat diakses bisa anda sesuaikan
        $config['encrypt_name'] = TRUE; //nama yang terupload nantinya
        $this->upload->initialize($config);

        if ($this->upload->do_upload('userfile'))
        {     
            $fileData = $this->upload->data();
                $gambar = $fileData['file_name'];
                $data=array(
                    'inbox_nama' => $nama,
                    'inbox_email' => $email,
                    'inbox_subject' => $subject,
                    'inbox_pesan' => $pesan,
                    'inbox_img' => $gambar
                );
                $this->db->insert('inbox',$data);
                echo $this->session->set_flashdata('msg','<div class="alert alert-info" role="alert"><button type="button" class="close" data-dismiss="alert"><i class="fa fa-close"></i></button> Thank you! your report sent, we will contact you soon</div>');
                redirect('user/profile');
        }else{
            echo $this->session->set_flashdata('msg','<div class="alert alert-warning" role="alert"><button type="button" class="close" data-dismiss="alert"><i class="fa fa-close"></i></button>Oh snap! your image is not qualified!</div>');
            redirect('user/report');
        }
	}
}