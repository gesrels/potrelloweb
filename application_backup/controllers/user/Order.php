<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Order extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        if ($this->session->userdata('masuk') != true) {
            $url = base_url('login');
            redirect($url);
        };
        $this->load->model('M_pengunjung', 'm_pengunjung');
        $this->load->model('M_room', 'm_room');
        $this->load->model('M_order', 'm_order');
        $this->load->model('M_address', 'm_address');
        $this->load->model('M_courier', 'm_courier');
        $this->load->model('M_size', 'm_size');
        $this->load->model('M_notif', 'm_notif');
        $this->load->model('M_user', 'm_user');

        $this->m_pengunjung->count_visitor();
        $this->load->library('upload');
    }

    public function index()
    {
        $x['service'] = $this->m_room->get_active_room();
        $x['address'] = $this->m_address->get_address_byid();
        $x['courier'] = $this->m_courier->get_all_courier();
        $x['size'] = $this->m_size->get_all_size();
        $this->session->set_userdata('isIntro', '');
        $this->load->view('user/orderUser_view', $x);
    }

    public function simpan_order()
    {
        $config['upload_path'] = './assets/images/'; //path folder
        $config['allowed_types'] = 'jpg|png|jpeg|bmp'; //type yang dapat diakses bisa anda sesuaikan
        $config['encrypt_name'] = true; //nama yang terupload nantinya
        $this->upload->initialize($config);

        if ($this->upload->do_upload('userfile')) {
            $fileData = $this->upload->data();
            $gambar = $fileData['file_name'];
            // //Compress Image
            // $config['image_library']='gd2';
            // $config['source_image']='./assets/images/order/'.$gbr;
            // $config['create_thumb']= FALSE;
            // $config['maintain_ratio']= FALSE;
            // $config['quality']= '100%';
            // $config['width']= 700;
            // $config['height']= 700;
            // $config['new_image']= './assets/images/order/'.$gbr;
            // $this->load->library('image_lib', $config);
            // $this->image_lib->resize();
            $orderNumber = rand(123456, 999999);
            $service = strip_tags($this->input->post('xservice'));
            $size = strip_tags($this->input->post('xsize'));

            $text = strip_tags($this->input->post('xcalligraphy'));
            $deskripsi = strip_tags($this->input->post('xdeskripsi'));
            $courier = strip_tags($this->input->post('xcourier'));
            $alamat = strip_tags($this->input->post('xalamat'));

            $this->m_order->simpan_order($orderNumber, $service, $size, $gambar, $text, $deskripsi, $courier, $alamat);
            echo $this->session->set_flashdata('msg', '<div class="alert alert-info" role="alert"><button type="button" class="close" data-dismiss="alert"><i class="fa fa-close"></i></button>Berhasil! Pesanan anda akan segera kami proses, kami akan menghubungi anda</div>');
            redirect('user/profile');
        } else {
            echo $this->session->set_flashdata('msg', '<div class="alert alert-warning" role="alert"><button type="button" class="close" data-dismiss="alert"><i class="fa fa-close"></i></button>Gagal! Gambar tidak jelas</div>');
            redirect('user/order');
        }
    }
    public function update_order()
    {
        $config['upload_path'] = './assets/images/'; //path folder
        $config['allowed_types'] = 'jpg|png|jpeg|bmp'; //type yang dapat diakses bisa anda sesuaikan
        $config['encrypt_name'] = true; //nama yang terupload nantinya
        $this->upload->initialize($config);
        $kode = strip_tags($this->input->post('xkode'));
        $service = strip_tags($this->input->post('xservice'));
        $size = strip_tags($this->input->post('xsize'));
        $text = strip_tags($this->input->post('xcalligraphy'));
        $deskripsi = strip_tags($this->input->post('xdeskripsi'));
        $courier = strip_tags($this->input->post('xcourier'));
        $alamat = strip_tags($this->input->post('xalamat'));

        if ($this->upload->do_upload('filefoto2')) {
            $fileData = $this->upload->data();
            $gambar = $fileData['file_name'];
            // //Compress Image
            // $config['image_library']='gd2';
            // $config['source_image']='./assets/images/order/'.$gbr;
            // $config['create_thumb']= FALSE;
            // $config['maintain_ratio']= FALSE;
            // $config['quality']= '100%';
            // $config['width']= 700;
            // $config['height']= 700;
            // $config['new_image']= './assets/images/order/'.$gbr;
            // $this->load->library('image_lib', $config);
            // $this->image_lib->resize();
            $this->m_order->update_order($kode, $service, $size, $gambar, $text, $deskripsi, $courier, $alamat);
            echo $this->session->set_flashdata('msg', '<div class="alert alert-info" role="alert"><button type="button" class="close" data-dismiss="alert"><i class="fa fa-close"></i></button>Berhasil! Pesanan berhasil diperbarui</div>');
            redirect('user/profile');
        } else {
            $this->m_order->update_order_noimg($kode, $service, $size, $text, $deskripsi, $courier, $alamat);
            echo $this->session->set_flashdata('msg', '<div class="alert alert-info" role="alert"><button type="button" class="close" data-dismiss="alert"><i class="fa fa-close"></i></button>Berhasil! Pesanan berhasil diperbarui</div>');
            redirect('user/profile');
        }
    }
    public function cancel_order()
    {
        $kode = strip_tags($this->input->post('xkode'));
        $this->m_order->cancel_order($kode);
        echo $this->session->set_flashdata('msg', '<div class="alert alert-info" role="alert"><button type="button" class="close" data-dismiss="alert"><i class="fa fa-close"></i></button> Berhasil! anda membatalkan pesanan</div>');
        redirect('user/profile');
    }

    public function selesai_order()
    {
        $kode = strip_tags($this->input->post('xkode'));
        $userId = strip_tags($this->input->post('xuserId'));
        $orderNumber = strip_tags($this->input->post('xorderNumber'));
        $userEmail = strip_tags($this->input->post('xuserEmail'));

        $config = array(
            'protocol' => 'smtp',
            'smtp_host' => 'ssl://mail.potrello.id',
            'smtp_port' => 465,
            'smtp_user' => 'order@potrello.id',
            'smtp_pass' => 'email123123',
            'mailtype' => 'html',
            'charset' => 'utf-8');
        $this->load->library('email');
        $this->email->initialize($config);
        $this->email->from('order@potrello.id', 'Potrello');
        $this->email->to($userEmail);
        $this->email->subject("Pesanan $orderNumber Selesai");

        $this->email->message("
	<html>
	<body
			style='background-color:#e2e1e0;font-family: Open Sans, sans-serif;font-size:100%;font-weight:400;line-height:1.4;color:#000;'>
			<table
					style='max-width:670px;margin:50px auto 10px;background-color:#fff;padding:50px;-webkit-border-radius:3px;-moz-border-radius:3px;border-radius:3px;-webkit-box-shadow:0 1px 3px rgba(0,0,0,.12),0 1px 2px rgba(0,0,0,.24);-moz-box-shadow:0 1px 3px rgba(0,0,0,.12),0 1px 2px rgba(0,0,0,.24);box-shadow:0 1px 3px rgba(0,0,0,.12),0 1px 2px rgba(0,0,0,.24); border-top: solid 10px green;'>
					<thead>
							<tr>
									<th style='text-align:left;'>Potrello</th>
									<th style='text-align:right;font-weight:400;'></th>
							</tr>
					</thead>
					<tbody>
							<tr>
									<td style='height:35px;'></td>
							</tr>
							<tr>
									<td colspan='2' style='border: solid 1px #ddd; padding:10px 20px;'>
											<p style='font-size:14px;margin:0 0 6px 0;'><span
															style='font-weight:bold;display:inline-block;min-width:150px'>Status Pesanan
													</span><b style='color:green;font-weight:normal;margin:0'>SUKSES</b></p>
											<p style='font-size:14px;margin:0 0 6px 0;'><span
															style='font-weight:bold;display:inline-block;min-width:146px'>No Pesanan</span>
															$orderNumber</p>
									</td>
							</tr>
							<tr>
									<td style='height:35px;'></td>
							</tr>
							<tr>
									<td colspan='2' style='padding:15px;'>
											<p style='font-size:14px;margin:0;padding:10px;border:solid 1px #ddd;font-weight:bold;'>
													<span style='display:block;font-size:13px;font-weight:normal;'>

													Terima kasih telah melakukan transaksi dengan Potrello.id, kepuasan anda adalah prioritas kami,
													Kami sangat menantikan feedback / kritik dan saran anda.

													</span></p>
													<a href='http://potrello.id' style='background-color: #4CAF50;
															border: none;
															color: white;
															padding: 15px 32px;
															text-align: center;
															text-decoration: none;
															display: inline-block;
															width:200px;
															font-size: 16px;'>KRITIK & SARAN
													</a>
									</td>
							</tr>
					</tbody>
					<tfooter>
							<tr>
									<td colspan='2' style='font-size:14px;padding:50px 15px 0 15px;'>
											<strong style='display:block;margin:0 0 10px 0;'>Salam,</strong> Potrello<br> Jakarta Barat, DKI
											Jakarta, IND<br><br>
											<b>Phone:</b> 082123228250<br>
											<b>Email:</b> cs@potrello.id
									</td>
							</tr>
					</tfooter>
			</table>
	</body>
	</html>
	");
        if ($this->email->send()) {
            $this->m_notif->order_selesai_notif($userId, $orderNumber);
            $this->m_order->selesai_order($kode);
            $this->m_user->update_rating_token($userId);
            $this->session->set_userdata('rating', 'norate');
            echo $this->session->set_flashdata('msg', '<div class="alert alert-info" role="alert"><button type="button" class="close" data-dismiss="alert"><i class="fa fa-close"></i></button> Pesanan anda berhasil!.</div>');
            redirect('user/profile');
        } else {
            echo $this->session->set_flashdata('msg', '<div class="alert alert-danger" role="alert"><button type="button" class="close" data-dismiss="alert"><i class="fa fa-close"></i></button> Gagal! pastikan koneksi internet stabil.</div>');
            redirect('user/profile');

        }
    }

}
