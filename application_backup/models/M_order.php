<?php
class M_order extends CI_Model
{

    public function get_all_order()
    {
        $hsl = $this->db->query("SELECT * FROM tbl_order where order_status IN('CHECKING','CONFIRMED','PENGERJAAN','KIRIM') order by order_tgl DESC");
        return $hsl;
    }
    public function get_ongoingConfirmed_order()
    {
        $hsl = $this->db->query("SELECT * FROM tbl_order where order_status IN('CHECKING','CONFIRMED','PENGERJAAN','KIRIM') order by order_tgl DESC");
        return $hsl;
    }

    public function simpan_order($orderNumber, $service, $size, $gambar, $text, $deskripsi, $courier, $alamat)
    {
        $iduser = $this->session->userdata('iduser');
        $time = date("ymd");
        $orderId = rand(1234, 9999);
        $hsl = $this->db->query("INSERT INTO tbl_order(order_id, order_number,order_user_id,order_service_id, order_size_id, order_img, order_calligraphy, order_deskripsi, order_courier_id, order_address_id)
        VALUES ('$orderId','ORD$time$orderNumber','$iduser','$service','$size','$gambar','$text','$deskripsi','$courier','$alamat')");
        return $hsl;
    }
    public function simpan_order_register($orderNumber, $userid, $service, $size, $gambar, $text, $deskripsi, $courier, $alamat)
    {
        $time = date("ymd");
        $orderId = rand(1234, 9999);
        $hsl = $this->db->query("INSERT INTO tbl_order(order_id, order_number,order_user_id,order_service_id, order_size_id, order_img, order_calligraphy, order_deskripsi, order_courier_id, order_address_id)
        VALUES ('$orderId','ORD$time$orderNumber','$userid','$service','$size','$gambar','$text','$deskripsi','$courier','$alamat')");
        return $hsl;
    }
    public function update_profile_nopass($kode, $nama, $telp)
    {
        $hsl = $this->db->query("UPDATE user set user_nama='$nama', user_telp='$telp' where user_id='$kode'");
        return $hsl;
    }

    public function cancel_order($kode)
    {
        $iduser = $this->session->userdata('iduser');
        $hsl = $this->db->query("UPDATE tbl_order set order_status='CANCELED' where order_id='$kode' AND order_user_id='$iduser'");
        return $hsl;
    }
    public function confirm_order($kode)
    {
        $hsl = $this->db->query("UPDATE tbl_order set order_status='CONFIRMED', order_tglconfirm=NOW() where order_id='$kode'");
        return $hsl;
    }
    public function set_exp($kode)
    {
        $hsl = $this->db->query("UPDATE tbl_order set order_expdate=DATE_ADD(order_tglconfirm, INTERVAL 1 DAY) where order_id='$kode'");
        return $hsl;
    }
    public function update_order($kode, $service, $size, $gambar, $text, $deskripsi, $courier, $alamat)
    {
        $hsl = $this->db->query("UPDATE tbl_order set order_service_id='$service', order_size_id='$size', order_img='$gambar', order_calligraphy='$text',order_deskripsi='$deskripsi', order_courier_id='$courier', order_address_id='$alamat' where order_id='$kode'");
        return $hsl;
    }
    public function update_order_noimg($kode, $service, $size, $text, $deskripsi, $courier, $alamat)
    {
        $hsl = $this->db->query("UPDATE tbl_order set order_service_id='$service', order_size_id='$size', order_calligraphy='$text', order_deskripsi='$deskripsi', order_courier_id='$courier', order_address_id='$alamat' where order_id='$kode'");
        return $hsl;
    }
    public function get_order_byid_profile()
    {
        $iduser = $this->session->userdata('iduser');
        $hsl = $this->db->query("SELECT * from tbl_order where order_user_id='$iduser' AND order_status IN('CHECKING','CONFIRMED','PENGERJAAN','KIRIM') order by order_tgl DESC");
        return $hsl;
    }
    public function get_history_order_byid_profile()
    {
        $iduser = $this->session->userdata('iduser');
        $hsl = $this->db->query("SELECT * from tbl_order where order_user_id='$iduser' order by order_tgl DESC");
        return $hsl;
    }

    public function simpan_invoice($kode, $userId, $namaService, $serviceAmount, $namaSize, $sizeAmount, $namaOtherAmount, $otherAmount, $namaShipping, $shippingAmount, $saldoUser, $total)
    {
        $hsl = $this->db->query("INSERT INTO invoice (invoice_order_id, invoice_user_id, invoice_nama_service, invoice_service_amount, invoice_nama_size, invoice_size_amount, invoice_nama_otheramount, invoice_other_amount, invoice_nama_shipping, invoice_shipping_amount,invoice_user_saldo, invoice_total)
        VALUES('$kode','$userId','$namaService','$serviceAmount','$namaSize','$sizeAmount','$namaOtherAmount','$otherAmount','$namaShipping','$shippingAmount','$saldoUser','$total')");
        return $hsl;
    }

    public function bayar_invoice_sisasaldo($kode, $saldo)
    {
        $hsl = $this->db->query("UPDATE invoice set invoice_total=invoice_total-$saldo where invoice_order_id='$kode'");
    }

    public function get_invoice_by_userid()
    {
        $iduser = $this->session->userdata('iduser');
        $hsl = $this->db->query("SELECT * from invoice where invoice_user_id='$iduser'");
        return $hsl;
    }

    public function paid_invoice_order($kode, $total)
    {
        $date = date("Y-m-d h:i");
        $hsl = $this->db->query("UPDATE invoice set invoice_paid='$total', invoice_status='PAID', invoice_payment_date='$date' where invoice_order_id='$kode'");
        return $hsl;
    }

    public function paid_invoice($invoiceId, $amount)
    {
        $date = date("Y-m-d h:i");
        $hsl = $this->db->query("UPDATE invoice set invoice_paid='$amount', invoice_status='PAID', invoice_payment_date='$date' where invoice_id='$invoiceId'");
        return $hsl;
    }

    public function progress_order($kode)
    {
        $hsl = $this->db->query("UPDATE tbl_order set order_status='PENGERJAAN' where order_id='$kode'");
        return $hsl;
    }

    public function kirim_order($kode, $resi, $driver, $kontak)
    {
        $hsl = $this->db->query("UPDATE tbl_order set order_status='KIRIM', order_resi_kirim='$resi', order_pengirim='$driver | $kontak' where order_id='$kode'");
        return $hsl;
    }

    public function Editkirim_order($kode, $resi, $driver)
    {
        $hsl = $this->db->query("UPDATE tbl_order set order_resi_kirim='$resi', order_pengirim='$driver' where order_id='$kode'");
        return $hsl;
    }

    public function selesai_order($kode)
    {
        $hsl = $this->db->query("UPDATE tbl_order set order_status='SELESAI' where order_id='$kode'");
        return $hsl;
    }

}
