<?php
class M_notif extends CI_Model{

	function order_confirm_notif($userId,$orderNumber){
		$icon = 'fa fa-spinner fa-spin';
		$data=array(
			'notifikasi_user_id' => $userId,
			'notifikasi_isi' => "<i class='$icon'></i> Pesanan $orderNumber dikonfirmasi, menunggu pembayaran"
		);
		$this->db->insert('notifikasi',$data);
	}
	function order_lunas_notif($userId,$orderNumber){
		$icon = 'fa fa-check';
		$data=array(
			'notifikasi_user_id' => $userId,
			'notifikasi_isi' => "<i class='$icon'></i> Pembayaran diterima, pesanan $orderNumber dalam pengerjaan"
		);
		$this->db->insert('notifikasi',$data);
	}

	function order_kirim_notif($userId,$orderNumber){
		$icon = "fa fa-truck";
		$data=array(
			'notifikasi_user_id' => $userId,
			'notifikasi_isi' => "<i class='$icon'></i> Pesanan $orderNumber dalam proses pengiriman"
		);
		$this->db->insert('notifikasi',$data);
	}

	function order_Editkirim_notif($userId,$orderNumber){
		$icon = "fa fa-truck";
		$data=array(
			'notifikasi_user_id' => $userId,
			'notifikasi_isi' => "<i class='$icon'></i> Pesanan $orderNumber mengganti kurir pengirim, dalam proses pengiriman"
		);
		$this->db->insert('notifikasi',$data);
	}

	function order_selesai_notif($userId,$orderNumber){
		$icon = "fa fa-check";
		$data=array(
			'notifikasi_user_id' => $userId,
			'notifikasi_isi' => "<i class='$icon'></i> Pesanan $orderNumber sudah selesai"
		);
		$this->db->insert('notifikasi',$data);
	}

	function saldo_notif($userId,$bayarLebih){
		$icon = 'fa fa-money';
		$data=array(
			'notifikasi_user_id' => $userId,
			'notifikasi_isi' => "<i class='$icon'></i> Kelebihan pembayaran Rp. $bayarLebih, dicairkan ke saldo ELLO-PAY"
		);
		$this->db->insert('notifikasi',$data);
	}
	

}