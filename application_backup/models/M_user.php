<?php
class M_user extends CI_Model{

    function get_user_byid(){
      $iduser = $this->session->userdata('iduser');
      $hsl=$this->db->query("SELECT * FROM user where user_id='$iduser'");
      return $hsl;
    }
    function update_profile($kode,$nama, $telp, $pass1){
    	$hsl=$this->db->query("UPDATE user set user_nama='$nama', user_telp='$telp', user_password=md5('$pass1') where user_id='$kode'");
    	return $hsl;
    }
    function update_profile_nopass($kode,$nama, $telp){
    	$hsl=$this->db->query("UPDATE user set user_nama='$nama', user_telp='$telp' where user_id='$kode'");
    	return $hsl;
    }
    function update_rating_token($userId){
      $hsl=$this->db->query("UPDATE user set user_rating_token='norate' where user_id='$userId'");
    	return $hsl;
    }
    function checkEmail($email){
        $hsl=$this->db->query("SELECT * FROM user where user_email='$email'");
        if($hsl->num_rows() > 0){
          return TRUE;
        }else{
          return FALSE;
        }
    }

    function check_otp($kodeOTP){
      $hsl=$this->db->query("SELECT * FROM user where user_otp='$kodeOTP'");
      if($hsl->num_rows() > 0){
        return TRUE;
      }else{
        return FALSE;
      }
    }
    function update_status_user($kodeOTP){
      $hsl=$this->db->query("UPDATE user set user_status='VERIFIED' where user_otp='$kodeOTP'");
      return $hsl;
    }
    function simpan_saldo($userId,$bayarLebih){
      $hsl=$this->db->query("UPDATE user set user_saldo=$bayarLebih where user_id='$userId'");
      return $hsl;
    }
    function clear_saldo($userId){
      $hsl=$this->db->query("UPDATE user set user_saldo=0 where user_id='$userId'");
      return $hsl;
    }
    function kurang_saldo($userId,$sisaSaldoUser){
      $hsl=$this->db->query("UPDATE user set user_saldo=$sisaSaldoUser where user_id='$userId'");
      return $hsl;
    }

    function ubah_password($userId,$pass1){
      $hsl=$this->db->query("UPDATE user set user_password=md5('$pass1') where user_id='$userId'");
      return $hsl;
    }
  
}
