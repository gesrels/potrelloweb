
<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->

	<title>404</title>

	<!-- Google font -->
  <link href="https://fonts.googleapis.com/css?family=Maven+Pro:400,900" rel="stylesheet">
  <link href='http://fonts.googleapis.com/css?family=Hind:400,300,500,600%7cMontserrat:400,700' rel='stylesheet' type='text/css'>


	<!-- Custom stlylesheet -->
	<link type="text/css" rel="stylesheet" href="css/style.css" />

	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
		  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
		  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->
	
		<style>
		* {
  -webkit-box-sizing: border-box;
          box-sizing: border-box;
}

body {
  padding: 0;
  margin: 0;
}

#notfound {
  position: relative;
  height: 80vh;
}

#notfound .notfound {
  position: absolute;
  left: 50%;
  top: 50%;
  -webkit-transform: translate(-50%, -50%);
      -ms-transform: translate(-50%, -50%);
          transform: translate(-50%, -50%);
}

.notfound {
  max-width: 920px;
  width: 100%;
  line-height: 1.4;
  text-align: center;
  padding-left: 15px;
  padding-right: 15px;
}

.notfound .notfound-404 {
  position: absolute;
  height: 100px;
  top: 0;
  left: 50%;
  -webkit-transform: translateX(-50%);
      -ms-transform: translateX(-50%);
          transform: translateX(-50%);
  z-index: -1;
}

.notfound .notfound-404 h1 {
  font-family: 'Maven Pro', sans-serif;
  color: #ececec;
  font-weight: 900;
  font-size: 276px;
  margin: 0px;
  position: absolute;
  left: 50%;
  top: 50%;
  -webkit-transform: translate(-50%, -50%);
      -ms-transform: translate(-50%, -50%);
          transform: translate(-50%, -50%);
}

.notfound h2 {
  font-family: 'Maven Pro', sans-serif;
  font-size: 46px;
  color: #000;
  font-weight: 900;
  text-transform: uppercase;
  margin: 0px;
}

.notfound p {
  font-family: 'Maven Pro', sans-serif;
  font-size: 16px;
  color: #000;
  font-weight: 400;
  text-transform: uppercase;
  margin-top: 15px;
}

.notfound a {
  font-family: 'Maven Pro', sans-serif;
  font-size: 14px;
  text-decoration: none;
  text-transform: uppercase;
  background: #ccc;
  display: inline-block;
  padding: 16px 38px;
  border: 2px solid transparent;
  border-radius: 40px;
  color: #fff;
  font-weight: 400;
  -webkit-transition: 0.2s all;
  transition: 0.2s all;
}

.notfound a:hover {
  background-color: #fff;
  border-color: #189cf0;
  color: #189cf0;
}
#footer {
  background-color: #232323
}

#footer .footer_top {
  background: #2e2e2e url(../images/bg-footer-top.png) no-repeat;
  padding: 20px 0
}

#footer .footer_top .mailchimp h4 {
  display: inline-block;
  font-family: 'Montserrat';
  margin: 0;
  color: #fff;
  font-size: 25px;
  vertical-align: middle;
  text-transform: uppercase;
  font-weight: normal
}

#footer .footer_top .mailchimp h4:before {
  content: "\f003";
  border: 3px solid #e1bd85;
  padding: 10px;
  color: #e1bd85;
  margin-right: 20px;
  font-size: 18px;
  font-family: 'FontAwesome';
  -webkit-border-radius: 100%;
  -moz-border-radius: 100%;
  border-radius: 100%
}

#footer .footer_top .mailchimp .mailchimp-form {
  display: inline-block;
  vertical-align: middle;
  margin-left: 65px
}

#footer .footer_top .mailchimp .mailchimp-form .input-text {
  border: 2px solid #fff;
  background-color: transparent;
  color: #fff;
  padding: 5px 10px;
  display: inline-block;
  vertical-align: middle;
  line-height: 40px;
  width: 290px
}

#footer .footer_top .mailchimp .mailchimp-form .input-text::-webkit-input-placeholder {
  color: #fff
}

#footer .footer_top .mailchimp .mailchimp-form .input-text:-moz-placeholder {
  color: #fff
}

#footer .footer_top .mailchimp .mailchimp-form .input-text::-moz-placeholder {
  color: #fff
}

#footer .footer_top .mailchimp .mailchimp-form .input-text:-ms-input-placeholder {
  color: #fff
}

#footer .footer_top .mailchimp .mailchimp-form .awe-btn {
  vertical-align: middle;
  min-width: 135px;
  text-align: center;
  padding: 7px 10px;
  height: 40px;
  color: #fff;
  background-color: #e1bd85;
  font-size: 16px;
  text-transform: uppercase;
  margin-left: 10px
}

#footer .footer_top .mailchimp .mailchimp-form .awe-btn:hover {
  background: #fff;
  color: #e1bd85
}

#footer .footer_top .social .social-content {
  font-size: 0
}

#footer .footer_top .social .social-content a {
  font-size: 14px;
  display: inline-block;
  color: #fff;
  border: 2px solid #fff;
  line-height: 32px;
  width: 32px;
  height: 32px;
  margin-top: 5px;
  text-align: center;
  margin-right: 20px;
  font-size: 16px;
  -webkit-border-radius: 50%;
  -moz-border-radius: 50%;
  border-radius: 50%;
  -webkit-transition: all .3s ease;
  -moz-transition: all .3s ease;
  -ms-transition: all .3s ease;
  -o-transition: all .3s ease
}

#footer .footer_top .social .social-content a:hover {
  border-color: #e1bd85
}

#footer .footer_center {
  padding-bottom: 40px
}

#footer .footer_center p {
  font-size: 14px
}

#footer .footer_center .widget-logo {
  overflow: hidden;
  padding-top: 20px
}

#footer .footer_center .widget-logo .img {
  max-width: 145px;
  display: inline-block;
  vertical-align: middle;
  margin-right: 40px
}

#footer .footer_center .widget-logo .img img {
  max-width: 100%
}

#footer .footer_center .widget-logo .text {
  display: inline-block;
  vertical-align: middle;
  margin-top: 10px
}

#footer .footer_center .widget-logo .text p {
  color: #acacac;
  margin-bottom: 10px
}

#footer .footer_center .widget-logo .text p i {
  margin-right: 10px;
  font-size: 16px;
  color: #fff
}

#footer .footer_center .widget-logo .text p a {
  color: #acacac
}

#footer .footer_center .widget-logo .text p a:hover {
  color: #e1bd85;
  text-decoration: underline
}

#footer .footer_center .widget-logo .text p:last-child {
  margin-bottom: 0
}

#footer .footer_center .widget {
  margin-top: 40px
}

#footer .footer_center .widget .widget-title {
  color: #fff;
  text-transform: uppercase;
  font-size: 16px;
  margin-top: 0;
  margin-bottom: 20px
}

#footer .footer_center .widget>ul {
  margin-top: -5px
}

#footer .footer_center .widget ul {
  list-style: none;
  padding-left: 0;
  margin-bottom: 0
}

#footer .footer_center .widget ul li a {
  color: #acacac;
  padding: 5px 0;
  display: inline-block;
  -webkit-transition: all .2s ease;
  -moz-transition: all .2s ease;
  -ms-transition: all .2s ease;
  -o-transition: all .2s ease
}

#footer .footer_center .widget ul li a:hover {
  color: #fff
}

#footer .footer_center .widget ul li .sub-menu {
  margin-left: 15px
}

#footer .footer_center .widget_tripadvisor .tripadvisor p {
  color: #acacac;
  font-size: 13.75px;
  margin-bottom: 20px
}

#footer .footer_center .widget_tripadvisor .tripadvisor img {
  max-width: 100%;
  margin-bottom: 20px
}

#footer .footer_center .widget_tripadvisor .tripadvisor .tripadvisor-circle {
  font-size: 0;
  clear: bold;
  display: block
}

#footer .footer_center .widget_tripadvisor .tripadvisor .tripadvisor-circle i {
  font-size: 15px;
  border: 2px solid #8bc23f;
  padding: 1px;
  background-color: #8bc23f;
  background-clip: content-box;
  display: inline-block;
  width: 14px;
  height: 14px;
  margin-right: 5px;
  position: relative;
  -webkit-border-radius: 50%;
  -moz-border-radius: 50%;
  border-radius: 50%
}

#footer .footer_center .widget_tripadvisor .tripadvisor .tripadvisor-circle i.part:before {
  content: '';
  position: absolute;
  background-color: #1f232b;
  display: block;
  height: 8px;
  width: 4px;
  right: 1px;
  top: 1px
}

#footer .footer_bottom {
  background-color: #101010;
  padding: 10px 0;
  text-align: center;
  color: #acacac;
  font-size: 12.22px
}

#footer .footer_bottom p {
  margin-bottom: 0;
  font-size: 14px
}

@media only screen and (max-width: 480px) {
  .notfound .notfound-404 h1 {
    font-size: 162px;
  }
  .notfound h2 {
    font-size: 26px;
  }
}

		</style>

</head>

<body>

	<div id="notfound">
		<div class="notfound">
			<div class="notfound-404">
				<h1>404</h1>
			</div>
			<h2>Maaf, Halaman tidak ditemukan!</h2>
			<p>Halaman yang anda cari mungkin sudah diubah atau dihapus secara permanen.</p>
			<a href="http://localhost/potrello">Kembali ke Beranda</a>
		</div>
  </div>
  <footer id="footer">

            <!-- FOOTER BOTTOM -->
            <div class="footer_bottom">
                <div class="">
                    <p>&copy; <?php echo date('Y');?> All rights reserved | By <a href="http://instagram.com/gesrels" target="_blank" style="font-family:calibri;color: #fff;">Potrello</a> </p>
                </div>
            </div>
            <!-- END / FOOTER BOTTOM -->

        </footer>


</body><!-- This templates was made by Colorlib (https://colorlib.com) -->

</html>

