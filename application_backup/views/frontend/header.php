 <div class="header_content" id="header_content">

                <div class="container">
                    <!-- HEADER LOGO -->
                    <div class="header_logo">
                        <a href="<?php echo site_url('');?>"><img draggable="false" onContextMenu="return false;" src="<?php echo base_url().'theme/images/logo1.png'?>" alt="" ></a>
                    </div>
                    <!-- END / HEADER LOGO -->

                    <!-- HEADER MENU -->
                    <nav class="header_menu">
                        <ul class="menu">
                            <li><a href="<?php echo site_url('');?>">Beranda</a></li>
                            <li><a href="<?php echo site_url('OrderRegister');?>">Pesan</a></li>
                            <li><a href="<?php echo site_url('service');?>">Layanan</a></li>
                            <li><a href="<?php echo site_url('testimonials');?>">Testimoni</a></li>
                            <li><a href="<?php echo site_url('login');?>">Masuk</a></li>
                        </ul>
                    </nav>
                    <!-- END / HEADER MENU -->

                    <!-- MENU BAR -->
                    <span class="menu-bars">
                        <span>
                        </span>
                    </span>
                    <!-- END / MENU BAR -->

                </div>
            </div>
