<footer id="footer">

            <!-- FOOTER BOTTOM -->
            <div class="footer_bottom">
                <div class="container">
                    <p>&copy; <?php echo date('Y');?> All rights reserved | By <a href="http://instagram.com/gesrels" target="_blank" style="color: #fff;">Potrello</a> </p>
                </div>
            </div>
            <!-- END / FOOTER BOTTOM -->

        </footer>
