<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <!-- TITLE -->
    <title>Potrello | Lapor Pembayaran</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <meta name="format-detection" content="telephone=no">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <link rel="shortcut icon" href="<?php echo base_url().'theme/images/favicon.png'?>"/>
	<meta name="description" content="Hotel by Geysler">

	<!-- META FOR IOS & HANDHELD -->
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
	<meta name="HandheldFriendly" content="true" />
	<meta name="apple-mobile-web-app-capable" content="YES" />
	<!-- //META FOR IOS & HANDHELD -->

    <!-- GOOGLE FONT -->
    <link href='http://fonts.googleapis.com/css?family=Hind:400,300,500,600%7cMontserrat:400,700' rel='stylesheet' type='text/css'>


    <!-- CSS LIBRARY -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url().'theme/css/lib/font-awesome.min.css'?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url().'theme/css/lib/font-lotusicon.css'?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url().'theme/css/lib/bootstrap.min.css'?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url().'theme/css/lib/owl.carousel.css'?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url().'theme/css/lib/jquery-ui.min.css'?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url().'theme/css/lib/magnific-popup.css'?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url().'theme/css/lib/settings.css'?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url().'theme/css/lib/bootstrap-select.min.css'?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url().'theme/css/helper.css'?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url().'theme/css/custom.css'?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url().'theme/css/responsive.css'?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/css/dropify.min.css'?>">


    <!-- MAIN STYLE -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url().'theme/css/style.css'?>">

</head>

<body>


    <!-- PRELOADER -->
    <div id="preloader">
        <span class="preloader-dot"></span>
    </div>
    <!-- END / PRELOADER -->

    <!-- PAGE WRAP -->
    <div id="page-wrap">

        <!-- HEADER -->
        <header id="header" class="header-v2">

            <!-- HEADER TOP -->
           <?php $this->load->view('user/headertop');?>
            <!-- END / HEADER TOP -->

            <!-- HEADER LOGO & MENU -->
          <?php $this->load->view('user/header');?>

			<!-- END / HEADER LOGO & MENU -->

        </header>
        <!-- END / HEADER -->

        <!--BANNER -->
        <section class="section-sub-banner bg-9">
            <div></div>
            <div class="sub-banner">
                <div class="container">
                    <div class="text text-center">
                    </div>
                </div>

            </div>

        </section>
        <!-- END BANNER -->

        <!-- CONTACT -->
        <section class="section-contact">
            <div class="container">
                <div class="contact">
                    <div class="row">
                        <div class="col-md-6 col-lg-5">

                            <div class="text">
                                <h2>Lapor Pembayaran</h2>
                                <ul>
                                    <li><i class="icon fa fa-circle-o"></i> Pastikan anda sudah melakukan pembayaran</li>
                                    <li><i class="icon fa fa-circle-o"></i> Informasi pembayaran telah dikirimkan melalui email saat konfirmasi</li>
                                    <li><i class="icon fa fa-circle-o"></i> Pilihlah nomor order sesuai order yang telah anda bayarkan</li>
                                    <li><i class="icon fa fa-circle-o"></i> Masukan Jumlah pembayaran, Nama rekening pengirim</li>
                                    <li><i class="icon fa fa-circle-o"></i> Upload bukti pembayaran (SS E Banking atau Struk ATM</li>
                                </ul>
                            </div>
                        </div>

                        <div class="col-md-6 col-lg-6 col-lg-offset-1">
                            <div class="contact-form">
                                <form action="<?php echo site_url('user/report/kirim');?>" method="post" enctype="multipart/form-data">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <input type="text" class="field-text"  name="name" placeholder="Name" value="<?php echo $this->session->userdata('nama'); ?>" readonly required>
                                        </div>
                                        <div class="col-sm-6">
                                            <input type="email" class="field-text" name="email" placeholder="Email" value="<?php echo $this->session->userdata('email'); ?>" readonly required>
                                        </div>
                                        <div class="col-sm-12">
                                            <select style="height:40px;" class="field-text" name="subject" id="" required>
                                                <option value="">- Pilih No Pesanan -</option>
                                                <?php foreach($order->result() as $i): ?>
                                                    <option value="<?php echo $i->order_number;?>"><?php echo $i->order_number; ?> (<?php echo date("d M Y", strtotime($i->order_tgl));?>)</option>
                                                <?php endforeach; ?>
                                            </select>                                        
                                        </div>
                                        <div class="col-sm-12">
                                            <textarea cols="30" rows="10" name="message"  class="field-textarea" placeholder="Jumlah Pembayaran, Nama Rekening Pengirim" required></textarea>
                                        </div>
                                        <div class="col-sm-12">
                                        <label style="margin-top:20px;margin-bottom:-30px" for="">Upload Bukti Pembayaran</label>
                                            <input style="height:300px" type="file" name="userfile" class="dropify"
                                                data-height="250" required>                                        
                                        </div>
                                        <div class="col-sm-6">
                                            <button type="submit" class="awe-btn awe-btn-14">KIRIM</button>
                                        </div>
                                        
                                    </div>
                                    <div id="contact-content"><?php echo $this->session->flashdata('msg');?></div>
                                </form>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </section>
        <!-- END / CONTACT -->



        <!-- FOOTER -->
        <?php $this->load->view('user/footer');?>
        <!-- END / FOOTER -->

    </div>
    <!-- END / PAGE WRAP -->

    <script type="text/javascript" src="<?php echo base_url().'theme/js/lib/jquery-1.11.0.min.js'?>"></script>
    <script type="text/javascript" src="<?php echo base_url().'theme/js/lib/jquery-ui.min.js'?>"></script>
    <script type="text/javascript" src="<?php echo base_url().'theme/js/lib/bootstrap.min.js'?>"></script>
    <script type="text/javascript" src="<?php echo base_url().'theme/js/lib/bootstrap-select.js'?>"></script>
    <script type="text/javascript" src="<?php echo base_url().'theme/js/lib/isotope.pkgd.min.js'?>"></script>
    <script type="text/javascript" src="<?php echo base_url().'theme/js/lib/jquery.themepunch.revolution.min.js'?>"></script>
    <script type="text/javascript" src="<?php echo base_url().'theme/js/lib/jquery.themepunch.tools.min.js'?>"></script>
    <script type="text/javascript" src="<?php echo base_url().'theme/js/lib/owl.carousel.js'?>"></script>
    <script type="text/javascript" src="<?php echo base_url().'theme/js/lib/jquery.appear.min.js'?>"></script>
    <script type="text/javascript" src="<?php echo base_url().'theme/js/lib/jquery.countTo.js'?>"></script>
    <script type="text/javascript" src="<?php echo base_url().'theme/js/lib/jquery.countdown.min.js'?>"></script>
    <script type="text/javascript" src="<?php echo base_url().'theme/js/lib/jquery.parallax-1.1.3.js'?>"></script>
    <script type="text/javascript" src="<?php echo base_url().'theme/js/lib/jquery.magnific-popup.min.js'?>"></script>

    <script type="text/javascript" src="<?php echo base_url().'theme/js/lib/jquery.form.min.js'?>"></script>
    <script type="text/javascript" src="<?php echo base_url().'theme/js/lib/jquery.validate.min.js'?>"></script>

    <script type="text/javascript" src="<?php echo base_url().'theme/js/scripts.js'?>"></script>
    <script src="<?php echo base_url().'assets/js/dropify.min.js'?>"></script>
    <script type="text/javascript">
    $(document).ready(function() {
        $('.dropify').dropify({
            messages: {
                default: 'Image 900 X 400 Pixels',
                replace: 'Ganti',
                remove: 'Hapus',
                error: 'error'
            }
        });

        $('.dropify2').dropify({
            messages: {
                default: 'Image 700 X 700 Pixels',
                replace: 'Ganti',
                remove: 'Hapus',
                error: 'error'
            }
        });
        $('.dropify3').dropify({
            messages: {
                default: 'Image 420 X 420 Pixels',
                replace: 'Ganti',
                remove: 'Hapus',
                error: 'error'
            }
        });
    });
    </script>

</body>
</html>
