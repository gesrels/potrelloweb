<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <!-- TITLE -->
    <title>Potrello | Pesanan</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <meta name="format-detection" content="telephone=no">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <link rel="shortcut icon" href="<?php echo base_url().'theme/images/favicon.png'?>" />
    <meta name="description" content="Hotel by Geysler">

    <!-- META FOR IOS & HANDHELD -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta name="HandheldFriendly" content="true" />
    <meta name="apple-mobile-web-app-capable" content="YES" />
    <!-- //META FOR IOS & HANDHELD -->

    <!-- GOOGLE FONT -->
    <link href='http://fonts.googleapis.com/css?family=Hind:400,300,500,600%7cMontserrat:400,700' rel='stylesheet'
        type='text/css'>


    <!-- CSS LIBRARY -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url().'theme/css/lib/font-awesome.min.css'?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url().'theme/css/lib/font-lotusicon.css'?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url().'theme/css/lib/bootstrap.min.css'?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url().'theme/css/lib/owl.carousel.css'?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url().'theme/css/lib/jquery-ui.min.css'?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url().'theme/css/lib/magnific-popup.css'?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url().'theme/css/lib/settings.css'?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url().'theme/css/lib/bootstrap-select.min.css'?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url().'theme/css/helper.css'?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url().'theme/css/custom.css'?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url().'theme/css/responsive.css'?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/css/dropify.min.css'?>">


    <!-- MAIN STYLE -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url().'theme/css/style.css'?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url().'theme/css/modal.css'?>">
    <link rel="stylesheet" href="<?php echo base_url().'theme/css/jssocials.css'?>">
    <link rel="stylesheet" href="<?php echo base_url().'theme/css/jssocials-theme-flat.css'?>">

</head>

<body>


    <!-- PRELOADER -->
    <div id="preloader">
        <span class="preloader-dot"></span>
    </div>
    <!-- END / PRELOADER -->

    <!-- PAGE WRAP -->
    <div id="page-wrap">

        <!-- HEADER -->
        <header id="header" class="header-v2">

            <!-- HEADER TOP -->
            <?php $this->load->view('user/headertop');?>
            <!-- END / HEADER TOP -->

            <!-- HEADER LOGO & MENU -->
            <?php $this->load->view('user/header');?>

            <!-- END / HEADER LOGO & MENU -->

        </header>
        <!-- END / HEADER -->

        <!--BANNER -->
        <section class="section-sub-banner bg-9">
            <div></div>
            <div class="sub-banner">
                <div class="container">
                    <div class="text text-center">
                    </div>
                </div>

            </div>

        </section>
        <!-- END BANNER -->

        <!-- CONTACT -->
        <section class="section-contact">
            <div class="container">
                <div class="contact">
                    <div class="row">
                        <div class="col-md-6 col-lg-5">

                            <div class="text">

                                <h2>Ketentuan Pesan</h2>
                                <p>(Mohon
                                    untuk memperhatikan ketentuan di bawah sebelum melakukan pesanan sketsa wajah)</p>
                                <div style="align-text:justify">
                                    <li>Harga sudah include frame / bingkai standard warna putih + packing menggunakan
                                        karton padi + bubblewrap (packing aman)</li>
                                    <li>Tambahan wajah dalam 1 frame +50.000 per kepala (hanya untuk A3 & A4)</li>
                                    <li>Jika ada lebih dari 1 wajah dlm 1 frame maka lama pengerjaan diperkirakan +1
                                        sampai 2 hari</li>
                                    <li>Jasa pengiriman yang digunakan adalah JNE, J&T, & SiCepat</li>
                                    <li>Jika order / pesanan sudah dikonfirmasi maka pesanan tersebut sudah masuk ke
                                        tahap pembuatan dan pembeli dianggap sudah menyetujui ketentuan yang ada
                                        termasuk membayarkan cancellation fee jika melakukan pembatalan pesanan tersebut
                                    </li>
                                    <li>Biaya pembatalan / cancellation fee adalah sebesar 50% atau setengah harga dari
                                        harga pesanan</li>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-6 col-lg-6 col-lg-offset-1">
                            <div class="contact-form">
                                <form action="<?php echo site_url('user/order/simpan_order');?>" method="post"
                                    enctype="multipart/form-data">
                                    <div id="contact-content"><?php echo $this->session->flashdata('msg');?></div>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <select style="height:40px;" class="field-text" name="xservice" id=""
                                                required>
                                                <option value="">- Pilih Layanan -</option>
                                                <?php
                                                    foreach($service->result_array() as $i):
                                                    $service_id = $i['kd_compare'];
                                                    $service_nama = $i['type'];
                                                ?>
                                                <option value="<?php echo $service_id; ?>"><?php echo $service_nama; ?>
                                                </option>
                                                <?php endforeach; ?>
                                            </select>
                                            <a class="btn btn-xs btn-secondary btn-info" id="service"
                                                style="margin-top:5px">Rincian <i
                                                    class="fa fa-chevron-circle-up"></i></a>
                                        </div>
                                        <div class="col-sm-6">
                                            <select style="height:40px;" class="field-text" name="xsize" id="" required>
                                                <option value="">- Pilih Ukuran -</option>
                                                <?php foreach($size->result() as $i): ?>
                                                <option value="<?php echo $i->size_id;?>"><?php echo $i->size_nama;?>
                                                    (<?php echo $i->size_ukuran;?>)</option>
                                                <?php endforeach; ?>
                                            </select>
                                            <a class="btn btn-xs btn-secondary btn-info" id="size"
                                                style="margin-top:5px">Rician <i
                                                    class="fa fa-chevron-circle-up"></i></a>
                                        </div>

                                        <div class="col-sm-12">
                                            <label style="margin-top:20px;margin-bottom:-30px" for="">Upload Contoh
                                                Gambar</label>
                                            <input style="height:300px" type="file" name="userfile" class="dropify1"
                                                data-height="250" required>
                                        </div>
                                        <div class="col-sm-12">
                                            <input type="text" class="field-text" name="xcalligraphy"
                                                placeholder="Kaligrafi (Opsional)">
                                            <a class="btn btn-xs btn-secondary btn-info" id="text"
                                                style="margin-top:5px">Rincian <i
                                                    class="fa fa-chevron-circle-up"></i></a>

                                        </div>
                                        <div class="col-sm-12">
                                            <textarea type="text" class="field-text" name="xdeskripsi"
                                                placeholder="Keterangan tambahan (Opsional)"></textarea>
                                        </div>
                                        <div class="col-sm-6">
                                            <select style="height:40px;" class="field-text" name="xcourier" id=""
                                                required>
                                                <option value="">- Pilih Kurir -</option>
                                                <?php foreach($courier->result() as $i): ?>
                                                <option value="<?php echo $i->courier_id;?>">
                                                    <?php echo $i->courier_nama; ?></option>
                                                <?php endforeach; ?>
                                            </select>
                                            <a class="btn btn-xs btn-secondary btn-info" id="courier"
                                                style="margin-top:5px">Rincian <i
                                                    class="fa fa-chevron-circle-up"></i></a>

                                        </div>
                                        <div class="col-sm-12">
                                            <select style="height:40px;" class="field-text" name="xalamat" id=""
                                                required>
                                                <option value="">- Pilih Alamat -</option>
                                                <?php foreach($address->result_array() as $i):
                                                      $address_id = $i['address_id'];
                                                      $address_nama = $i['address_nama'];
                                                      $address_alamat = $i['address_alamat'];
                                                    ?>
                                                <option value="<?php echo $address_id; ?>">
                                                    (<?php echo $address_nama; ?>) <?php echo $address_alamat; ?>
                                                </option>
                                                <?php endforeach; ?>
                                            </select>
                                            
                                            <?php if($address->num_rows() === 0): ?>
                                            <a class="btn btn-xs btn-secondary btn-info" data-toggle="modal"
                                                data-target="#ModalAddressAdd" style="margin-top:5px">Tambah Alamat <i
                                                    class="fa fa-chevron-circle-left"></i></a>
                                            <?php else: ?>
                                            <a class="btn btn-xs btn-secondary btn-info" data-toggle="modal"
                                                data-target="#DataAddress" style="margin-top:5px">Konfigurasi Alamat <i
                                                    class="fa fa-chevron-circle-up"></i></a>
                                            <?php endif; ?>
                                        </div>

                                        <div class="col-sm-6">
                                            <button type="submit" class="awe-btn awe-btn-14">PESAN</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </section>
        <!-- END / CONTACT -->



        <!-- FOOTER -->
        <?php $this->load->view('frontend/footer');?>
        <!-- END / FOOTER -->

        <!-- MODALS -->
        <!-- Modal Service -->
        <div class="modal" id="ModalService" tabindex="-1" role="dialog" aria-labelledby="modal-normal"
            aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="block block-themed block-transparent mb-0" style="padding:20px">
                        <div class="block-header bg-info-dark">
                            <h3 class="block-title">Layanan</h3>
                        </div>
                        <div class="block-content">
                            <!-- ITEM -->
                            <?php foreach ($service->result() as $row):?>
                            <div class="room_item-3 thumbs-left">
                                <div class="img">
                                    <img src="<?php echo base_url(). 'assets/images/'.$row->gambar_large;?>" alt="">
                                </div>
                                <div class="text-thumbs">
                                    <div class="text">
                                        <h2><a href="#"><?php echo $row->type;?></a></h2>
                                        <small><span class="price">Mulai dari <span
                                                    class="amout"><b><?php echo 'Rp '.number_format($row->rate);?></b>
                                                    <?php echo $row->detail;?></small>
                                    </div>
                                </div>
                            </div>
                            <?php endforeach; ?>
                            <!-- END / ITEM -->
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-info btn-square" data-dismiss="modal">Tutup</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- END Normal Modal -->

        <!-- Modal Size -->
        <div class="modal" id="ModalSize" tabindex="-1" role="dialog" aria-labelledby="modal-normal" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="block block-themed block-transparent mb-0" style="padding:20px">
                        <div class="block-header bg-info-dark">
                            <h3 class="block-title">Ukuran</h3>
                        </div>
                        <div class="block-content">
                            <div class="img">
                                <img src="<?php echo base_url(). 'theme/images/paper1.png'?>" alt="">
                            </div>

                            <div class="text-thumbs" style="width:400px"> </br>
                                <div class="text">
                                    <?php foreach($size->result() as $i): ?>
                                    <b><?php echo $i->size_nama;?> <?php echo $i->size_ukuran;?> :</b> Rp.
                                    <?php echo $i->size_harga; ?></br>
                                    <p><?php echo $i->size_keterangan;?></p>
                                    <?php endforeach; ?>
                                </div>
                            </div>
                            <!-- END / ITEM -->
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-info btn-square" data-dismiss="modal">Tutup</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- END Normal Modal -->

        <!-- Modal Text -->
        <div class="modal" id="ModalText" tabindex="-1" role="dialog" aria-labelledby="modal-normal" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="block block-themed block-transparent mb-0" style="padding:20px">
                        <div class="block-header bg-info-dark">
                            <h3 class="block-title">Kaligrafi</h3>
                        </div>
                        <div class="block-content">
                            <div class="img">
                                <img src="<?php echo base_url(). 'theme/images/text.jpg'?>" alt="">
                            </div>
                            <div class="text-thumbs"> </br>
                                <div class="text">
                                    <center>
                                        <p>
                                            Anda bisa menambahkan teks / tulisan pada lukisan atau sketsa yang dipesan
                                            <b>tanpa biaya tambahan</b>.
                                            Teks tersebut akan ditulis tangan menggunakan spidol berwarna gold (emas),
                                            kecuali ada permintaan tertentu mengenai warna tulisan atau
                                            lainnya yang disampaikan pada saat konfirmasi pesanan.
                                        </p>
                                    </center>
                                </div>
                            </div>
                            <!-- END / ITEM -->
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-info btn-square" data-dismiss="modal">Tutup</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- END Normal Modal -->

        <!-- Modal Courier -->
        <div class="modal" id="ModalCourier" tabindex="-1" role="dialog" aria-labelledby="modal-normal"
            aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="block block-themed block-transparent mb-0" style="padding:20px">
                        <div class="block-header bg-info-dark">
                            <h3 class="block-title">Kurir</h3>
                        </div>
                        <div class="block-content">
                            <?php foreach($courier->result() as $i): ?>
                            <div class="room_item-3 thumbs-left">
                                <div class="img">
                                    <img src="<?php echo base_url(). 'theme/images/courier.jpg';?>" alt="">
                                </div>
                                <div class="text-thumbs">
                                    <div class="text">
                                        <img width="100px"
                                            src="<?php echo base_url(). 'assets/images/'.$i->courier_logo;?>" alt="">
                                        <h2><a href="#"><?php echo $i->courier_nama;?></a></h2>
                                        <span class="price">Mulai dari <span
                                                class="amout"><b><?php echo 'Rp '.number_format($i->courier_harga);?></b>
                                    </div>
                                </div>
                            </div>
                            <?php endforeach; ?>
                            <!-- END / ITEM -->
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-info btn-square" data-dismiss="modal">Tutup</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- END Normal Modal -->

        <!-- Moda Add Address -->
        <form action="<?php echo base_url().'user/profile/add_address_order'?>" method="post">
            <div class="modal fade in" id="ModalAddressAdd" tabindex="-1" role="dialog" aria-labelledby="modal-normal"
                aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="block block-themed block-transparent mb-0" style="padding:20px">
                            <div class="block-header bg-primary-dark">
                                <button type="button" class="btn btn-xs btn-secondary pull-right" data-dismiss="modal"
                                    aria-label="Close">
                                    <i class="fa fa-remove"></i>
                                </button>
                                <h3 class="block-title">Tambah Alamat</h3>
                            </div></br>
                            <div class="block-content">
                                <div class="form-group">
                                    <input name="xnama" type="text" class="form-control" placeholder="Nama Alamat"
                                        required>
                                </div>
                                <div class="form-group">
                                    <textarea class="form-control" row="30" type="text" name="xalamat"
                                        placeholder="Alamat lengkap" required></textarea>
                                </div>
                                <div class="form-group">
                                    <input name="xkontak" type="number" class="form-control"
                                        placeholder="No Telp Penerima" required>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default btn-square"
                                data-dismiss="modal">Batalkan</button>
                            <button type="submit" class="btn btn-primary btn-square">Simpan</button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
        <!-- END Normal Modal -->

        <!-- Modal Address -->
        <div class="modal fade in " id="DataAddress" tabindex="-1" role="dialog" aria-labelledby="modal-normal"
            aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="block block-themed block-transparent mb-0" style="padding:20px">
                        <div class="block-header bg-primary-dark">
                            <button type="button" class="btn btn-xs btn-secondary pull-right" data-dismiss="modal"
                                aria-label="Close">
                                <i class="fa fa-remove"></i>
                            </button>
                            <h3 class="block-title">Data Alamat</h3>
                        </div></br>
                        <div class="block-content">
                            <?php if($address->num_rows() == 0): ?>
                            <center>
                                <div align="center">
                                    <a data-dismiss="modal" data-toggle="modal" data-target="#ModalAddressAdd"><img width="360px"
                                            src="<?php echo base_url().'theme/images/nodata_alamat.png';?>"
                                            alt=""></a></br> </div>
                            </center>
                            <?php else: ?>
                            <a data-dismiss="modal" data-toggle="modal" data-target="#ModalAddressAdd" class="btn btn-xs btn-info"> Tambah Alamat</a>
                            <div class="table-responsive">
                                <table id="" class="table table-striped">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th style="width:120px">Nama</th>
                                            <th>Alamat</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php 
                                            $no = 0;
                                            foreach($address->result_array() as $i):
                                                $no++;
                                                $address_id = $i['address_id'];
                                                $address_nama = $i['address_nama'];
                                                $address_alamat = $i['address_alamat'];?>
                                        <tr>
                                            <td><?php echo $no; ?></td>
                                            <td><?php echo $address_nama; ?></td>
                                            <td><?php echo $address_alamat; ?></td>
                                        </tr>
                                        <?php endforeach; ?>
                                    </tbody>
                                </table>
                            </div>
                            <?php endif; ?>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default btn-square" data-dismiss="modal">Tutup</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- END Normal Modal -->

        <!-- Modal Ubah Address -->
        <?php foreach($address->result_array() as $i): 
            $address_id = $i['address_id'];
            $address_nama = $i['address_nama'];
            $address_alamat = $i['address_alamat'];
            ?>
        <form action="<?php echo base_url().'user/profile/update_address'?>" method="post">
            <div class="modal fade" id="ModalAddressUpdate<?php echo $address_id; ?>" tabindex="-1" role="dialog"
                aria-labelledby="modal-normal" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="block block-themed block-transparent mb-0" style="padding:20px">
                            <div class="block-header bg-primary-dark">
                                <button type="button" class="btn pull-right" data-dismiss="modal" aria-label="Close">
                                    <i class="fa fa-remove"></i>
                                </button>
                                <h3 class="block-title">Ubah Alamat</h3>
                            </div></br>
                            <div class="block-content">
                                <div class="form-group">
                                    <input type="hidden" value="<?php echo $address_id;?>" name="kode">
                                    <input name="xnama" type="text" value="<?php echo $address_nama;?>"
                                        class="form-control" required>
                                </div>
                                <div class="form-group">
                                    <textarea class="form-control" row="30" type="text" name="xalamat"
                                        placeholder="Alamat lengkap"
                                        value="<?php echo $address_alamat; ?>"><?php echo $address_alamat; ?></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default btn-square" data-dismiss="modal">Batal</button>
                            <button type="submit" class="btn btn-primary btn-square">Simpan</button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
        <?php endforeach; ?>
        <!-- END Normal Modal -->

    </div>
    <!-- END / PAGE WRAP -->

    <script type="text/javascript" src="<?php echo base_url().'theme/js/lib/jquery-1.11.0.min.js'?>"></script>
    <script type="text/javascript" src="<?php echo base_url().'theme/js/lib/jquery-ui.min.js'?>"></script>
    <script type="text/javascript" src="<?php echo base_url().'theme/js/lib/bootstrap.min.js'?>"></script>
    <script type="text/javascript" src="<?php echo base_url().'theme/js/lib/bootstrap-select.js'?>"></script>
    <script type="text/javascript" src="<?php echo base_url().'theme/js/lib/isotope.pkgd.min.js'?>"></script>
    <script type="text/javascript" src="<?php echo base_url().'theme/js/lib/jquery.themepunch.revolution.min.js'?>">
    </script>
    <script type="text/javascript" src="<?php echo base_url().'theme/js/lib/jquery.themepunch.tools.min.js'?>"></script>
    <script type="text/javascript" src="<?php echo base_url().'theme/js/lib/owl.carousel.js'?>"></script>
    <script type="text/javascript" src="<?php echo base_url().'theme/js/lib/jquery.appear.min.js'?>"></script>
    <script type="text/javascript" src="<?php echo base_url().'theme/js/lib/jquery.countTo.js'?>"></script>
    <script type="text/javascript" src="<?php echo base_url().'theme/js/lib/jquery.countdown.min.js'?>"></script>
    <script type="text/javascript" src="<?php echo base_url().'theme/js/lib/jquery.parallax-1.1.3.js'?>"></script>
    <script type="text/javascript" src="<?php echo base_url().'theme/js/lib/jquery.magnific-popup.min.js'?>"></script>

    <script type="text/javascript" src="<?php echo base_url().'theme/js/lib/jquery.form.min.js'?>"></script>
    <script type="text/javascript" src="<?php echo base_url().'theme/js/lib/jquery.validate.min.js'?>"></script>

    <script type="text/javascript" src="<?php echo base_url().'theme/js/scripts.js'?>"></script>
    <script src="<?php echo base_url().'assets/js/core/jquery.min.js'?>"></script>
    <script src="<?php echo base_url().'assets/js/core/popper.min.js'?>"></script>
    <script src="<?php echo base_url().'assets/js/core/bootstrap.min.js'?>"></script>
    <script src="<?php echo base_url().'assets/js/core/jquery.slimscroll.min.js'?>"></script>
    <script src="<?php echo base_url().'assets/js/core/jquery.scrollLock.min.js'?>"></script>
    <script src="<?php echo base_url().'assets/js/core/jquery.appear.min.js'?>"></script>
    <script src="<?php echo base_url().'assets/js/core/jquery.countTo.min.js'?>"></script>
    <script src="<?php echo base_url().'assets/js/core/js.cookie.min.js'?>"></script>
    <script src="<?php echo base_url().'assets/js/codebase.js'?>"></script>
    <script src="<?php echo base_url().'assets/ckeditor/ckeditor.js'?>"></script>
    <script src="<?php echo base_url().'assets/js/dropify.min.js'?>"></script>
    <script type="text/javascript">
    $(document).ready(function() {
        $('.dropify').dropify({
            messages: {
                info: 'IMAGE 700 X 700 Pixels',
                replace: 'CHANGE',
                remove: 'DELETE',
                error: 'error'
            }
        });

        $('.dropify1').dropify({
            messages: {
                info: 'Gambar 700 x 700 Pixels',
                replace: 'Ganti',
                remove: 'Hapus',
                error: 'error'
            }
        });
    });
    </script>
    <script type="text/javascript">
    $(document).ready(function() {
        $("#sharePopup").jsSocials({
            showCount: true,
            showLabel: true,
            shareIn: "popup",
            shares: [{
                    share: "twitter",
                    label: "Twitter"
                },
                {
                    share: "facebook",
                    label: "Facebook"
                },
                {
                    share: "googleplus",
                    label: "Google+"
                },
                {
                    share: "linkedin",
                    label: "Linked In"
                },
                {
                    share: "pinterest",
                    label: "Pinterest"
                },
                {
                    share: "whatsapp",
                    label: "Whats App"
                }
            ]
        });
    });
    </script>
    <script>
    //Show Modal Service
    $('#service').on('click', function() {
        $('#ModalService').modal('show');
    });

    //Show Modal Size
    $('#size').on('click', function() {
        $('#ModalSize').modal('show');
    });

    //Show Modal Text
    $('#text').on('click', function() {
        $('#ModalText').modal('show');
    });

    //Show Modal Courier
    $('#courier').on('click', function() {
        $('#ModalCourier').modal('show');
    });
    </script>

</body>

</html>