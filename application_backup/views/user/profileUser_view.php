<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <!-- TITLE -->
    <title>Potrello | <?php echo $this->session->userdata('nama'); ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <meta name="format-detection" content="telephone=no">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <link rel="shortcut icon" href="<?php echo base_url().'theme/images/favicon.png'?>" />
    <meta name="description" content="Hotel by Geysler">

    <!-- META FOR IOS & HANDHELD -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta name="HandheldFriendly" content="true" />
    <meta name="apple-mobile-web-app-capable" content="YES" />
    <!-- //META FOR IOS & HANDHELD -->

    <!-- GOOGLE FONT -->
    <link href='http://fonts.googleapis.com/css?family=Hind:400,300,500,600%7cMontserrat:400,700' rel='stylesheet'
        type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Oleo+Script' rel='stylesheet' type='text/css'>



    <!-- CSS LIBRARY -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url().'theme/css/lib/font-awesome.min.css'?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url().'theme/css/lib/font-lotusicon.css'?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url().'theme/css/lib/bootstrap.min.css'?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url().'theme/css/lib/owl.carousel.css'?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url().'theme/css/lib/jquery-ui.min.css'?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url().'theme/css/lib/magnific-popup.css'?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url().'theme/css/lib/settings.css'?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url().'theme/css/lib/bootstrap-select.min.css'?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url().'theme/css/helper.css'?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url().'theme/css/custom.css'?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url().'theme/css/responsive.css'?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/css/dropify.min.css'?>">
    <link rel="stylesheet" href="<?php echo base_url().'assets/js/plugins/datatables/dataTables.bootstrap4.min.css'?>">



    <!-- MAIN STYLE -->

    <link rel="stylesheet" type="text/css" href="<?php echo base_url().'theme/css/style.css'?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url().'theme/css/modal.css'?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url().'theme/css/rating.css'?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url().'theme/css/introSlider.css'?>">



    <link rel="stylesheet" href="<?php echo base_url().'theme/css/jssocials.css'?>">
    <link rel="stylesheet" href="<?php echo base_url().'theme/css/tab.css'?>">
    <link rel="stylesheet" href="<?php echo base_url().'theme/css/timeline.css'?>">

    <link rel="stylesheet" href="<?php echo base_url().'theme/css/jssocials-theme-flat.css'?>">


</head>

<body>

    <!-- PRELOADER -->
    <div id="preloader">
        <span class="preloader-dot"></span>
    </div>
    <?php
      echo $rating;
    ?>
    <?php
      echo $passOTP;
    ?>
    <!-- END / PRELOADER -->

    <!-- PAGE WRAP -->
    <div id="page-wrap">

        <!-- HEADER -->
        <header id="header" class="header-v2">

            <!-- HEADER TOP -->
            <?php $this->load->view('user/headertop');?>
            <!-- END / HEADER TOP -->

            <!-- HEADER LOGO & MENU -->
            <?php $this->load->view('user/header');?>

            <!-- END / HEADER LOGO & MENU -->

        </header>
        <!-- END / HEADER -->

        <!--BANNER -->
        <section class="section-sub-banner bg-9">
            <div></div>
            <div class="sub-banner">
                <div class="container">
                    <div class="text text-center">
                    </div>
                </div>

            </div>

        </section>
        <!-- END BANNER -->

        <!-- CONTACT -->
        <section class="section-contact">
            <div class="container">
                <div class="contact">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="row">
                                <div id="contact-content"><?php echo $this->session->flashdata('msg');?></div>
                                <div class="col-xs-12 col-sm-12 col-md-3" style="border-style: outset;height:375px">
                                    <br>
                                    <center><img src="<?php echo base_url().'theme/images/user3.png';?>"
                                            class="cursor-circle-photo" width="130px"></center></br>
                                    <center>
                                        <?php foreach($user->result() as $i): ?>
                                        <h4><?php echo $i->user_nama;?></h4>
                                        <h6><?php echo $i->user_email;?></h6>
                                    </center></br>
                                    <center>
                                        <h6><a href="">ELLO-PAY</a></h6>
                                        <?php foreach($user->result() as $i): ?>
                                        <h4>Rp. <?php echo number_format($i->user_saldo); ?>,00 </h4>
                                        <?php endforeach; ?>
                                    </center></br>
                                    <center>
                                        <a class="btn btn-xs btn-default" href="#!"><i class="fa fa-share"></i>
                                            Bagikan</a>
                                        <a class="btn btn-xs btn-default" data-toggle="modal" data-target="#ModalRating"
                                            href="#!"><i class="fa fa-star-o"></i> Penilaian</a>
                                        <a class="btn btn-xs btn-default" data-toggle="modal" data-target="#ModalIntro"
                                            href="#!"><i class="fa fa-question-circle"></i> Bantuan</a>
                                    </center>
                                    <?php endforeach; ?>
                                    <br></br>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-9" style="border-style: outset;">
                                    <div class="tab">
                                        <button class="tablinks active" onclick="openCity(event, 'order')"><i
                                                class="fa fa-circle-o-notch fa-spin"></i> Pesan berjalan</button>
                                        <button class="tablinks" onclick="openCity(event, 'history')"><i
                                                class="fa fa-history"></i> Riyawat </button>
                                        <button class="tablinks" onclick="openCity(event, 'address')"><i
                                                class="fa fa-street-view"></i> Data Alamat</button>
                                        <button class="tablinks" onclick="openCity(event, 'profile')"><i
                                                class="fa fa-user"></i> Data Pribadi</button>
                                      
                                    </div>
                                    <div class="contact-option_rsp">
                                        </br>
                                        <div style="" id="profile" class="tabcontent">
                                            <form action="<?php echo base_url().'user/profile/update_profile'; ?>"
                                                method="post">
                                                <?php foreach($user->result() as $i): ?>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for=""><b>Nama</b></label>
                                                            <input type="hidden" class="form-control" placeholder="Name"
                                                                name="xkode"
                                                                value="<?php echo $this->session->userdata('iduser'); ?>"
                                                                required>
                                                            <input type="text" class="form-control" placeholder="Name"
                                                                name="xnama" value="<?php echo $i->user_nama;?>"
                                                                required>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for=""><b>Nomor Telepon (+62)</b></label>
                                                            <input type="phone" class="form-control" placeholder="+62"
                                                                name="xtelp" value="<?php echo $i->user_telp;?>"
                                                                required>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for=""><b>Alamat Email</b></label>
                                                            <input type="phone" class="form-control" placeholder="Email"
                                                                name="xemail"
                                                                value="<?php echo $this->session->userdata('email'); ?>"
                                                                readonly>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for=""><b>Password baru</b></label>
                                                            <input type="password" class="form-control"
                                                                placeholder="Type password" name="xpass1" value="">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for=""><b>Konfirmasi Password Baru</b></label>
                                                            <input type="password" class="form-control"
                                                                placeholder="Re-type password" name="xpass2">
                                                        </div>
                                                    </div>

                                                </div>
                                                <script>
                                                function Confirm() {
                                                    confirm("Are you sure want to update your profile?");
                                                }
                                                </script>
                                                <button onclick="Confirm()" type="submit"
                                                    class="btn btn-info pull-right">Pembaruan Data</button></br>
                                                <?php endforeach; ?>
                                            </form>
                                            <br>

                                        </div>
                                        <div id="order" class="tabcontent" style="display:block;overflow: scroll;">
                                            <div class="box-body" style="background-color:white">
                                                <script type="text/javascript">
                                                </script>
                                                <?php if($order->num_rows() == 0): ?>
                                                <center>
                                                    <div align="center">
                                                        <a href="<?php echo base_url().'user/order';?>">
                                                            <img width="370px"
                                                                src="<?php echo base_url().'theme/images/nodata_order.png';?>"
                                                                alt=""></a>
                                                    </div>
                                                </center>
                                                <?php else: ?>
                                                <div class="table-responsive">
                                                    <table id="" class="table table-striped" id="display">
                                                        <thead>
                                                            <tr>
                                                                <th>No Pesanan</th>
                                                                <th>Tanggal</th>
                                                                <th>Layanan</th>
                                                                <th>Ukuran</th>
                                                                <th>Kurir</th>
                                                                <th>Status</th>
                                                                <th style="text-align:right;">Tindakan</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>

                                                            <?php foreach($order->result() as $i): ?>

                                                            <tr>
                                                                <td>
                                                                    <a href="#" data-toggle="modal"
                                                                        data-target="#ModalDetail<?php echo $i->order_id;?>">#<?php echo $i->order_number; ?></a>
                                                                </td>
                                                                <td><?php echo date("d M Y, H:i A", strtotime($i->order_tgl)); ?>
                                                                </td>
                                                                <td>
                                                                    <?php $service = $this->db->query("SELECT * FROM compare where kd_compare='$i->order_service_id'");
                                                                        foreach($service->result() as $serv):
                                                                        echo $serv->type;
                                                                        endforeach;
                                                                    ?>
                                                                </td>
                                                                <td>
                                                                    <?php $size = $this->db->query("SELECT * FROM size where size_id='$i->order_size_id'");
                                                                        foreach($size->result() as $size):
                                                                        echo $size->size_nama;
                                                                        endforeach;?>
                                                                </td>
                                                                <td>
                                                                    <?php $courier = $this->db->query("SELECT * FROM courier where courier_id='$i->order_courier_id'");
                                                                        foreach($courier->result() as $cour):
                                                                        echo $cour->courier_nama;
                                                                        endforeach;?>
                                                                </td>
                                                                <td>
                                                                    <?php if($i->order_status === 'CHECKING'): ?>
                                                                    <a class="btn btn-xs btn-secondary btn-default">MENUNGGU
                                                                        <span
                                                                            class="fa fa-spinner fa-pulse fa-fw"></span></a>
                                                                    <?php elseif($i->order_status === 'PENGERJAAN'): ?>
                                                                    <a class="btn btn-xs btn-secondary btn-default"
                                                                        data-toggle="modal"
                                                                        data-target="#ModalInvoice<?php echo $i->order_id;?>">PENGERJAAN
                                                                        <span
                                                                            class="fa fa-refresh fa-spin"></span></a>
                                                                    <?php elseif($i->order_status === 'CONFIRMED'): ?>
                                                                    <a class="btn btn-xs btn-secondary btn-info"
                                                                        data-toggle="modal"
                                                                        data-target="#ModalInvoice<?php echo $i->order_id;?>">MENUNGGU
                                                                        <br>PEMBAYARAN
                                                                        <span class="fa fa-spinner fa-pulse fa-fw"></span></a>
                                                                    <?php elseif($i->order_status === 'KIRIM'): ?>
                                                                    <a class="btn btn-xs btn-secondary btn-info"
                                                                        data-toggle="modal"
                                                                        data-target="#ModalKirim<?php echo $i->order_id;?>">MENGIRIM
                                                                        <span class="fa fa-truck"></span></a>
                                                                    <?php endif; ?>
                                                                <td>
                                                                    <?php if($i->order_status === 'CHECKING'): ?>
                                                                    <a title="Edit" data-toggle="modal"
                                                                        data-target="#ModalEditOrder<?php echo $i->order_id;?>"
                                                                        class="btn btn-xs btn-secondary btn-primary"><i
                                                                            class="fa fa-edit"></i> Ubah</a>
                                                                    <a title="Cancel" data-toggle="modal"
                                                                        data-target="#ModalCancel<?php echo $i->order_id;?>"
                                                                        class="btn btn-xs btn-secondary btn-danger"><i
                                                                            class="fa fa-remove"></i> Batalkan</a>
                                                                    
                                                                    <?php else: ?>
                                                                    <b>- tidak tersedia -</b>
                                                                    <?php endif; ?>

                                                                </td>
                                                            </tr>
                                                            <?php endforeach; ?>

                                                        </tbody>
                                                    </table>
                                                </div>
                                                <?php endif; ?>
                                            </div>
                                        </div>
                                        <div id="address" class="tabcontent" style="overflow: scroll;">

                                            <?php if($address->num_rows() == 0): ?>
                                            <center>
                                                <a href="#!" id="btn-add">
                                                    <img width="370px"
                                                        src="<?php echo base_url().'theme/images/nodata_alamat.png';?>"
                                                        alt=""></a>
                                            </center>
                                            <?php else: ?>
                                            <button id="btn-add" onclick="" type="submit"
                                                class="btn btn-xs btn-secondary btn-primary"><i class="fa fa-plus"></i>
                                                Tambah
                                                Alamat</button>
                                            <div class="table-responsive">

                                                <table id="" class="table table-striped">

                                                    <thead>
                                                        <tr>
                                                            <th>No</th>
                                                            <th style="width:180px">Nama</th>
                                                            <th>Alamat</th>
                                                        </tr>
                                                    </thead>

                                                    <tbody>
                                                        <?php 
                                                                $no = 0;
                                                                foreach($address->result_array() as $i):
                                                                $no++;
                                                                $address_id = $i['address_id'];
                                                                $address_nama = $i['address_nama'];
                                                                $address_alamat = $i['address_alamat'];?>
                                                        <tr>
                                                            <td><?php echo $no; ?></td>
                                                            <td><?php echo $address_nama; ?></td>
                                                            <td><?php echo $address_alamat; ?></td>
                                                        </tr>
                                                        <?php endforeach; ?>
                                                    </tbody>
                                                </table>
                                            </div>
                                            <?php endif; ?>
                                        </div>

                                        <div id="history" class="tabcontent" style="overflow: scroll;">
                                            <?php if($history->num_rows() == 0): ?>
                                            <center>
                                                <div align="center">
                                                    <a href="<?php echo base_url().'user/order';?>"><img width="370px"
                                                            src="<?php echo base_url().'theme/images/nodata_order.png';?>"
                                                            alt=""></a></br> </div>
                                            </center>
                                            <?php else: ?>
                                            <div class="table-responsive">
                                                <table id="" class="table table-striped" id="display">
                                                    <thead>
                                                        <tr>
                                                            <th>No Order</th>
                                                            <th>Tanggal</th>
                                                            <th>Layanan</th>
                                                            <th>Ukuran</th>
                                                            <th>Kurir</th>
                                                            <th>Status</th>
                                                        </tr>
                                                    </thead>

                                                    <tbody>
                                                        <?php foreach($history->result() as $i): ?>
                                                        <tr>
                                                                <td>
                                                                    <a href="#" data-toggle="modal"
                                                                        data-target="#ModalDetail<?php echo $i->order_id;?>">#<?php echo $i->order_number; ?></a>
                                                                </td>
                                                            <td><?php echo date("d M Y, H:i A", strtotime($i->order_tgl)); ?>
                                                            </td>
                                                            <td>
                                                                <?php $service = $this->db->query("SELECT * FROM compare where kd_compare='$i->order_service_id'");
                                                                        foreach($service->result() as $serv):
                                                                        echo $serv->type;
                                                                        endforeach;
                                                                    ?>
                                                            </td>
                                                            <td>
                                                                <?php $size = $this->db->query("SELECT * FROM size where size_id='$i->order_size_id'");
                                                                        foreach($size->result() as $size):
                                                                        echo $size->size_nama;
                                                                        endforeach;?>
                                                            </td>
                                                            <td>
                                                                <?php $courier = $this->db->query("SELECT * FROM courier where courier_id='$i->order_courier_id'");
                                                                        foreach($courier->result() as $cour):
                                                                        echo $cour->courier_nama;
                                                                        endforeach;?>
                                                            </td>
                                                            <td>
                                                                <a
                                                                    class="btn btn-xs btn-secondary btn-default"><?php echo $i->order_status; ?></a>
                                                        </tr>
                                                        <?php endforeach; ?>
                                                    </tbody>
                                                </table>
                                            </div>
                                            <?php endif; ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- END / CONTACT -->



        <!-- FOOTER -->
        <?php $this->load->view('frontend/footer');?>
        <!-- END / FOOTER -->

        <!-- MODALS -->
        <!-- Modal Address -->
        <form action="<?php echo base_url().'user/profile/add_address'?>" method="post">
            <div class="modal fade in" id="ModalAddressAdd" tabindex="-1" role="dialog" aria-labelledby="modal-normal"
                aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="block block-themed block-transparent mb-0" style="padding:20px">
                            <div class="block-header bg-primary-dark">
                                <button type="button" class="btn btn-xs btn-secondary pull-right" data-dismiss="modal"
                                    aria-label="Close">
                                    <i class="fa fa-remove"></i>
                                </button>
                                <h3 class="block-title">Tambah Alamat</h3>
                            </div></br>
                            <div class="block-content">
                                <div class="form-group">
                                    <input name="xnama" type="text" class="form-control" placeholder="Nama Alamat"
                                        required>
                                </div>
                                <div class="form-group">
                                    <textarea class="form-control" row="30" type="text" name="xalamat"
                                        placeholder="Alamat lengkap" required></textarea>
                                </div>
                                <div class="form-group">
                                    <input name="xkontak" type="number" class="form-control"
                                        placeholder="No Telp Penerima" required>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default btn-square"
                                data-dismiss="modal">Batalkan</button>
                            <button type="submit" class="btn btn-primary btn-square">Simpan</button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
        <!-- END Normal Modal -->

        <!-- Modal Rating -->
        <form action="<?php echo base_url().'user/profile/add_rating'?>" method="post">
            <div class="modal fade in" id="ModalRating" tabindex="-1" role="dialog" aria-labelledby="modal-normal"
                aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="block block-themed block-transparent mb-0" style="padding:20px">
                            <div class="block-header bg-primary-dark">
                                <button type="button" class="btn btn-xs btn-secondary pull-right" data-dismiss="modal"
                                    aria-label="Close">
                                    <i class="fa fa-remove"></i>
                                </button>
                                <h3 class="block-title">Penilaian Potrello</h3>

                            </div></br>
                            <div class="block-content">
                                <div class="form-group">
                                    <p>Seberapa puaskah anda dengan layanan kami?</p>
                                </div>
                                <div class="form-group">
                                    <fieldset class="rating">
                                        <input type="radio" id="star5" name="rating" value="5" /><label class="full"
                                            for="star5" title="Awesome - 5 stars"></label>
                                        <input type="radio" id="star4half" name="rating" value="4.5" /><label
                                            class="half" for="star4half" title="Pretty good - 4.5 stars"></label>
                                        <input type="radio" id="star4" name="rating" value="4" /><label class="full"
                                            for="star4" title="Pretty good - 4 stars"></label>
                                        <input type="radio" id="star3half" name="rating" value="3.5" /><label
                                            class="half" for="star3half" title="Meh - 3.5 stars"></label>
                                        <input type="radio" id="star3" name="rating" value="3" /><label class="full"
                                            for="star3" title="Meh - 3 stars"></label>
                                        <input type="radio" id="star2half" name="rating" value="2.5" /><label
                                            class="half" for="star2half" title="Kinda bad - 2.5 stars"></label>
                                        <input type="radio" id="star2" name="rating" value="2" /><label class="full"
                                            for="star2" title="Kinda bad - 2 stars"></label>
                                        <input type="radio" id="star1half" name="rating" value="1.5" /><label
                                            class="half" for="star1half" title="Meh - 1.5 stars"></label>
                                        <input type="radio" id="star1" name="rating" value="1" /><label class="full"
                                            for="star1" title="Sucks big time - 1 star"></label>
                                        <input type="radio" id="starhalf" name="rating" value="half" /><label
                                            class="half" for="starhalf" title="Sucks big time - 0.5 stars"></label>
                                    </fieldset>

                                </div>
                                <div class="form-group">
                                    <textarea style="height:200px" row="30" class="form-control" name="saran"
                                        placeholder="Berikan Kritik dan Saran..." required></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default btn-square" data-dismiss="modal">Batal</button>
                            <button type="submit" class="btn btn-primary btn-square">Rate</button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
        <!-- END Normal Modal -->

        <!-- Modal Address -->
        <?php foreach($address->result_array() as $i): 
            $address_id = $i['address_id'];
            $address_nama = $i['address_nama'];
            $address_alamat = $i['address_alamat'];
            ?>
        <form action="<?php echo base_url().'user/profile/update_address'?>" method="post">
            <div class="modal fade" id="ModalAddressUpdate<?php echo $address_id; ?>" tabindex="-1" role="dialog"
                aria-labelledby="modal-normal" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="block block-themed block-transparent mb-0" style="padding:20px">
                            <div class="block-header bg-primary-dark">
                                <button type="button" class="btn pull-right" data-dismiss="modal" aria-label="Close">
                                    <i class="fa fa-remove"></i>
                                </button>
                                <h3 class="block-title">Ubah Alamat</h3>
                            </div></br>
                            <div class="block-content">
                                <div class="form-group">
                                    <input type="hidden" value="<?php echo $address_id;?>" name="kode">
                                    <input name="xnama" type="text" value="<?php echo $address_nama;?>"
                                        class="form-control" required>
                                </div>
                                <div class="form-group">
                                    <textarea class="form-control" row="30" type="text" name="xalamat"
                                        placeholder="Alamat lengkap"
                                        value="<?php echo $address_alamat; ?>"><?php echo $address_alamat; ?></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default btn-square" data-dismiss="modal">Batal</button>
                            <button type="submit" class="btn btn-primary btn-square">Simpan</button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
        <?php endforeach; ?>
        <!-- END Normal Modal -->

        <!-- Modal Edit Order -->
        <?php foreach($order->result_array() as $i): 
            $order_id = $i['order_id'];
            $order_number = $i['order_number'];
            $order_service_id = $i['order_service_id'];
            $order_size_id = $i['order_size_id'];
            $order_calligraphy = $i['order_calligraphy'];
            $order_img = $i['order_img'];
            $order_address_id = $i['order_address_id'];
            $order_courier_id = $i['order_courier_id'];
            $order_deskripsi = $i['order_deskripsi'];?>
        <form action="<?php echo base_url().'user/order/update_order'?>" method="post" enctype="multipart/form-data">
            <div class="modal" id="ModalEditOrder<?php echo $order_id; ?>" tabindex="-1" role="dialog"
                aria-labelledby="modal-normal" aria-hidden="true">
                <div class="modal-lg" role="document">
                    <div class="modal-content">
                        <div class="block block-themed block-transparent mb-0" style="padding:20px">
                            <div class="block-header bg-primary-dark">
                                <button type="button" class="btn pull-right" data-dismiss="modal" aria-label="Close">
                                    <i class="fa fa-remove"></i>
                                </button>
                                <h3 class="block-title">Ubah Pesanan <?php echo $order_number; ?></h3>
                            </div></br>
                            <div class="block-content row">
                                <div class="form-group col-xs-6">
                                    <input type="hidden" value="<?php echo $order_id;?>" name="xkode">
                                    <select name="xservice" id="" class="form-control" required>
                                        <?php $service = $this->db->query("SELECT * FROM compare where kd_compare='$order_service_id'");
                                        foreach($service->result() as $serv):?>
                                        <option value="<?php echo $serv->kd_compare;?>"><?php echo $serv->type; ?>
                                        </option>
                                        <?php endforeach;?>
                                        <?php foreach($Allroom->result() as $i): ?>
                                        <option value="<?php echo $i->kd_compare;?>"><?php echo $i->type;?></option>
                                        <?php endforeach; ?>
                                    </select>

                                </div>
                                <div class="form-group col-xs-6">
                                    <select name="xsize" class="form-control" id="">
                                        <?php $querySize = $this->db->query("SELECT * FROM size where size_id='$order_size_id'");
                                        foreach($querySize->result() as $size):?>
                                        <option value="<?php echo $size->size_id;?>"><?php echo $size->size_nama; ?> |
                                            <?php echo $size->size_ukuran; ?></option>
                                        <?php endforeach;?>
                                        <?php foreach($Allsize->result() as $i): ?>
                                        <option value="<?php echo $i->size_id;?>"><?php echo $i->size_nama;?> |
                                            <?php echo $i->size_ukuran;?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                                <div class="form-group col-xs-12">
                                    <input style="height:400px" type="file" name="filefoto2" class="dropify2"
                                        data-default-file="<?php echo base_url().'assets/images/'.$order_img;?>">
                                </div>
                                <div class="form-group col-xs-12">
                                    <?php if($order_calligraphy == ""): ?>
                                    <input name="xcalligraphy" class="form-control" type="text" value=""
                                        placeholder="- not set -">
                                    <?php else: ?>
                                    <input name="xcalligraphy" class="form-control" type="text"
                                        value="<?php echo $order_calligraphy; ?>">
                                    <?php endif; ?>
                                </div>
                                <div class="form-group col-xs-12">
                                    <?php if($order_deskripsi == ""): ?>
                                    <textarea name="xdeskripsi" class="form-control" type="text" value=""
                                        placeholder="Addtional - not set -"></textarea>
                                    <?php else: ?>
                                    <textarea name="xdeskripsi" class="form-control" type="text"
                                        value="<?php echo $order_deskripsi; ?>"><?php echo $order_deskripsi; ?></textarea>
                                    <?php endif; ?>
                                </div>

                                <div class="form-group col-xs-6">
                                    <select name="xcourier" id="" class="form-control">
                                        <?php $courier = $this->db->query("SELECT * FROM courier where courier_id='$order_courier_id'");
                                        foreach($courier->result() as $cour):?>
                                        <option value="<?php echo $cour->courier_id;?>">
                                            <?php echo $cour->courier_nama; ?></option>
                                        <?php endforeach;?>
                                        <?php foreach($Allcourier->result() as $i): ?>
                                        <option value="<?php echo $i->courier_id;?>"><?php echo $i->courier_nama;?>
                                        </option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                                <div class="form-group col-xs-6">
                                    <select name="xalamat" id="" class="form-control">
                                        <?php $addr = $this->db->query("SELECT * FROM address where address_id='$order_address_id'");
                                        foreach($addr->result() as $addr):?>
                                        <option value="<?php echo $addr->address_id;?>">
                                            <?php echo $addr->address_nama; ?> | <?php echo $addr->address_alamat; ?>
                                        </option>
                                        <?php endforeach;?>
                                        <?php foreach($address->result() as $i): ?>
                                        <option value="<?php echo $i->address_id;?>"><?php echo $i->address_nama;?> |
                                            <?php echo $i->address_alamat; ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default btn-square" data-dismiss="modal">Batal</button>
                            <button type="submit" class="btn btn-primary btn-square">Simpan</button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
        <?php endforeach; ?>
        <!-- END Normal Modal -->

        <!-- Modal Kirim -->
        <?php foreach($order->result_array() as $i): 
            $order_id = $i['order_id'];
            $order_number = $i['order_number'];
            $order_courier_id = $i['order_courier_id'];
            $order_resi_kirim = $i['order_resi_kirim'];
            $order_pengirim = $i['order_pengirim'];
            $order_address_id = $i['order_address_id'];?>
        <div class="modal fade" id="ModalKirim<?php echo $order_id; ?>" tabindex="-1" role="dialog"
            aria-labelledby="modal-normal" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="block block-themed block-transparent mb-0" style="padding:20px">
                        <div class="block-header bg-primary-dark">
                            <div class="block-options">
                                <button type="button" class="btn btn-xs pull-right" data-dismiss="modal"
                                    aria-label="Close">
                                    <i class="fa fa-remove"></i>
                                </button>
                            </div>
                            <h3 class="block-title">Pengiriman Order <?php echo $order_number; ?></h3></br>
                        </div>
                        <div class="block-content">
                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                        <th>No Resi / Live Tracking</th>
                                        <th>Jasa Pengirim</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td style="width:300px"><a href="<?php echo $order_resi_kirim;?>"
                                                target="_blank"><?php echo $order_resi_kirim; ?></a></td>
                                        <td>
                                            <?php $qcourier = $this->db->query("SELECT * FROM courier where courier_id='$order_courier_id'");
                                                        foreach($qcourier->result() as $qc):?>
                                            <?php echo $qc->courier_nama;?>
                                            <?php endforeach;?>
                                        </td>
                                    </tr>

                                </tbody>
                            </table>
                            <hr>
                            </hr>

                            <p class="text-muted well well-sm no-shadow" style="margin-top: 10px;">
                                Informasi Kurir : <?php echo $order_pengirim;?>
                            </p>
                            <hr>
                            </hr>
                            <?php $qaddress = $this->db->query("SELECT * FROM address where address_id='$order_address_id'");
                                            foreach($qaddress->result() as $qa):?>
                            <p>Ke Alamat : <b><?php echo $qa->address_nama;?></b></br>
                                <?php echo $qa->address_alamat;?></p>
                            <?php endforeach;?>
                            <a title="Selesai" data-dismiss="modal" data-toggle="modal" data-target="#ModalSelesai<?php echo $order_id;?>"
                                class="btn btn-xs btn-secondary btn-success">Barang Sampai</a>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default btn-square" data-dismiss="modal">Tutup</button>
                    </div>
                </div>
            </div>
        </div>
        <?php endforeach; ?>
        <!-- END Normal Modal -->

        <!-- Modal Cancel -->
        <?php foreach($order->result_array() as $i): 
            $order_id = $i['order_id'];
            $order_number = $i['order_number'];?>
        <form action="<?php echo base_url().'user/order/cancel_order'?>" method="post">
            <div class="modal fade" id="ModalCancel<?php echo $order_id; ?>" tabindex="-1" role="dialog"
                aria-labelledby="modal-normal" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="block block-themed block-transparent mb-0" style="padding:20px">
                            <div class="block-header bg-primary-dark">
                                <div class="block-options">
                                    <button type="button" class="btn btn-xs pull-right" data-dismiss="modal"
                                        aria-label="Close">
                                        <i class="fa fa-remove"></i>
                                    </button>
                                </div>
                                <h3 class="block-title">Batalkan Pesanan <?php echo $order_number; ?></h3></br>
                            </div>
                            <div class="block-content">
                                <p>Anda yakin ingin membatalkan pesanan ini?</p>
                                <input type="hidden" name="xkode" value="<?php echo $order_id; ?>" required>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default btn-square" data-dismiss="modal">Tutup</button>
                            <button type="submit" class="btn btn-primary btn-square">Ya</button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
        <?php endforeach; ?>
        <!-- END Normal Modal -->

        <!-- Modal Selesai -->
        <?php foreach($order->result_array() as $i): 
            $order_id = $i['order_id'];
            $order_number = $i['order_number'];?>
        <form action="<?php echo base_url().'user/order/selesai_order'?>" method="post">
            <div class="modal fade" id="ModalSelesai<?php echo $order_id; ?>" tabindex="-1" role="dialog"
                aria-labelledby="modal-normal" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="block block-themed block-transparent mb-0" style="padding:20px">
                            <div class="block-header bg-primary-dark">
                                <div class="block-options">
                                    <button type="button" class="btn btn-xs pull-right" data-dismiss="modal"
                                        aria-label="Close">
                                        <i class="fa fa-remove"></i>
                                    </button>
                                </div>
                                <h3 class="block-title">Pesanan <?php echo $order_number; ?> Selesai</h3></br>
                            </div>
                            <div class="block-content">
                                
                                <input type="hidden" name="xkode" value="<?php echo $order_id; ?>" required>
                                <input type="hidden" name="xorderNumber" value="<?php echo $order_number; ?>" required>
                                <input type="hidden" name="xuserId" value="<?php echo $this->session->userdata('iduser');?>" required>
                                <input type="hidden" name="xuserEmail" value="<?php echo $this->session->userdata('email');?>" required>
                                <div class="alert alert-info">
                                    <p>Apakah anda benar sudah menerima barang dan ingin menyelesaikan pesanan?</p>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default btn-square" data-dismiss="modal">Tutup</button>
                            <button type="submit" class="btn btn-primary btn-square">Ya</button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
        <?php endforeach; ?>
        <!-- END Normal Modal -->

        <!-- Modal Gopay -->
        <div class="modal fade" id="ModalGopay" tabindex="-1" role="dialog" aria-labelledby="modal-normal"
            aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="block block-themed block-transparent mb-0" style="padding:20px">
                        <div class="block-header bg-primary-dark">
                            <div class="block-options">
                                <button type="button" class="btn btn-xs pull-right" data-dismiss="modal"
                                    aria-label="Close">
                                    <i class="fa fa-remove"></i>
                                </button>
                            </div>
                            <h3 class="block-title">Pembayaran GO-PAY</h3>

                        </div><br>
                        <div class="block-content">
                            <center><img draggable="false" onContextMenu="return false;"
                                    src="<?php echo base_url().'theme/images/whatsappBarcode.jpeg' ?>" alt="">
                                <h2>Nomor : 082123228250</h2>
                            </center>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default btn-square" data-dismiss="modal">Tutup</button>

                    </div>
                </div>
            </div>
        </div>
        <!-- END Normal Modal -->

        <!-- Modal Password -->
        <form action="<?php echo base_url().'user/profile/ubah_password'?>" method="post">
            <div class="modal fade in" id="ModalPassword" tabindex="-1" role="dialog" aria-labelledby="modal-normal"
                aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="block block-themed block-transparent mb-0" style="padding:20px">
                            <div class="block-header bg-primary-dark">
                                <div class="block-options">
                                </div>
                                <h3 class="block-title">Ubah Password</h3>
                            </div><br>
                            <div class="block-content">
                                <div class="form-group">
                                    <input type="hidden" name="xuserId"
                                        value="<?php echo $this->session->userdata('iduser'); ?>">
                                    <input type="password" name="password1" class="form-control"
                                        placeholder="Password baru" required>
                                </div>
                                <div class="form-group">
                                    <input type="password" name="password2" class="form-control"
                                        placeholder="Konfirmasi Password baru" required>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="submit" class="btn btn-primary btn-square">Ubah</button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
        <!-- END Normal Modal -->

        <!-- Modal Invoice -->
        <?php foreach($invoice->result_array() as $i):
            $invoice_order_id = $i['invoice_order_id'];
            $invoice_nama_service = $i['invoice_nama_service'];
            $invoice_service_amount = $i['invoice_service_amount'];
            $invoice_nama_size = $i['invoice_nama_size'];
            $invoice_size_amount= $i['invoice_size_amount'];
            $invoice_nama_otheramount = $i['invoice_nama_otheramount'];
            $invoice_other_amount = $i['invoice_other_amount'];
            $invoice_nama_shipping = $i['invoice_nama_shipping'];
            $invoice_shipping_amount = $i['invoice_shipping_amount'];
            $invoice_total_amount = $i['invoice_total'];
            $invoice_total_paid = $i['invoice_paid'];
            $invoice_status = $i['invoice_status'];
            $invoice_payment_date = $i['invoice_payment_date'];
            $invoice_user_saldo = $i['invoice_user_saldo'];
            ?>
        <div class="modal fade" id="ModalInvoice<?php echo $invoice_order_id ?>" tabindex="-1" role="dialog"
            aria-labelledby="modal-normal" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="block block-themed block-transparent mb-0" style="padding:20px">
                        <div class="block-header bg-primary-dark">

                            <div class="block-options">
                                <button type="button" class="btn btn-xs pull-right" data-dismiss="modal"
                                    aria-label="Close">
                                    <i class="fa fa-remove"></i>
                                </button>
                            </div>
                            <h3 class="block-title">Rincian Pembayaran</h3>
                        </div></br>
                        <div class="block-content">
                            <!-- Table row -->
                            <div class="row">
                                <div class="col-xs-12 table-responsive">
                                    <table class="table table-striped">
                                        <thead>
                                            <tr>
                                                <th>Produk</th>
                                                <th style="width:200px">Deskripsi</th>
                                                <th>Subtotal</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>Layanan</td>
                                                <td><?php echo $invoice_nama_service; ?></td>
                                                <td>Rp. <?php echo number_format($invoice_service_amount); ?>,00</td>
                                            </tr>
                                            <tr>
                                                <td>Ukuran</td>
                                                <td><?php echo $invoice_nama_size; ?></td>
                                                <td>Rp. <?php echo number_format($invoice_size_amount); ?>,00</td>
                                            </tr>
                                            <tr>
                                                <td>Pengiriman</td>
                                                <td><?php echo $invoice_nama_shipping; ?></td>
                                                <td>Rp. <?php echo number_format($invoice_shipping_amount); ?>,00</td>
                                            </tr>
                                            <tr>
                                                <td>Lainnya</td>
                                                <td><?php echo $invoice_nama_otheramount; ?></td>
                                                <td>Rp. <?php echo number_format($invoice_other_amount); ?>,00</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <!-- /.col -->
                            </div>
                            <!-- /.row -->
                            <div class="row">
                                <!-- accepted payments column -->
                                <div class="col-xs-5">
                                    <?php if($invoice_status === 'UNPAID'): ?>
                                    
                                    <p class="text-muted well well-sm no-shadow" style="margin-top: 10px;">
                                        Transfer ke Rekening </br>
                                        BCA Bank Central Asia </br>
                                        <input disabled="true" id="rekening" value="2382379218379"><br><br>
                                        Ellora Desyana Permata Latif
                                    </p>
                                    <a class="btn btn-xs btn-info" onclick="copyFunction()"> Salin No Rekening</a><br></br>
                                    
                                    <?php else: ?>
                                    <p class="text-muted well well-sm no-shadow" style="margin-top: 10px;">
                                        Terima kasih telah melakukan pembayaran</br>
                                        Informasi tanda terima dikirimkan melalui</br>
                                        Email <?php echo $this->session->userdata('email'); ?>
                                    </p>
                                    <?php endif; ?>
                                </div>
                                <!-- /.col -->
                                <div class="col-xs-7">
                                    <div class="table-responsive">
                                        <table class="table">
                                            <tr>
                                                <th>Pajak (0%)</th>
                                                <td>Rp. 0,00</td>
                                            </tr>
                                            <tr>
                                                <th>Total Bayar</th>
                                                <td>
                                                    <?php if($invoice_status === 'PAID'):?>
                                                    <b>Rp. <?php echo number_format($invoice_total_paid); ?>,00</b>
                                                    <?php else: ?>
                                                    <b>Rp. <?php echo number_format($invoice_total_amount); ?>,00</b>
                                                    <?php endif; ?>
                                                </td>   
                                            </tr>
                                            <?php if($invoice_status === 'PAID'): ?>
                                            <tr>
                                                <th>Konfirmasi</th>
                                                <td><?php echo date("d M Y",strtotime($invoice_payment_date)); ?></td>
                                            </tr>
                                            <?php else: ?>
                                            <?php endif; ?>
                                        </table>
                                        <?php if($invoice_status === 'UNPAID'): ?>
                                        <img draggable="false" onContextMenu="return false;" id="imgKuat"
                                            src="<?php echo base_url().'theme/images/belumlunas.jpg';?>" alt="">
                                            <?php $qorder = $this->db->query("SELECT * FROM tbl_order where order_id='$invoice_order_id'"); ?>
                                        <?php foreach($qorder->result_array() as $i):
                                                $order_expdate = $i['order_expdate'];?>
                                            <?php endforeach; ?>
                                            <div class="pull-right">
                                            <a href="#!" class="btn btn-default" data-dismiss="modal"
                                                data-toggle="modal" data-target="#ModalGopay"><img width="70px"
                                                src="<?php echo base_url().'theme/images/gopay.png';?>" alt=""></a>
                                                <a class="btn btn-primary" data-dismiss="modal" data-toggle="modal" data-target="#ModalLapor<?php echo $invoice_order_id;?>"> Lapor Pembayaran</a>
                                                
                                            </div>
                                            
                                        <?php else: ?>
                                        <img draggable="false" onContextMenu="return false;" id="imgKuat"
                                            src="<?php echo base_url().'theme/images/lunas.jpg';?>" alt="">
                                        <?php endif; ?>

                                        
                                    </div>
                                </div>
                                <!-- /.col -->
                                
                            </div><br>
                            <!-- /.row -->
                            
                        </div>
                        
                    </div>
                    <div class="modal-footer">
                    
                        <button type="button" class="btn btn-default btn-square" data-dismiss="modal">Tutup</button>
                    </div>
                </div>
            </div>
        </div>
        <?php endforeach; ?>
        <!-- END Normal Modal -->

        <!-- Modal Detail -->
        <?php foreach($order->result_array() as $i): 
            $order_id = $i['order_id'];
            $order_number = $i['order_number'];
            $order_service_id = $i['order_service_id'];
            $order_size_id = $i['order_size_id'];
            $order_calligraphy = $i['order_calligraphy'];
            $order_img = $i['order_img'];
            $order_address_id = $i['order_address_id'];
            $order_tgl = $i['order_tgl'];
            $order_deskripsi = $i['order_deskripsi']; 
            $order_tglconfirmed = $i['order_tglconfirm'];?>
        <div class="modal" id="ModalDetail<?php echo $order_id; ?>" tabindex="-1" role="dialog"
            aria-labelledby="modal-normal" aria-hidden="true">
            <div class="modal-lg" role="document">
                <div class="modal-content">
                    <div class="block block-themed block-transparent mb-0" style="padding:20px">
                        <div class="block-header bg-primary-dark">
                            <button type="button" class="btn pull-right" data-dismiss="modal" aria-label="Close">
                                <i class="fa fa-remove"></i>
                            </button>

                            <div class="block-options">
                            </div>
                            <h3>Rincian Pesanan</h3></br>
                        </div>
                        <div class="block-content">

                            <div class="row">
                                <div class="col-md-4">
                                    <img width="300px" style="padding-bottom:10px"
                                        src="<?php echo base_url().'assets/images/'.$order_img;?>" alt="">

                                </div>
                                <div class="col-md-8">
                                    <table class="table table-striped">
                                        <tr>
                                            <th style="width:170px">No Pesanan</th>
                                            <td>: <?php echo $order_number; ?></td>
                                        </tr>
                                        <tr>
                                            <th style="width:120px">Service</th>
                                            <td>:
                                                <?php $qserv = $this->db->query("SELECT * FROM compare where kd_compare='$order_service_id'");
                                            foreach($qserv->result() as $serv):
                                                echo $serv->type;
                                            endforeach;?>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th style="width:120px">Ukuran</th>
                                            <td>:
                                                <?php $qsize = $this->db->query("SELECT * FROM size where size_id='$order_size_id'");
                                            foreach($qsize->result() as $size):?>
                                                <?php echo $size->size_nama;?> |
                                                <?php echo $size->size_ukuran;?>
                                                <?php endforeach;?>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th style="width:120px">Kurir</th>
                                            <td>:
                                                <?php $qcourier = $this->db->query("SELECT * FROM courier where courier_id='$order_courier_id'");
                                            foreach($qcourier->result() as $cour):
                                                echo $cour->courier_nama;
                                            endforeach;?>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th style="width:120px">Alamat Pengiriman</th>
                                            <td>:
                                                <?php $qaddr = $this->db->query("SELECT * FROM address where address_id='$order_address_id'");
                                            foreach($qaddr->result() as $addr):
                                                echo $addr->address_nama;
                                            endforeach;?>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th style="width:120px">Kaligrafi </th>
                                            <td>: <?php if($order_calligraphy == ""): ?>
                                                <i>- tidak dipasang -</i>
                                                <?php else :?>
                                                <b>"<?php echo $order_calligraphy; ?>"</b>
                                                <?php endif; ?>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th style="width:120px">Keterangan Lain </th>
                                            <td>: <?php if($order_deskripsi == ""): ?>
                                                <i>- tidak dipasang -</i>
                                                <?php else: ?>
                                                <?php echo $order_deskripsi; ?>
                                                <?php endif; ?>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th style="width:120px">Tanggal Pesan</th>
                                            <td>: <?php echo date("d M y h:i",strtotime($order_tgl)); ?></td>
                                        </tr>
                                        <tr>
                                            <th style="width:120px">Tanggal Konfirmasi</th>
                                            <td>:
                                                <?php if($order_tglconfirmed > 0 ): ?>
                                                <?php echo date("d M y h:i", strtotime($order_tglconfirmed)); ?>
                                                <?php else: ?>
                                                <i>- belum terkonfirmasi -</i>
                                                <?php endif; ?>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                         
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default btn-square" data-dismiss="modal">Tutup</button>
                    </div>
                </div>
            </div>
        </div>
        <?php endforeach; ?>
        <!-- END Normal Modal -->

        <!-- Modal Intro -->
        <div class="modal" id="ModalIntro" tabindex="-1" role="dialog" aria-labelledby="modal-normal"
            aria-hidden="true">
            <div class="modal-lg" role="document">
                <div class="modal-content">
                    <div class="block block-themed block-transparent mb-0" style="padding:20px;">
                        <div class="block-header bg-primary-dark">
                            <button type="button" class="btn pull-right" data-dismiss="modal" aria-label="Close">
                                <i class="fa fa-remove"></i>
                            </button>
                        </div></br>
                        <div class="block-content">
                            <center>
                                <h2
                                    style="font: 100 50px/1.3 'Oleo Script', Helvetica, sans-serif;color: #2b2b2b;text-shadow: 4px 4px 0px rgba(0,0,0,0.1);">
                                    User Dashboard Potrello.id</h2>
                                <div class="slider">
                                    <div id="slider">
                                        <a href="#" class="control_next"><i class="fa fa-chevron-circle-right"></i></a>
                                        <a href="#" class="control_prev"><i class="fa fa-chevron-circle-left"></i></a>
                                        <ul>
                                            <li><img src="<?php echo base_url().'theme/images/tutorAwal.png' ?>" alt="">
                                            </li>
                                            <li><img src="<?php echo base_url().'theme/images/tutor1.png' ?>" alt="">
                                            </li>
                                            <li><img src="<?php echo base_url().'theme/images/tutor2.png' ?>" alt="">
                                            </li>
                                            <li>
                                                <img src="<?php echo base_url().'theme/images/tutor3.png' ?>" alt="">

                                            </li>
                                        </ul>                                       
                                    </div>
                            </center>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END Normal Modal -->

        <!-- Modal Lapor -->
        <?php foreach($order->result_array() as $i): 
            $order_id = $i['order_id'];
            $order_number = $i['order_number'];?>
      <form action="<?php echo site_url('user/report/kirim');?>" method="post" enctype="multipart/form-data">
        <div class="modal fade in" id="ModalLapor<?php echo $order_id; ?>" tabindex="-1" role="dialog" aria-labelledby="modal-normal"
            aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="block block-themed block-transparent mb-0" style="padding:20px;">
                        <div class="block-header bg-primary-dark">
                            <button type="button" class="btn pull-right" data-dismiss="modal" aria-label="Close">
                                <i class="fa fa-remove"></i>
                            </button>
                        </div>
                        <h3 class="block-title">Lapor Pembayaran</h3></br>
                        <div class="block-content">
                                    <input type="hidden" class="form-control"  name="name" placeholder="Name" value="<?php echo $this->session->userdata('nama'); ?>" readonly required>
                                    <input type="hidden" class="form-control" name="email" placeholder="Email" value="<?php echo $this->session->userdata('email'); ?>" readonly required>
                                <div class="form-group">
                                    <input type="text" class="form-control" name="subject" value="<?php echo $order_number; ?>" readonly required>                                 
                                </div>
                                <div class="form-group">
                                    <textarea cols="30" rows="5" name="message"  class="form-control" placeholder="Jumlah Pembayaran, Nama Rekening Pengirim" required></textarea>
                                </div>
                                <div class="form-group">
                                    <label style="margin-top:20px;margin-bottom:-30px" for="">Upload Bukti Pembayaran</label>
                                    <input style="height:300px" type="file" name="userfile" class="dropify" data-height="250" required>                                        
                                </div>
                    </div>
                        <div class="modal-footer">
                            <button type="submit" class="btn btn-primary btn-square">Lapor</button>
                            <button type="button" class="btn btn-default btn-square" data-dismiss="modal">Tutup</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </form>
        <?php endforeach; ?>
        <!-- END Normal Modal -->


    </div>
    <!-- END / PAGE WRAP -->

    <script type="text/javascript" src="<?php echo base_url().'theme/js/lib/jquery-1.11.0.min.js'?>"></script>
    <script type="text/javascript" src="<?php echo base_url().'theme/js/lib/jquery-ui.min.js'?>"></script>
    <script type="text/javascript" src="<?php echo base_url().'theme/js/lib/bootstrap.min.js'?>"></script>
    <script type="text/javascript" src="<?php echo base_url().'theme/js/lib/bootstrap-select.js'?>"></script>
    <script type="text/javascript" src="<?php echo base_url().'theme/js/lib/isotope.pkgd.min.js'?>"></script>
    <script type="text/javascript" src="<?php echo base_url().'theme/js/lib/jquery.themepunch.revolution.min.js'?>">
    </script>
    <script type="text/javascript" src="<?php echo base_url().'theme/js/lib/jquery.themepunch.tools.min.js'?>"></script>
    <script type="text/javascript" src="<?php echo base_url().'theme/js/lib/owl.carousel.js'?>"></script>
    <script type="text/javascript" src="<?php echo base_url().'theme/js/lib/jquery.appear.min.js'?>"></script>
    <script type="text/javascript" src="<?php echo base_url().'theme/js/lib/jquery.countTo.js'?>"></script>
    <script type="text/javascript" src="<?php echo base_url().'theme/js/lib/jquery.countdown.min.js'?>"></script>
    <script type="text/javascript" src="<?php echo base_url().'theme/js/lib/jquery.parallax-1.1.3.js'?>"></script>
    <script type="text/javascript" src="<?php echo base_url().'theme/js/lib/jquery.magnific-popup.min.js'?>"></script>

    <script type="text/javascript" src="<?php echo base_url().'theme/js/lib/jquery.form.min.js'?>"></script>
    <script type="text/javascript" src="<?php echo base_url().'theme/js/lib/jquery.validate.min.js'?>"></script>
    <script type="text/javascript" src="<?php echo base_url().'theme/js/introSlider.js'?>"></script>


    <script type="text/javascript" src="<?php echo base_url().'theme/js/scripts.js'?>"></script>
    <script src="<?php echo base_url().'assets/js/core/jquery.min.js'?>"></script>
    <script src="<?php echo base_url().'assets/js/core/popper.min.js'?>"></script>
    <script src="<?php echo base_url().'assets/js/core/bootstrap.min.js'?>"></script>
    <script src="<?php echo base_url().'assets/js/core/jquery.slimscroll.min.js'?>"></script>
    <script src="<?php echo base_url().'assets/js/core/jquery.scrollLock.min.js'?>"></script>
    <script src="<?php echo base_url().'assets/js/core/jquery.appear.min.js'?>"></script>
    <script src="<?php echo base_url().'assets/js/core/jquery.countTo.min.js'?>"></script>
    <script src="<?php echo base_url().'assets/js/core/js.cookie.min.js'?>"></script>
    <script src="<?php echo base_url().'assets/js/codebase.js'?>"></script>
    <script src="<?php echo base_url().'assets/ckeditor/ckeditor.js'?>"></script>
    <script src="<?php echo base_url().'assets/js/plugins/datatables/jquery.dataTables.min.js'?>"></script>
    <script src="<?php echo base_url().'assets/js/plugins/datatables/dataTables.bootstrap4.min.js'?>"></script>
    <script src="<?php echo base_url().'assets/js/dropify.min.js'?>"></script>

    <script>
    function copyFunction() {
        /* Get the text field */
        var copyText = document.getElementById("rekening");

        /* Select the text field */
        copyText.select();

        /* Copy the text inside the text field */
        document.execCommand("copy");

        /* Alert the copied text */
        alert(copyText.value + " Disalin ke clipboard");
    }
    </script>
    <script>
    document.getElementById('imgKuat').ondragstart = function() {
        return false;
    };
    </script>

    <script type="text/javascript">
    $(document).ready(function() {
        $('.dropify').dropify({
            messages: {
                default: 'Image 900 X 400 Pixels',
                replace: 'Ganti',
                remove: 'Hapus',
                error: 'error'
            }
        });

        $('.dropify2').dropify({
            messages: {
                default: 'Image 700 X 700 Pixels',
                replace: 'Ganti',
                remove: 'Hapus',
                error: 'error'
            }
        });
        $('.dropify3').dropify({
            messages: {
                default: 'Image 420 X 420 Pixels',
                replace: 'Ganti',
                remove: 'Hapus',
                error: 'error'
            }
        });
    });
    </script>
    <script type="text/javascript">
    $(document).ready(function() {
        $('.dropify').dropify({
            messages: {
                default: 'IMAGE 700 X 700 Pixels',
                replace: 'CHANGE',
                remove: 'DELETE',
                error: 'error'
            }
        });

        $('.dropify2').dropify({
            messages: {
                default: 'Image 230 X 280 Pixels',
                replace: 'Ganti',
                remove: 'Hapus',
                error: 'error'
            }
        });
    });
    </script>
    <script type="text/javascript">
    $(document).ready(function() {
        $('#mytable').DataTable();
        $('#mytable2').DataTable();
        $('#mytable3').DataTable();
        $("#sharePopup").jsSocials({
            showCount: true,
            showLabel: true,
            shareIn: "popup",
            shares: [{
                    share: "twitter",
                    label: "Twitter"
                },
                {
                    share: "facebook",
                    label: "Facebook"
                },
                {
                    share: "googleplus",
                    label: "Google+"
                },
                {
                    share: "linkedin",
                    label: "Linked In"
                },
                {
                    share: "pinterest",
                    label: "Pinterest"
                },
                {
                    share: "whatsapp",
                    label: "Whats App"
                }
            ]
        });
    });
    </script>
    <script>
    function openCity(evt, cityName) {
        var i, tabcontent, tablinks;
        tabcontent = document.getElementsByClassName("tabcontent");
        for (i = 0; i < tabcontent.length; i++) {
            tabcontent[i].style.display = "none";
        }

        tablinks = document.getElementsByClassName("tablinks");
        for (i = 0; i < tablinks.length; i++) {
            tablinks[i].className = tablinks[i].className.replace(" active", "");
        }
        document.getElementById(cityName).style.display = "block";
        evt.currentTarget.className += " active";
    }
    </script>
    <script>
    //Show Modal Add New Address
    $('#btn-add').on('click', function() {
        $('#ModalAddressAdd').modal('show');
    });

    //Show Modal Add New Address
    $('#btn-edit').on('click', function() {
        var id = $(this).data('id');
        var nama = $(this).data('nama');
        var alamat = $(this).data('alamat');
        $('#ModalAddressUpdate').modal('show');
        $('[name="xkode"]').val(id);
        $('[name="xnama"]').val(nama);
        $('[name="xalamat"]').val(alamat);
    });
    //Show data modal invoice record
    $('#btn-invoice').on('click', function() {

        var service = $(this).data('service');
        var size = $(this).data('size');
        var shipping = $(this).data('shipping');
        var address = $(this).data('address');
        $('[name="service"]').val(service);
        $('[name="size"]').val(size);
        $('[name="shipping"]').val(shipping);
        $('[name="address"]').val(service);
    });
    </script>



</body>

</html>