<!doctype html>
<html lang="en" class="no-focus"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">

        <title>Size Post</title>

        <meta name="description" content="">
        <meta name="author" content="Gesrel Schwarzenegger">
        <meta name="robots" content="noindex, nofollow">

        <!-- Icons -->
        <!-- The following icons can be replaced with your own, they are used by desktop and mobile browsers -->
        <link rel="shortcut icon" href="<?php echo base_url().'assets/images/favicon.png'?>">

        <!-- END Icons -->
        <link rel="stylesheet" href="<?php echo base_url().'assets/js/plugins/datatables/dataTables.bootstrap4.min.css'?>">
        <link rel="stylesheet" id="css-main" href="<?php echo base_url().'assets/css/codebase.min.css'?>">



    </head>
    <body>
        <!-- Page Container -->

        <div id="page-container" class="sidebar-o side-scroll main-content-boxed side-trans-enabled page-header-fixed">


            <?php echo $this->load->view('admin/v_sidemenu.php');?>

            <!-- Header -->
            <?php echo $this->load->view('admin/header.php');?>
            <!-- END Header -->

            <!-- Main Container -->
            <main id="main-container">
                <!-- Page Content -->
                <div class="content">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="block">
                                <div class="block-header block-header-default">
                                    <h3 class="block-title">Size List</h3>
                                    <div class="block-options">
                                        <button class="btn btn-primary" id="btn-add-new"><span class="fa fa-plus"></span> Add New</button>
                                    </div>
                                </div>
                                <div class="block-content block-content-full" style="overflow:auto">
                                    <table id="mytable" class="table table-striped">
                                        <thead>
                                            <tr>
                                                <th style="width: 120px;text-align: left;">No</th>
                                                <th>Size</th>
                                                <th>Price</th>
                                                <th>Cm</th>
                                                <th>Desc</th>
                                                <th style="text-align:center;">Aksi</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        <?php
                                            $no=0;
                                            $data=$this->db->query("SELECT * FROM size");
                                            foreach ($data->result() as $row) :
                                            $no++;
                                        ?>
                                            <tr>
                                                <td><?php echo $no;?></td>
                                                <td><?php echo $row->size_nama;?></td>
                                                <td>Rp. <?php echo number_format($row->size_harga); ?></td>
                                                <td><?php echo $row->size_ukuran; ?></td>
                                                <td><?php echo $row->size_keterangan; ?></td>
                                                <td style="width: 90px;text-align: center;">
                                                    <a href="javascript:void(0);" class="btn btn-sm btn-secondary btn-circle btn-edit" data-id="<?php echo $row->size_id;?>" data-size="<?php echo $row->size_nama;?>" data-harga="<?php echo $row->size_harga;?>" data-ukuran="<?php echo $row->size_ukuran;?>" data-keterangan="<?php echo $row->size_keterangan; ?>"><span class="fa fa-pencil"></span></a>
                                                    <a href="javascript:void(0);" class="btn btn-sm btn-secondary btn-circle btn-hapus" data-id="<?php echo $row->size_id;?>"><span class="fa fa-trash"></span></a>
                                                </td>
                                            </tr>
                                        <?php endforeach;?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>


                </div>
                <!-- END Page Content -->
            </main>
            <!-- END Main Container -->
            <!-- Footer -->
            <footer id="page-footer" class="opacity-0">
                <div class="content py-20 font-size-xs clearfix">
                    <div class="float-right">
                        Created with <i class="fa fa-heart text-pulse"></i> by <a class="font-w600" href="http://mfikri.com" target="_blank">Gesrel Schwarzenegger</a>
                    </div>
                    <div class="float-left">
                        <a class="font-w600" href="https://mfikri.com" target="_blank">Gesrel</a> &copy; <span class="js-year-copy">2017</span>
                    </div>
                </div>
            </footer>
            <!-- END Footer -->
        </div>
        <!-- END Page Container -->

        <!-- Modal Hapus -->
        <form action="<?php echo base_url().'admin/size/simpan_size'?>" method="post">
        <div class="modal" id="ModalAddNew" tabindex="-1" role="dialog" aria-labelledby="modal-normal" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="block block-themed block-transparent mb-0">
                        <div class="block-header bg-primary-dark">
                            <h3 class="block-title">Add New</h3>
                            <div class="block-options">
                                <button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
                                    <i class="si si-close"></i>
                                </button>
                            </div>
                        </div>
                        <div class="block-content">
                            <div class="form-group">
                                <input type="text" name="xsize" class="form-control" placeholder="Size" required>
                            </div>
                            <div class="form-group">
                                <input type="number" name="xharga" class="form-control" placeholder="Harga" required>
                            </div>
                            <div class="form-group">
                                <input type="text" name="xukuran" class="form-control" placeholder="Ukuran" required>
                            </div>
                            <div class="form-group">
                                <textarea type="text" name="xketerangan" class="form-control" placeholder="Keterangan" required></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default btn-square" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary btn-square">Save</button>
                    </div>
                </div>
            </div>
        </div>
        </form>
        <!-- END Normal Modal -->

        <!-- Modal Hapus -->
        <form action="<?php echo base_url().'admin/size/update_size'?>" method="post">
        <div class="modal" id="ModalUpdate" tabindex="-1" role="dialog" aria-labelledby="modal-normal" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="block block-themed block-transparent mb-0">
                        <div class="block-header bg-primary-dark">
                            <h3 class="block-title">Update Size</h3>
                            <div class="block-options">
                                <button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
                                    <i class="si si-close"></i>
                                </button>
                            </div>
                        </div>
                        <div class="block-content">
                            <div class="form-group">
                                <input type="text" name="xsize2" class="form-control" placeholder="Size" required>
                            </div>
                            <input type="hidden" name="xkode" id="kode" required>
                            <div class="form-group">
                                <input type="number" name="xharga" class="form-control" placeholder="Harga" required>
                            </div>
                            <div class="form-group">
                                <input type="text" name="xukuran" class="form-control" placeholder="Ukuran" required>
                            </div>
                            <div class="form-group">
                                <textarea type="text" name="xketerangan" class="form-control" placeholder="Keterangan" required></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default btn-square" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary btn-square">Update</button>
                    </div>
                </div>
            </div>
        </div>
        </form>
        <!-- END Normal Modal -->

        <!-- Modal Hapus -->
        <form action="<?php echo base_url().'admin/size/hapus_size'?>" method="post">
        <div class="modal" id="Modalhapus" tabindex="-1" role="dialog" aria-labelledby="modal-normal" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="block block-themed block-transparent mb-0">
                        <div class="block-header bg-primary-dark">
                            <h3 class="block-title">Info</h3>
                            <div class="block-options">
                                <button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
                                    <i class="si si-close"></i>
                                </button>
                            </div>
                        </div>
                        <div class="block-content">
                            <p>Anda yakin mau menghapus Size ini?</p>
                            <input type="hidden" name="kode" id="kode" required>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default btn-square" data-dismiss="modal">Tidak</button>
                        <button type="submit" class="btn btn-primary btn-square">Ya</button>
                    </div>
                </div>
            </div>
        </div>
        </form>
        <!-- END Normal Modal -->


        <!-- Codebase Core JS -->
        <script src="<?php echo base_url().'assets/js/core/jquery.min.js'?>"></script>
        <script src="<?php echo base_url().'assets/js/core/popper.min.js'?>"></script>
        <script src="<?php echo base_url().'assets/js/core/bootstrap.min.js'?>"></script>
        <script src="<?php echo base_url().'assets/js/core/jquery.slimscroll.min.js'?>"></script>
        <script src="<?php echo base_url().'assets/js/core/jquery.scrollLock.min.js'?>"></script>
        <script src="<?php echo base_url().'assets/js/core/jquery.appear.min.js'?>"></script>
        <script src="<?php echo base_url().'assets/js/core/jquery.countTo.min.js'?>"></script>
        <script src="<?php echo base_url().'assets/js/core/js.cookie.min.js'?>"></script>
        <script src="<?php echo base_url().'assets/js/codebase.js'?>"></script>
        <script src="<?php echo base_url().'assets/ckeditor/ckeditor.js'?>"></script>
        <script src="<?php echo base_url().'assets/js/plugins/datatables/jquery.dataTables.min.js'?>"></script>
        <script src="<?php echo base_url().'assets/js/plugins/datatables/dataTables.bootstrap4.min.js'?>"></script>

        <script type="text/javascript">
            $(document).ready(function() {
                $('#mytable').DataTable();

                //Show Modal Add New
                $('#btn-add-new').on('click',function(){
                    $('#ModalAddNew').modal('show');
                });

                //Show Modal Update size
                $('.btn-edit').on('click',function(){
                    var size_id=$(this).data('id');
                    var size_nama=$(this).data('size');
                    var size_harga=$(this).data('harga');
                    var size_ukuran=$(this).data('ukuran');
                    var size_keterangan=$(this).data('keterangan');
                    $('#ModalUpdate').modal('show');
                    $('[name="xkode"]').val(size_id);
                    $('[name="xsize2"]').val(size_nama);
                    $('[name="xharga"]').val(size_harga);
                    $('[name="xukuran"]').val(size_ukuran);
                    $('[name="xketerangan"]').val(size_keterangan);

                });

                //Show Konfirmasi modal hapus record
                $('.btn-hapus').on('click',function(){
                    var size_id=$(this).data('id');
                    $('#Modalhapus').modal('show');
                    $('[name="kode"]').val(size_id);
                });

            });
        </script>

    </body>
</html>
