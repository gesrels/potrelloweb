<div class="content-header-item">
    <a class="link-effect font-w700" href="<?php echo base_url().'admin/dashboard'?>">
        <i class="text-primary"></i>
        <span class="font-size-xl text-dual-primary-dark">POTRELLO</span><span class="font-size-xl text-success"><strong>CMS</strong></span>
    </a>
</div>
