<!doctype html>
<html lang="en" class="no-focus"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">

        <title>Order</title>

        <meta name="description" content="">
        <meta name="author" content="Gesrel Schwarzenegger">
        <meta name="robots" content="noindex, nofollow">

        <!-- Icons -->
        <!-- The following icons can be replaced with your own, they are used by desktop and mobile browsers -->
        <link rel="shortcut icon" href="<?php echo base_url().'assets/images/favicon.png'?>">

        <!-- END Icons -->
        <link rel="stylesheet" href="<?php echo base_url().'assets/js/plugins/datatables/dataTables.bootstrap4.min.css'?>">
        <link rel="stylesheet" id="css-main" href="<?php echo base_url().'assets/css/codebase.min.css'?>">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/css/dropify.min.css'?>">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/plugins/toast/jquery.toast.min.css'?>"/>



    </head>
    <body>
        <!-- Page Container -->

        <div id="page-container" class="sidebar-o side-scroll main-content-boxed side-trans-enabled page-header-fixed">


             <?php echo $this->load->view('admin/v_sidemenu'); ?>

            <!-- Header -->
            <?php echo $this->load->view('admin/header.php');?>
            <!-- END Header -->

            <!-- Main Container -->
            <main id="main-container">
                <!-- Page Content -->
                <div class="content">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="block">
                                <div class="block-header block-header-default">
                                    <h3 class="block-title">Order List</h3>
                                    <div class="block-options">
                                        <!-- <button class="btn btn-primary" id="btn-add-new"><span class="fa fa-plus"></span> Add New</button> -->
                                    </div>
                                </div>
                                <div class="block-content block-content-full" style="overflow:auto">
                                <table id="mytable" class="table table-striped" id="display">
                                                        <thead>
                                                            <tr>
                                                                <th>No Order</th>
                                                                <th>Date</th>
                                                                <th>User</th>
                                                                <th>Service</th>
                                                                <th>Size</th>
                                                                <!-- <th>Courier</th> -->
                                                                <th>Status</th>
                                                                <th style="text-align:right;">Action</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>

                                                            <?php foreach($order->result() as $i): ?>

                                                            <tr>
                                                                <td><a href="#" data-toggle="modal"
                                                                        data-target="#ModalDetail<?php echo $i->order_id;?>">#<?php echo $i->order_number; ?></a>
                                                                </td>
                                                                <td><?php echo date("d M Y, H:i A", strtotime($i->order_tgl)); ?>
                                                                </td>
                                                                <td>
                                                                <?php $quser = $this->db->query("SELECT * FROM user where user_id='$i->order_user_id'");
                                                                        foreach($quser->result() as $usr):?>
                                                                    <a href="message:<?php echo $usr->user_email; ?>"><?php echo $usr->user_email; ?></a></br>
                                                                    <a target="_blank" href="https://wa.me/<?php echo $usr->user_telp; ?>"><?php echo $usr->user_telp; ?></a>
                                                                        
                                                                   <?php endforeach;?>
                                                                </td>
                                                                <td>
                                                                    <?php $service = $this->db->query("SELECT * FROM compare where kd_compare='$i->order_service_id'");
                                                                        foreach($service->result() as $serv):
                                                                        echo $serv->type;
                                                                        endforeach;
                                                                    ?>
                                                                </td>
                                                                <td>
                                                                    <?php $size = $this->db->query("SELECT * FROM size where size_id='$i->order_size_id'");
                                                                        foreach($size->result() as $size):
                                                                        echo $size->size_nama;
                                                                        endforeach;?>
                                                                </td>
                                                                <!-- <td>
                                                                    <?php $courier = $this->db->query("SELECT * FROM courier where courier_id='$i->order_courier_id'");
                                                                        foreach($courier->result() as $cour):
                                                                        // echo $cour->courier_nama;
                                                                        endforeach;?>
                                                                </td> -->
                                                                <td>
                                                                    <?php if($i->order_status === 'CHECKING'): ?>
                                                                    <span class="badge badge-info"><?php echo $i->order_status; ?>
                                                                        <span class="fa fa-refresh fa-spin"></span></span>
                                                                    <?php elseif($i->order_status === 'CANCELED'):?>
                                                                    <span class="badge badge-danger"><?php echo $i->order_status; ?>
                                                                        <span class="fa fa-remove"></span></span>
                                                                    <?php elseif($i->order_status === 'PENGERJAAN'):?>
                                                                    <span class="badge badge-success"><?php echo $i->order_status; ?>
                                                                        <span class="fa fa-refresh fa-spin"></span></span>
                                                                    <?php elseif($i->order_status === 'KIRIM'):?>
                                                                    <span class="badge badge-info"><?php echo $i->order_status; ?>
                                                                        <span class="fa fa-cube"></span></span>
                                                                    <?php else: ?>
                                                                    <span class="badge badge-warning"><?php echo $i->order_status; ?>
                                                                        <span class="fa fa-check"></span></span>
                                                                    <?php endif; ?>
                                                                <td>
                                                                    <?php if($i->order_status === 'CONFIRMED'): ?>
                                                                    <a title="Paid" data-toggle="modal"
                                                                        data-target="#ModalPaid<?php echo $i->order_id;?>"
                                                                        class="btn btn-sm btn-secondary btn-circle"><i
                                                                            class="fa fa-money"></i></a>
                                                                    <a title="Cancel" data-toggle="modal"
                                                                        data-target="#ModalCancel2<?php echo $i->order_id;?>"
                                                                        class="btn btn-sm btn-secondary btn-circle"><i
                                                                            class="fa fa-remove"></i></a>

                                                                    <?php elseif($i->order_status === 'CHECKING'): ?>

                                                                    <a title="Confirm" data-toggle="modal"
                                                                        data-target="#ModalConfirm<?php echo $i->order_id;?>"
                                                                        class="btn btn-sm btn-secondary btn-circle"><i
                                                                            class="fa fa-check"></i></a>
                                                                    <a title="Edit" data-toggle="modal"
                                                                        data-target="#ModalEditOrder<?php echo $i->order_id;?>"
                                                                        class="btn btn-sm btn-secondary btn-circle"><i
                                                                            class="fa fa-edit"></i></a>
                                                                    <a title="Cancel" data-toggle="modal"
                                                                        data-target="#ModalCancel1<?php echo $i->order_id;?>"
                                                                        class="btn btn-sm btn-secondary btn-circle"><i
                                                                            class="fa fa-remove"></i></a>

                                                                    <?php elseif($i->order_status === 'PENGERJAAN'): ?>
                                                                    <a title="Kirim" data-toggle="modal"
                                                                        data-target="#ModalKirim<?php echo $i->order_id;?>"
                                                                        class="btn btn-sm btn-secondary btn-circle"><i
                                                                            class="fa fa-truck"></i></a>
                                                                    <?php elseif($i->order_status === 'KIRIM'): ?>
                                                                    <a title="Done" data-toggle="modal"
                                                                        data-target="#ModalSelesai<?php echo $i->order_id;?>"
                                                                        class="btn btn-sm btn-secondary btn-circle"><i
                                                                            class="fa fa-smile-o"></i></a>
                                                                    <a title="Ubah Kirim" data-toggle="modal"
                                                                        data-target="#ModalEditKirim<?php echo $i->order_id;?>"
                                                                        class="btn btn-sm btn-secondary btn-circle"><i
                                                                            class="fa fa-truck"></i></a>
                                                                    <?php else: ?>
                                                                    <b>- not available -</b>
                                                                    <?php endif; ?>
                                                                </td>
                                                            </tr>
                                                            <?php endforeach; ?>

                                                        </tbody>
                                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>


                </div>
                <!-- END Page Content -->
            </main>
            <!-- END Main Container -->
            <!-- Footer -->
            <footer id="page-footer" class="opacity-0">
                <div class="content py-20 font-size-xs clearfix">
                    <div class="float-right">
                        Created with <i class="fa fa-heart text-pulse"></i> by <a class="font-w600" href="http://mfikri.com" target="_blank">Gesrel Schwarzenegger</a>
                    </div>
                    <div class="float-left">
                        <a class="font-w600" href="https://.com" target="_blank">Geysler</a> &copy; <span class="js-year-copy">2017</span>
                    </div>
                </div>
            </footer>
            <!-- END Footer -->
        </div>
        <!-- END Page Container -->


        <!-- Modal Edit Order -->
        <?php foreach($order->result_array() as $i): 
            $order_id = $i['order_id'];
            $order_service_id = $i['order_service_id'];
            $order_size_id = $i['order_size_id'];
            $order_calligraphy = $i['order_calligraphy'];
            $order_img = $i['order_img'];
            $order_address_id = $i['order_address_id'];
            $order_courier_id = $i['order_courier_id'];
            $order_deskripsi = $i['order_deskripsi'];
            ?>
        <form action="<?php echo base_url().'user/order/update_order'?>" method="post" enctype="multipart/form-data">
            <div class="modal" id="ModalEditOrder<?php echo $order_id; ?>" tabindex="-1" role="dialog"
                aria-labelledby="modal-normal" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="block block-themed block-transparent mb-0" style="padding:20px">
                            <div class="block-header bg-primary-dark">
                                <h3 class="block-title">Edit Order</h3>
                            </div></br>
                            <div class="block-content">
                                <div class="form-group">
                                    <input type="hidden" value="<?php echo $order_id;?>" name="xkode">
                                    <select name="xservice" id="" class="form-control" required>
                                        <?php $service = $this->db->query("SELECT * FROM compare where kd_compare='$order_service_id'");
                                        foreach($service->result() as $serv):?>
                                        <option value="<?php echo $serv->kd_compare;?>"><?php echo $serv->type; ?>
                                        </option>
                                        <?php endforeach;?>
                                        <?php foreach($Allroom->result() as $i): ?>
                                        <option value="<?php echo $i->kd_compare;?>"><?php echo $i->type;?></option>
                                        <?php endforeach; ?>
                                    </select>

                                </div>
                                <div class="form-group">
                                    <select name="xsize" class="form-control" id="">
                                        <?php $querySize = $this->db->query("SELECT * FROM size where size_id='$order_size_id'");
                                        foreach($querySize->result() as $size):?>
                                        <option value="<?php echo $size->size_id;?>"><?php echo $size->size_nama; ?> |
                                            <?php echo $size->size_ukuran; ?></option>
                                        <?php endforeach;?>
                                        <?php foreach($Allsize->result() as $i): ?>
                                        <option value="<?php echo $i->size_id;?>"><?php echo $i->size_nama;?> |
                                            <?php echo $i->size_ukuran;?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <input style="height:400px" type="file" name="filefoto2" class="dropify2"
                                        data-default-file="<?php echo base_url().'assets/images/'.$order_img;?>">
                                </div>
                                <div class="form-group">
                                    <?php if($order_calligraphy == ""): ?>
                                    <input name="xcalligraphy" style="text-transform:uppercase" class="form-control"
                                        type="text" value="" placeholder="- not set -">
                                    <?php else: ?>
                                    <input name="xcalligraphy" style="text-transform:uppercase" class="form-control"
                                        type="text" value="<?php echo $order_calligraphy; ?>">
                                    <?php endif; ?>
                                </div>
                                <div class="form-group">
                                    <?php if($order_deskripsi == ""): ?>
                                    <textarea name="xdeskripsi" class="form-control" type="text" value=""
                                        placeholder="Addtional - not set -"></textarea>
                                    <?php else: ?>
                                    <textarea name="xdeskripsi" class="form-control" type="text"
                                        value="<?php echo $order_deskripsi; ?>"><?php echo $order_deskripsi; ?></textarea>
                                    <?php endif; ?>
                                </div>

                                <div class="form-group">
                                    <select name="xcourier" id="" class="form-control">
                                        <?php $courier = $this->db->query("SELECT * FROM courier where courier_id='$order_courier_id'");
                                        foreach($courier->result() as $cour):?>
                                        <option value="<?php echo $cour->courier_id;?>">
                                            <?php echo $cour->courier_nama; ?></option>
                                        <?php endforeach;?>
                                        <?php foreach($Allcourier->result() as $i): ?>
                                        <option value="<?php echo $i->courier_id;?>"><?php echo $i->courier_nama;?>
                                        </option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <select name="xalamat" id="" class="form-control">
                                        <?php $addr = $this->db->query("SELECT * FROM address where address_id='$order_address_id'");
                                        foreach($addr->result() as $addr):?>
                                        <option value="<?php echo $addr->address_id;?>">
                                            <?php echo $addr->address_nama; ?> | <?php echo $addr->address_alamat; ?>
                                        </option>
                                        <?php endforeach;?>
                                        <?php foreach($address->result() as $i): ?>
                                        <option value="<?php echo $i->address_id;?>"><?php echo $i->address_nama;?> |
                                            <?php echo $i->address_alamat; ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default btn-square"
                                data-dismiss="modal">Cancel</button>
                            <button type="submit" class="btn btn-primary btn-square">Save</button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
        <?php endforeach; ?>
        <!-- END Normal Modal -->

        <!-- Modal Paid -->
        <?php foreach($order->result_array() as $i): 
            $order_id = $i['order_id'];
            $order_user_id = $i['order_user_id'];
            $order_number = $i['order_number'];
            ?>
        <form action="<?php echo base_url().'admin/order/paid_order'?>" method="post">
            <div class="modal" id="ModalPaid<?php echo $order_id; ?>" tabindex="-1" role="dialog"
                aria-labelledby="modal-normal" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="block block-themed block-transparent mb-0" style="padding:20px">
                            <div class="block-header bg-primary-dark">
                                <h3 class="block-title">Order Payment</h3>
                                <div class="block-options">
                                    <button type="button" class="btn btn-xs pull-right" data-dismiss="modal"
                                        aria-label="Close">
                                        <i class="fa fa-remove"></i>
                                    </button>
                                </div>
                            </div>
                            <div class="block-content">
                                <div class="form-group">
                                    <input type="hidden" name="xkode" value="<?php echo $order_id; ?>" required>
                                    <input type="hidden" name="xorderNumber" value="<?php echo $order_number; ?>" required>

                                    <?php $queryInv = $this->db->query("SELECT * FROM invoice where invoice_order_id = '$order_id'");
                                        foreach($queryInv->result() as $i):?>
                                    <input type="hidden" name="xInvoiceId" value="<?php echo $i->invoice_id; ?>" required>
                                    <input type="hidden" name="xTotal" value="<?php echo $i->invoice_total; ?>" required>
                                    <?php endforeach; ?>
                                    <?php $quser = $this->db->query("SELECT * FROM user where user_id = '$order_user_id'");
                                        foreach($quser->result() as $qu):?>
                                    <input type="hidden" name="xuserEmail" value="<?php echo $qu->user_email; ?>" required>
                                    <?php endforeach; ?>
                                    <input type="hidden" name="xuserId" value="<?php echo $order_user_id; ?>" required>
                                    <input type="number" class="form-control" name="xAmountPayment" placeholder="Payment Rp...." required>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default btn-square" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary btn-square">Yes</button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
        <?php endforeach; ?>
        <!-- END Normal Modal -->

        <!-- Modal Kirim -->
        <?php foreach($order->result_array() as $i): 
            $order_id = $i['order_id'];
            $order_user_id = $i['order_user_id'];
            $order_number = $i['order_number']; ?>
        <form action="<?php echo base_url().'admin/order/kirim_order'?>" method="post">
            <div class="modal" id="ModalKirim<?php echo $order_id; ?>" tabindex="-1" role="dialog"
                aria-labelledby="modal-normal" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="block block-themed block-transparent mb-0" style="padding:20px">
                            <div class="block-header bg-primary-dark">
                                <h3 class="block-title">Kirim Order</h3>
                                <div class="block-options">
                                    <button type="button" class="btn btn-circle pull-right" data-dismiss="modal"
                                        aria-label="Close">
                                        <i class="fa fa-remove"></i>
                                    </button>
                                </div>
                            </div>
                            <div class="block-content">
                                <div class="form-group">
                                    <input type="hidden" name="xkode" value="<?php echo $order_id; ?>" required>
                                    <input type="hidden" name="xuserId" value="<?php echo $order_user_id; ?>" required>
                                    <input type="hidden" name="xorderNumber" value="<?php echo $order_number; ?>" required>
                                    <?php $quser = $this->db->query("SELECT * FROM user where user_id = '$order_user_id'");
                                        foreach($quser->result() as $qu):?>
                                    <input type="hidden" name="xuserEmail" value="<?php echo $qu->user_email; ?>" required>
                                    <?php endforeach; ?>

                                    <input type="text" class="form-control" name="xresi" placeholder="No Resi/Tracking..." required>
                                </div>
                                <div class="form-group">
                                    <input type="text" class="form-control" name="xdriver" placeholder="Nama Driver..." required>
                                </div>
                                <div class="form-group">
                                    <input type="number" class="form-control" name="xkontakdriver" placeholder="No Telp Driver" required>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default btn-square" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary btn-square">Yes</button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
        <?php endforeach; ?>
        <!-- END Normal Modal -->

        <!-- Modal EditKirim -->
        <?php foreach($order->result_array() as $i): 
            $order_id = $i['order_id'];
            $order_user_id = $i['order_user_id'];
            $order_number = $i['order_number'];
            $order_resi_kirim = $i['order_resi_kirim'];
            $order_pengirim = $i['order_pengirim']; ?>
        <form action="<?php echo base_url().'admin/order/Editkirim_order'?>" method="post">
            <div class="modal" id="ModalEditKirim<?php echo $order_id; ?>" tabindex="-1" role="dialog"
                aria-labelledby="modal-normal" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="block block-themed block-transparent mb-0" style="padding:20px">
                            <div class="block-header bg-primary-dark">
                                <h3 class="block-title">Ubah Kirim Order</h3>
                                <div class="block-options">
                                    <button type="button" class="btn btn-circle pull-right" data-dismiss="modal"
                                        aria-label="Close">
                                        <i class="fa fa-remove"></i>
                                    </button>
                                </div>
                            </div>
                            <div class="block-content">
                                <div class="form-group">
                                    <input type="hidden" name="xkode" value="<?php echo $order_id; ?>" required>
                                    <input type="hidden" name="xuserId" value="<?php echo $order_user_id; ?>" required>
                                    <input type="hidden" name="xorderNumber" value="<?php echo $order_number; ?>" required>
                                    <?php $quser = $this->db->query("SELECT * FROM user where user_id = '$order_user_id'");
                                        foreach($quser->result() as $qu):?>
                                    <input type="hidden" name="xuserEmail" value="<?php echo $qu->user_email; ?>" required>
                                    <?php endforeach; ?>
                                    <input type="text" class="form-control" name="xresi" placeholder="No Resi/Tracking..." value="<?php echo $order_resi_kirim; ?>" required>
                                </div>
                                <div class="form-group">
                                    <input type="text" class="form-control" name="xdriver" placeholder="Nama Driver..." value="<?php echo $order_pengirim; ?>" required>
                                </div>

                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default btn-square" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary btn-square">Yes</button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
        <?php endforeach; ?>
        <!-- END Normal Modal -->

        <!-- Modal Selesai -->
        <?php foreach($order->result_array() as $i): 
            $order_id = $i['order_id'];
            $order_user_id = $i['order_user_id'];
            $order_number = $i['order_number']; ?>
        <form action="<?php echo base_url().'admin/order/selesai_order'?>" method="post">
            <div class="modal" id="ModalSelesai<?php echo $order_id; ?>" tabindex="-1" role="dialog"
                aria-labelledby="modal-normal" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="block block-themed block-transparent mb-0" style="padding:20px">
                            <div class="block-header bg-primary-dark">
                                <h3 class="block-title">Selesaikan Order</h3>
                                <div class="block-options">
                                    <button type="button" class="btn btn-circle pull-right" data-dismiss="modal"
                                        aria-label="Close">
                                        <i class="fa fa-remove"></i>
                                    </button>
                                </div>
                            </div>
                            <div class="block-content">
                                <div class="form-group">
                                    <input type="hidden" name="xkode" value="<?php echo $order_id; ?>" required>
                                    <input type="hidden" name="xuserId" value="<?php echo $order_user_id; ?>" required>
                                    <input type="hidden" name="xorderNumber" value="<?php echo $order_number; ?>" required>
                                    <?php $quser = $this->db->query("SELECT * FROM user where user_id = '$order_user_id'");
                                        foreach($quser->result() as $qu):?>
                                    <input type="hidden" name="xuserEmail" value="<?php echo $qu->user_email; ?>" required>
                                    <?php endforeach; ?>
                                </div>
                                <div class="alert alert-danger">
                                    <p>Pastikan anda telah mengkonfirmasi pesana telah sampai tujuan dengan customer!!</p>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default btn-square" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary btn-square">Yes</button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
        <?php endforeach; ?>
        <!-- END Normal Modal -->


        <!-- Modal Cancel -->
        <?php foreach($order->result_array() as $i): 
            $order_id = $i['order_id']; ?>
        <form action="<?php echo base_url().'admin/order/cancel_order'?>" method="post">
            <div class="modal" id="ModalCancel<?php echo $order_id; ?>" tabindex="-1" role="dialog"
                aria-labelledby="modal-normal" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="block block-themed block-transparent mb-0" style="padding:20px">
                            <div class="block-header bg-primary-dark">
                                <h3 class="block-title">Cancel Order</h3>
                                <div class="block-options">
                                    <button type="button" class="btn btn-xs pull-right" data-dismiss="modal"
                                        aria-label="Close">
                                        <i class="fa fa-remove"></i>
                                    </button>
                                </div>
                            </div>
                            <div class="block-content">
                                <p>Are you sure want to cancel this order?</p>
                                <input type="hidden" name="xkode" value="<?php echo $order_id; ?>" required>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default btn-square" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary btn-square">Yes</button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
        <?php endforeach; ?>
        <!-- END Normal Modal -->

        <!-- Modal Cancel2 -->
        <?php foreach($order->result_array() as $i): 
            $order_id = $i['order_id']; ?>
        <form action="<?php echo base_url().'admin/order/cancel_order'?>" method="post">
            <div class="modal" id="ModalCancel2<?php echo $order_id; ?>" tabindex="-1" role="dialog"
                aria-labelledby="modal-normal" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="block block-themed block-transparent mb-0" style="padding:20px">
                            <div class="block-header bg-primary-dark">
                                <h3 class="block-title">Cancel Order</h3>
                                <div class="block-options">
                                    <button type="button" class="btn btn-xs pull-right" data-dismiss="modal"
                                        aria-label="Close">
                                        <i class="fa fa-remove"></i>
                                    </button>
                                </div>
                            </div>
                            <div class="block-content">
                                <p>Are you sure want to cancel this order?</p>
                                <input type="hidden" name="xkode" value="<?php echo $order_id; ?>" required>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default btn-square" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary btn-square">Yes</button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
        <?php endforeach; ?>
        <!-- END Normal Modal -->

        <?php foreach($order->result_array() as $i): 
            $order_id = $i['order_id'];
            $order_user_id = $i['order_user_id'];
            $order_number = $i['order_number'];
            $order_service_id = $i['order_service_id'];
            $order_size_id = $i['order_size_id'];
            $order_calligraphy = $i['order_calligraphy'];
            $order_img = $i['order_img'];
            $order_address_id = $i['order_address_id'];
            $order_courier_id = $i['order_courier_id'];
            $order_deskripsi = $i['order_deskripsi'];
            $order_tgl = $i['order_tgl'];?>
        <form action="<?php echo base_url().'admin/order/confirm_order'?>" method="post">
            <div class="modal" id="ModalConfirm<?php echo $order_id; ?>" tabindex="-1" role="dialog"
                aria-labelledby="modal-normal" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="block block-themed block-transparent mb-0" style="padding:20px">
                            <div class="block-header bg-primary-dark">
                                <h3 class="block-title">Confirm Order</h3>
                                <div class="block-options">
                                </div>
                            </div>
                            <div class="block-content">
                                <input type="hidden" name="xkode" value="<?php echo $order_id; ?>" required>
                                <input type="hidden" name="xuserId" value="<?php echo $order_user_id; ?>" required>

                                <input type="hidden" name="xorderNumber" value="<?php echo $order_number; ?>" required>
                                <?php $qaddr = $this->db->query("SELECT * FROM address where address_user_id='$order_user_id'");
                                    foreach($qaddr->result() as $qa):?>
                                    <input type="hidden" name="xAddressUser" value="<?php echo $qa->address_nama; ?> | <?php echo $qa->address_alamat; ?>">
                                <?php endforeach; ?>
                                <input type="hidden" name="xorderAddress" value="<?php echo $order_address_id; ?>" required>
                                <input type="hidden" name="xorderDeskripsi" value="<?php echo $order_number; ?>" required>
                                <input type="hidden" name="xorderImg" value="<?php echo $order_img; ?>" required>
                                <input type="hidden" name="xorderTgl" value="<?php echo $order_tgl; ?>" required>





                                <?php $quser = $this->db->query("SELECT * FROM user where user_id='$order_user_id'");
                                    foreach($quser->result() as $qu):?>
                                    <input type="hidden" name="xemailUser" value="<?php echo $qu->user_email; ?>">
                                    <input type="hidden" name="xsaldoUser" value="<?php echo $qu->user_saldo; ?>">
                                <?php endforeach; ?>
                                <?php $qservice = $this->db->query("SELECT * FROM compare where kd_compare='$order_service_id'");
                                    foreach($qservice->result() as $p):?>
                                    <input type="hidden" name="xnamaService" value="<?php echo $p->type; ?>">
                                    <input type="hidden" name="xserviceAmount" value="<?php echo $p->rate; ?>">
                                <?php endforeach; ?>
                                <?php $qsize = $this->db->query("SELECT * FROM size where size_id='$order_size_id'");
                                    foreach($qsize->result() as $q):?>
                                    <input type="hidden" name="xnamaSize" value="<?php echo $q->size_nama; ?>">
                                    <input type="hidden" name="xsizeAmount" value="<?php echo $q->size_harga; ?>">
                                <?php endforeach; ?>
                                <div class="form-group">
                                    <textarea class="form-control" type="text" name="xnamaOtherAmount" placeholder="Other Payment" required></textarea>
                                </div>
                                <div class="form-group">
                                    <input class="form-control" type="number" name="xotherAmount" placeholder="Other Amount" required>
                                </div>
                                <div class="form-group">
                                    <select class="form-control" name="xnamaShipping"  required>
                                        <option value="">- Shippment Name -</option>
                                        <?php foreach($Allcourier->result() as $i): ?>
                                        <option value="<?php echo $i->courier_nama;?>"><?php echo $i->courier_nama;?>
                                        </option>
                                        <?php endforeach; ?>
                                    </select>                              
                                 </div>
                                <div class="form-group">
                                    <input class="form-control" type="number" name="xshippingAmount" placeholder="Shipping Amount" required>
                                </div>
                                <div class="form-group">
                                    <div class="alert alert-danger">
                                        <p>Make sure that you already contact the customer for Order Details</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default btn-square" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary btn-square">Confirm</button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
        <?php endforeach; ?>
        <!-- END Normal Modal -->

        <!-- Modal Detail -->
        <?php foreach($order->result_array() as $i): 
            $order_id = $i['order_id'];
            $order_number = $i['order_number'];
            $order_service_id = $i['order_service_id'];
            $order_size_id = $i['order_size_id'];
            $order_calligraphy = $i['order_calligraphy'];
            $order_img = $i['order_img'];
            $order_address_id = $i['order_address_id'];
            $order_tgl = $i['order_tgl'];
            $order_deskripsi = $i['order_deskripsi']; ?>
        <div class="modal" id="ModalDetail<?php echo $order_id; ?>" tabindex="-1" role="dialog"
            aria-labelledby="modal-normal" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="block block-themed block-transparent mb-0" style="padding:20px">
                        <div class="block-header bg-primary-dark">
                        <h3 class="block-title">Order Detail</h3>
                            <div class="block-options">
                            </div>
                        </div>
                        <div class="block-content">
                            <div class="row">
                                <div class="col-md-4">
                                    <a href="<?php echo base_url().'assets/images/'.$order_img;?>" target="_blank"><img style="padding-right:20px" width="150px" src="<?php echo base_url().'assets/images/'.$order_img;?>" alt=""></a>
                                </div>
                                <div class="col-md-8">
                                    <table class="">
                                        <tr>
                                            <th style="width:120px">Order Number</th>
                                            <td>: <?php echo $order_number; ?></td>
                                        </tr>
                                        <tr>
                                            <th style="width:120px">Service</th>


                                            <td>:
                                                <?php $qserv = $this->db->query("SELECT * FROM compare where kd_compare='$order_service_id'");
                                            foreach($qserv->result() as $serv):
                                                echo $serv->type;
                                            endforeach;?>
                                            </td>


                                        </tr>
                                        <tr>
                                            <th style="width:120px">Size</th>
                                            <td>:
                                                <?php $qsize = $this->db->query("SELECT * FROM size where size_id='$order_size_id'");
                                            foreach($qsize->result() as $size):?>
                                                <?php echo $size->size_nama;?> |
                                                <?php echo $size->size_ukuran;?>
                                                <?php endforeach;?>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th style="width:120px">Courier</th>
                                            <td>:
                                                <?php $qcourier = $this->db->query("SELECT * FROM courier where courier_id='$order_courier_id'");
                                            foreach($qcourier->result() as $cour):
                                                echo $cour->courier_nama;
                                            endforeach;?>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th style="width:120px">Ship Address</th>
                                            <td>:
                                                <?php $qaddr = $this->db->query("SELECT * FROM address where address_id='$order_address_id'");
                                            foreach($qaddr->result() as $addr):
                                                echo $addr->address_nama;
                                            endforeach;?>
                                            </td>
                                        </tr>

                                        <tr>
                                            <th style="width:120px">Date </th>
                                            <td>: <?php echo date("d M y, H:i A",strtotime($order_tgl)); ?></td>
                                        </tr>
                                        <tr>
                                            <th style="width:120px">Comfirmed Date</th>
                                            <td>: <?php echo $order_address_id; ?></td>
                                        </tr>
                                    </table>
                                </div>
                                <div class="col-md-12"></br>
                                <h6 class="pull-left">Calligraphy : <b>
                                        <?php if($order_calligraphy == ""): ?>
                                        <i>- not set -</i>
                                        <?php else :?>
                                        "<?php echo $order_calligraphy; ?>"
                                        <?php endif; ?></b>
                                </h6></br></br>
                                <h6>Additional</h6>
                                <p><?php if($order_deskripsi == ""): ?>
                                    <i>- not set -</i>
                                    <?php else: ?>
                                    <?php echo $order_deskripsi; ?>
                                    <?php endif; ?></p>
                            </div>

                            </div>
                           
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default btn-square" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
        <?php endforeach; ?>
        <!-- END Normal Modal -->


        <!-- Codebase Core JS -->
        <script src="<?php echo base_url().'assets/js/core/jquery.min.js'?>"></script>
        <script src="<?php echo base_url().'assets/js/core/popper.min.js'?>"></script>
        <script src="<?php echo base_url().'assets/js/core/bootstrap.min.js'?>"></script>
        <script src="<?php echo base_url().'assets/js/core/jquery.slimscroll.min.js'?>"></script>
        <script src="<?php echo base_url().'assets/js/core/jquery.scrollLock.min.js'?>"></script>
        <script src="<?php echo base_url().'assets/js/core/jquery.appear.min.js'?>"></script>
        <script src="<?php echo base_url().'assets/js/core/jquery.countTo.min.js'?>"></script>
        <script src="<?php echo base_url().'assets/js/core/js.cookie.min.js'?>"></script>
        <script src="<?php echo base_url().'assets/js/codebase.js'?>"></script>
        <script src="<?php echo base_url().'assets/ckeditor/ckeditor.js'?>"></script>
        <script src="<?php echo base_url().'assets/js/plugins/datatables/jquery.dataTables.min.js'?>"></script>
        <script src="<?php echo base_url().'assets/js/plugins/datatables/dataTables.bootstrap4.min.js'?>"></script>
        <script src="<?php echo base_url().'assets/js/dropify.min.js'?>"></script>
        <script type="text/javascript" src="<?php echo base_url().'assets/plugins/toast/jquery.toast.min.js'?>"></script>
        <?php if($this->session->flashdata('msg')=='gagal-email'):?>
            <script type="text/javascript">
                    $.toast({
                        heading: 'Gagal!',
                        text: "Gagal memproses! periksa kembali koneksi internet anda.",
                        showHideTransition: 'slide',
                        icon: 'error',
                        hideAfter: false,
                        position: 'bottom-right',
                        bgColor: '#FF4859'
                    });
            </script>
        <?php elseif($this->session->flashdata('msg')=='gagal-bayar'):?>
            <script type="text/javascript">
                    $.toast({
                        heading: 'Gagal!',
                        text: "Gagal memproses! pembayaran kurang.",
                        showHideTransition: 'slide',
                        icon: 'error',
                        hideAfter: false,
                        position: 'bottom-right',
                        bgColor: '#FF4859'
                    });
            </script>
        <?php elseif($this->session->flashdata('msg')=='cancel'):?>
            <script type="text/javascript">
                    $.toast({
                        heading: 'Sukses',
                        text: "Pesanan berhasil dibatalkan.",
                        showHideTransition: 'slide',
                        icon: 'warning',
                        hideAfter: false,
                        position: 'bottom-right',
                        bgColor: '#FFC017'
                    });
            </script>
        <?php elseif($this->session->flashdata('msg')=='selesai'):?>
            <script type="text/javascript">
                    $.toast({
                        heading: 'Sukses',
                        text: "Pesanan berhasil dan selesai.",
                        showHideTransition: 'slide',
                        icon: 'warning',
                        hideAfter: false,
                        position: 'bottom-right',
                        bgColor: '#FFC017'
                    });
            </script>
        <?php elseif($this->session->flashdata('msg')=='konfirmasi'):?>
            <script type="text/javascript">
                    $.toast({
                        heading: 'Sukses',
                        text: "Pesanan berhasil dikonfirmasi.",
                        showHideTransition: 'slide',
                        icon: 'success',
                        hideAfter: false,
                        position: 'bottom-right',
                        bgColor: '#7EC857'
                    });
            </script>
        <?php elseif($this->session->flashdata('msg')=='bayar'):?>
            <script type="text/javascript">
                    $.toast({
                        heading: 'Sukses',
                        text: "Pesanan berhasil dibayarkan.",
                        showHideTransition: 'slide',
                        icon: 'success',
                        hideAfter: false,
                        position: 'bottom-right',
                        bgColor: '#7EC857'
                    });
            </script>
        <?php elseif($this->session->flashdata('msg')=='kirim'):?>
            <script type="text/javascript">
                    $.toast({
                        heading: 'Sukses',
                        text: "Info pengiriman berhasil",
                        showHideTransition: 'slide',
                        icon: 'info',
                        hideAfter: false,
                        position: 'bottom-right',
                        bgColor: '#00C9E6'
                    });
            </script>
        <?php elseif($this->session->flashdata('msg')=='edit-kirim'):?>
            <script type="text/javascript">
                    $.toast({
                        heading: 'Sukses',
                        text: "Info pengiriman berhasil diubah",
                        showHideTransition: 'slide',
                        icon: 'success',
                        hideAfter: false,
                        position: 'bottom-right',
                        bgColor: '#7EC857'
                    });
            </script>
        <?php elseif($this->session->flashdata('msg')=='edit-pesanan'):?>
            <script type="text/javascript">
                    $.toast({
                        heading: 'Sukses',
                        text: "Info pesanan berhasil diubah",
                        showHideTransition: 'slide',
                        icon: 'success',
                        hideAfter: false,
                        position: 'bottom-right',
                        bgColor: '#7EC857'
                    });
            </script>
        <?php elseif($this->session->flashdata('msg')=='show-modal'):?>
            <script type="text/javascript">
                    $('#ModalResetPassword').modal('show');
            </script>
        <?php else:?>

        <?php endif;?>

    <script type="text/javascript">
    $(document).ready(function() {
        $('.dropify').dropify({
            messages: {
                default: 'Image 900 X 400 Pixels',
                replace: 'Ganti',
                remove: 'Hapus',
                error: 'error'
            }
        });

        $('.dropify2').dropify({
            messages: {
                default: 'Image 700 X 700 Pixels',
                replace: 'Ganti',
                remove: 'Hapus',
                error: 'error'
            }
        });
        $('.dropify3').dropify({
            messages: {
                default: 'Image 420 X 420 Pixels',
                replace: 'Ganti',
                remove: 'Hapus',
                error: 'error'
            }
        });
    });
    </script>


        <script type="text/javascript">
            $(document).ready(function() {
                $('#mytable').DataTable();

                //Show Modal Add New
                $('#btn-add-new').on('click',function(){
                    $('#ModalAddNew').modal('show');
                });

                //Show Modal Update order
                $('.btn-edit').on('click',function(){
                    var order_id=$(this).data('id');
                    var order_nama=$(this).data('order');
                    $('#ModalUpdate').modal('show');
                    $('[name="xkode"]').val(order_id);
                    $('[name="xorder2"]').val(order_nama);
                });

                //Show Konfirmasi modal hapus record
                $('.btn-hapus').on('click',function(){
                    var order_id=$(this).data('id');
                    $('#Modalhapus').modal('show');
                    $('[name="kode"]').val(order_id);
                });

            });
            
        </script>

    </body>
</html>
