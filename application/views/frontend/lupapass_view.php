<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <!-- TITLE -->
    <title>Potrello | Lupa Password</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <meta name="format-detection" content="telephone=no">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <link rel="shortcut icon" href="<?php echo base_url() . 'theme/images/favicon.png' ?>"/>
	<meta name="description" content="Potrello Sketch">
		<meta property="og:image" content="<?php echo base_url().'theme/images/logo1.png'?>" />
    <meta property="og:image:width" content="460" />
    <meta property="og:image:height" content="440" />

	<!-- META FOR IOS & HANDHELD -->
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
	<meta name="HandheldFriendly" content="true" />
	<meta name="apple-mobile-web-app-capable" content="YES" />
	<!-- //META FOR IOS & HANDHELD -->

    <!-- GOOGLE FONT -->
    <link href='https://fonts.googleapis.com/css?family=Hind:400,300,500,600%7cMontserrat:400,700' rel='stylesheet' type='text/css'>


    <!-- CSS LIBRARY -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() . 'theme/css/lib/font-awesome.min.css' ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() . 'theme/css/lib/font-lotusicon.css' ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() . 'theme/css/lib/bootstrap.min.css' ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() . 'theme/css/lib/owl.carousel.css' ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() . 'theme/css/lib/jquery-ui.min.css' ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() . 'theme/css/lib/magnific-popup.css' ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() . 'theme/css/lib/settings.css' ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() . 'theme/css/lib/bootstrap-select.min.css' ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() . 'theme/css/helper.css' ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() . 'theme/css/custom.css' ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() . 'theme/css/responsive.css' ?>">


    <!-- MAIN STYLE -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() . 'theme/css/style.css' ?>">
    <link rel="stylesheet" href="<?php echo base_url() . 'theme/css/jssocials.css' ?>">
    <link rel="stylesheet" href="<?php echo base_url() . 'theme/css/jssocials-theme-flat.css' ?>">

</head>

<body>


    <!-- PRELOADER -->
    <div id="preloader">
        <span class="preloader-dot"></span>
    </div>
    <!-- END / PRELOADER -->

    <!-- PAGE WRAP -->
    <div id="page-wrap">

        <!-- HEADER -->
        <header id="header" class="header-v2">

            <!-- HEADER TOP -->
           <?php $this->load->view('frontend/headertop');?>
            <!-- END / HEADER TOP -->

            <!-- HEADER LOGO & MENU -->
          <?php $this->load->view('frontend/header');?>

			<!-- END / HEADER LOGO & MENU -->

        </header>
        <!-- END / HEADER -->

        <!--BANNER -->
        <section class="section-sub-banner bg-9">
            <div></div>
            <div class="sub-banner">
                <div class="container">
                    <div class="text text-center">
                    </div>
                </div>

            </div>

        </section>
        <!-- END BANNER -->

        <!-- CONTACT -->
        <section class="section-contact">
            <div class="container">
                <div class="contact">
                    <div class="row">
                        <div class="col-md-6 col-lg-5">

                            <div class="text">

                                <h2>Lupa Kata Sandi</h2>
                                <p>Verify Email anda yang terdaftar untuk reset kata sandi anda.</p>
                            </div>
                        </div>
                        <div class="col-md-6 col-lg-6 col-lg-offset-1">
                            <div class="contact-form">
                                <form action="<?php echo site_url('login/lupa_password'); ?>" method="post">
                                <div id="contact-content"><?php echo $this->session->flashdata('msg'); ?></div>
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <input type="email" class="field-text" name="email" placeholder="Alamat Email" required>
                                        </div>
                                        <div class="col-sm-6">
                                            <button type="submit" class="awe-btn awe-btn-14">LANJUT</button>
                                        </div>
                                    </div>
                                    <div class="row">
                                    <div class="col-xs-6">
                                        <div style="padding-top:30px">
                                            <a href="<?php echo base_url() . 'register' ?>" ><i class="fa fa-user-plus"></i> Daftar Disini</a>
                                        </div>
                                     </div>
                                     <div class="col-xs-6">
                                        <div style="padding-top:30px">
                                            <a href="<?php echo base_url().'login';?>" ><i class="fa fa-sign-in"></i> Kembali ke Login</a>
                                        </div>
                                     </div>
                                    </div>
                                </form>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </section>
        <!-- END / CONTACT -->



        <!-- FOOTER -->
        <?php $this->load->view('frontend/footer');?>
        <!-- END / FOOTER -->

    </div>
    <!-- END / PAGE WRAP -->

    <script type="text/javascript" src="<?php echo base_url() . 'theme/js/lib/jquery-1.11.0.min.js' ?>"></script>
    <script type="text/javascript" src="<?php echo base_url() . 'theme/js/lib/jquery-ui.min.js' ?>"></script>
    <script type="text/javascript" src="<?php echo base_url() . 'theme/js/lib/bootstrap.min.js' ?>"></script>
    <script type="text/javascript" src="<?php echo base_url() . 'theme/js/lib/bootstrap-select.js' ?>"></script>
    <script type="text/javascript" src="<?php echo base_url() . 'theme/js/lib/isotope.pkgd.min.js' ?>"></script>
    <script type="text/javascript" src="<?php echo base_url() . 'theme/js/lib/jquery.themepunch.revolution.min.js' ?>"></script>
    <script type="text/javascript" src="<?php echo base_url() . 'theme/js/lib/jquery.themepunch.tools.min.js' ?>"></script>
    <script type="text/javascript" src="<?php echo base_url() . 'theme/js/lib/owl.carousel.js' ?>"></script>
    <script type="text/javascript" src="<?php echo base_url() . 'theme/js/lib/jquery.appear.min.js' ?>"></script>
    <script type="text/javascript" src="<?php echo base_url() . 'theme/js/lib/jquery.countTo.js' ?>"></script>
    <script type="text/javascript" src="<?php echo base_url() . 'theme/js/lib/jquery.countdown.min.js' ?>"></script>
    <script type="text/javascript" src="<?php echo base_url() . 'theme/js/lib/jquery.parallax-1.1.3.js' ?>"></script>
    <script type="text/javascript" src="<?php echo base_url() . 'theme/js/lib/jquery.magnific-popup.min.js' ?>"></script>

    <script type="text/javascript" src="<?php echo base_url() . 'theme/js/lib/jquery.form.min.js' ?>"></script>
    <script type="text/javascript" src="<?php echo base_url() . 'theme/js/lib/jquery.validate.min.js' ?>"></script>

    <script type="text/javascript" src="<?php echo base_url() . 'theme/js/scripts.js' ?>"></script>
    <script type="text/javascript">
        $(document).ready(function(){
            $("#sharePopup").jsSocials({
                    showCount: true,
                    showLabel: true,
                    shareIn: "popup",
                    shares: [
                    { share: "twitter", label: "Twitter" },
                    { share: "facebook", label: "Facebook" },
                    { share: "googleplus", label: "Google+" },
                    { share: "linkedin", label: "Linked In" },
                    { share: "pinterest", label: "Pinterest" },
                    { share: "whatsapp", label: "Whats App" }
                    ]
            });
        });
    </script>

</body>
</html>
