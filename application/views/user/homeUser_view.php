<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <!-- TITLE -->
    <title>Situs Lukisan Realis Berkualitas | Potrello</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <meta name="format-detection" content="telephone=no">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <link rel="shortcut icon" href="<?php echo base_url().'theme/images/favicon.png'?>" />
    <meta name="description" content="Potrello Sketch">

    <!-- META FOR IOS & HANDHELD -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta name="HandheldFriendly" content="true" />
    <meta name="apple-mobile-web-app-capable" content="YES" />
    <!-- //META FOR IOS & HANDHELD -->

    <!-- GOOGLE FONT -->
    <link href='https://fonts.googleapis.com/css?family=Hind:400,300,500,600%7cMontserrat:400,700' rel='stylesheet'
        type='text/css'>
        <link href='https://fonts.googleapis.com/css?family=Oleo+Script' rel='stylesheet' type='text/css'>

    <!-- CSS LIBRARY -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url().'theme/css/lib/font-awesome.min.css'?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url().'theme/css/lib/font-lotusicon.css'?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url().'theme/css/lib/bootstrap.min.css'?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url().'theme/css/lib/owl.carousel.css'?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url().'theme/css/lib/jquery-ui.min.css'?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url().'theme/css/lib/magnific-popup.css'?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url().'theme/css/lib/settings.css'?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url().'theme/css/lib/bootstrap-select.min.css'?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url().'theme/css/helper.css'?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url().'theme/css/custom.css'?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url().'theme/css/responsive.css'?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url().'theme/css/modal.css'?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url().'theme/css/introSlider.css'?>">
    <!-- MAIN STYLE -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url().'theme/css/style.css'?>">

</head>

<body>

    <!-- PRELOADER -->
    <div id="preloader">
        <span class="preloader-dot"></span>
    </div>
    <?php echo $regIntro;?>
    <!-- END / PRELOADER -->
    <!--BANNER -->
        <section class="section-sub-banner bg-9">
            <div></div>
            <div class="sub-banner">
                <div class="container">
                    <div class="text text-center">
                    </div>
                </div>

            </div>

        </section>
        <!-- END BANNER -->

    <!-- PAGE WRAP -->
    <div id="page-wrap">

        <!-- HEADER -->
        <header id="header" class="header-v2">

            <!-- HEADER TOP -->
            <?php $this->load->view('user/headertop');?>
            <!-- END / HEADER TOP -->

            <!-- HEADER LOGO & MENU -->
            <?php $this->load->view('user/header');?>
            <!-- END / HEADER LOGO & MENU -->

        </header>
        <!-- END / HEADER -->

        <!-- BANNER SLIDER -->
        <section class="section-slider">
            <h1 class="element-invisible">Slider</h1>
            <div id="slider-revolution">
                <ul>
                    <?php

					foreach($slider as $i):


				?>
                    <li data-transition="fade">
                        <img src="<?php echo base_url().'assets/images/'.$i->gambar;?>" style="filter:brightness(50%)"
                            data-bgposition="left center" data-duration="14000" data-bgpositionend="right center"
                            alt="">

                        <div class="tp-caption sft fadeout slider-caption-sub slider-caption-1" data-x="center"
                            data-y="100" data-speed="700" data-start="1500" data-easing="easeOutBack">
                        </div>

                        <div class="tp-caption sft fadeout slider-caption-sub slider-caption-1" data-x="center"
                            data-y="240" data-speed="700" data-start="1500" data-easing="easeOutBack">
                            <?php echo $i->caption_1;?>
                        </div>

                        <div class="tp-caption sfb fadeout slider-caption slider-caption-sub-1" data-x="center"
                            data-y="280" data-speed="700" data-easing="easeOutBack" data-start="2000">
                            <?php echo $i->caption_2;?> </div>
                        <div class="tp-caption sfb fadeout slider-caption-sub slider-caption-sub-3" data-x="center"
                            data-y="365" data-easing="easeOutBack" data-speed="700" data-start="2200">
                            <?php echo $i->caption_3;?></div>
                    </li>


                    <?php endforeach; ?>


                </ul>
            </div>

        </section>
        <!-- END / BANNER SLIDER -->

        <!-- CHECK AVAILABILITY -->
        <section class="section-check-availability">
            <div class="container">
                <div class="check-availability">
                    <div class="row v-align">
                        <div class="col-lg-3">
                            <h2>Pesan Sekarang</h2>
                        </div>
                        <div class="col-lg-9">
                            <div class="availability-form">
                                <div class="vailability-submit">
                                    <a href="<?php echo site_url('user/order');?>" class="awe-btn awe-btn-13"
                                        style="background-color:#000">Buat
                                        Pesanan</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- END / CHECK AVAILABILITY -->


        <!-- ROOM DETAIL -->
        <section class="section-room-detail bg-white">
            <div class="container">

                <!-- COMPARE ACCOMMODATION -->
                <div class="room-detail_compare">
                    <div class="row">
                        <div class="col col-xs-12 col-lg-6 col-lg-offset-3">
                            <div class="ot-heading row-20 mb30 text-center">
                                <h2 class="shortcode-heading">Koleksi Layanan</h2>
                            </div>
                        </div>
                    </div>

                    <div class="room-compare_content">

                        <div class="row">
                            <!-- ITEM -->

                            <?php
								foreach($rooms->result() as $j):
							?>


                            <div class="col-sm-4 col-md-4 col-lg-4">
                                <div class="room-compare_item">
                                    <div class="img">
                                        <a href="<?php echo site_url('user/order');?>"><img class="img img-responsive"
                                                src="<?php echo base_url().'assets/images/'.$j->gambar_large;?>"
                                                alt="<?php echo $j->type;?>"></a>
                                    </div>

                                    <center>
                                        <div class="text">
                                            <h2><a
                                                    herf="<?php echo site_url('user/order');?>"><?php echo $j->type;?></a>
                                            </h2>
                                            <ul>
                                                <h6>Mulai dari <?php echo 'Rp '.number_format($j->rate);?>,00</h6>
                                            </ul>
                                        </div>
                                    </center>
                                </div>
                            </div>
                            <!-- END / ITEM -->

                            <?php endforeach; ?>



                        </div>
                        <div class="text-center">
                            <a href="<?php echo site_url('user/service');?>"
                                class="awe-btn awe-btn-default font-hind f12 bold btn-medium mt15">Lihat selanjutnya</a>
                        </div>
                        <!-- END / COMPARE ACCOMMODATION -->
                    </div>
        </section>
        <!-- END / SHOP DETAIL -->
        
        <!-- TESTIMONI -->
        <section class="bg-white">
                <div class="container">
                    <div class="content">
                        <div class="row">
                            <div class="col col-xs-12 col-lg-6 col-lg-offset-3">
                                <div class="ot-heading row-20 mb30 text-center">
                                    <h2 class="shortcode-heading">testimoni</h2>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <?php 
                             function limit_words($string, $word_limit){
                                $words = explode(" ",$string);
                                return implode(" ",array_splice($words,0,$word_limit));
                            }
                            foreach($testi->result() as $i): ?>
                            <div class="col-xs-6 col-sm-3">
                                <div class="item">
                                    <div class="img">
                                        <img class="img-full" src="<?php echo base_url().'assets/images/'.$i->gambar?>" alt="" width="">
                                        
                                    </div>
                                    <div class="info" style="width:80%">
                                        <p class="sub font-monserat f12 f-600 upper mt10 mb20"><?php echo $i->nama; ?> <?php echo limit_words($i->detail,10).'<b>...</b>'; ?></p>
                                    </div>
                                </div>
                            </div>
                            <?php endforeach; ?>
                        </div>
                        <div class="text-center">
                           <a href="<?php echo site_url('testimonials');?>"class="awe-btn awe-btn-default font-hind f12 bold btn-medium mt15">Lihat
                             Selanjutnya</a>
                        </div>
                        <br></br>
                    </div>
                </div>
            </section>
            
        <!-- END / TESTIMONI -->
        
        <!-- DAFTAR HARGA -->
        <section class="section-restaurant-4 bg-white">
            <div class="container">
                    <div class="row">
                    <div class="col col-xs-12 col-lg-6 col-lg-offset-3">
                        <div class="ot-heading row-20 mb30 text-center">
                            <h2 class="shortcode-heading">Daftar Harga</h2>
                        </div>
                    </div>
                
                <div class="restaurant-tabs">
                    <div class="tabs tabs-restaurant">
                        
                           <ul>
                            <li><a href="#tabs-layanan">Layanan Gambar<span></span></a></li>
                            <li><a href="#tabs-ukuran">Ukuran Gambar<span></span></a></li>
                            <li><a href="#tabs-kurir">Jasa Pengiriman<span></span></a></li>
                           </ul>
                                <div id="tabs-layanan">
                                    <div class="restaurant_content">
                                        <div class="row">
                                            <?php foreach($rooms->result() as $i): ?>
                                            <!-- ITEM -->
                                            <div class="col-md-6">
                                                <div class="restaurant_item small-thumbs">
                                                    <div class="img">
                                                        <a href="#!"><img src="<?php echo base_url().'assets/images/'.$i->gambar_large;?>" alt=""></a>
                                                    </div>
        
                                                    <div class="text">
                                                        <h2><a href="#!"><?php echo $i->type;?></a></h2>
                                                        <p class="desc">Klik Pemesanan untuk memesan</p>

                                                        <p class="price">
                                                           Rp. <?php echo number_format($i->rate); ?>
                                                           <del><span class="amout">Rp. 225,000</span></del>
                                                        </p>
                                                    </div>
        
                                                </div>
                                            </div>
                                            <?php endforeach ?>
                                        </div>
                                    </div>
                                </div>
                                
                                 <div id="tabs-ukuran">
                                    <div class="restaurant_content">
                                        <div class="row">
                                            <?php 
                                            $querySize = $this->db->query("SELECT * FROM size limit 4");
                                            foreach($querySize->result() as $i): ?>

                                            <!-- ITEM -->
                                            <div class="col-md-6">
                                                <div class="restaurant_item small-thumbs">
                                                    <div class="img">
                                                        <a href="#!"><img src="<?php echo base_url().'theme/images/frame2.png';?>" alt=""></a>
                                                    </div>
                                                    <div class="text">
                                                        <h2><a href="#!"><?php echo $i->size_nama;?> | <?php echo $i->size_ukuran;?></a></h2>
                                                        <p class="desc"><small><?php echo $i->size_keterangan;?></small></p>
                                                        <p class="price">
                                                           <ins><span class="amout">Rp. <?php echo number_format($i->size_harga); ?></span></ins>
                                                            <del><span class="amout">Rp. 100,000</span></del>
                                                        </p>
                                                    </div>
        
                                                </div>
                                            </div>
                                            <?php endforeach; ?>
                                        </div>
                                    </div>
                                </div>
                                
                                 <div id="tabs-kurir">
                                    <div class="restaurant_content">
                                        <div class="row">
                                            <?php 
                                            $queryCourier = $this->db->query("SELECT * FROM courier limit 4");
                                            foreach($queryCourier->result() as $i): ?>

                                            <!-- ITEM -->
                                            <div class="col-md-6">
                                                <div class="restaurant_item small-thumbs">
        
                                                    <div class="img">
                                                        <a href="#!"><img width="200px" src="<?php echo base_url().'assets/images/'.$i->courier_logo;?>" alt=""></a>
                                                    </div>
        
                                                    <div class="text">
                                                        <h2><a href="#!"><?php echo $i->courier_nama;?></a></h2>
                                                        <p class="price">
                                                           <ins><span class="amout">Rp. <?php echo number_format($i->courier_harga); ?></span></ins>
                                                           
                                                        </p>
                                                    </div>
        
                                                </div>
                                            </div>
                                            <?php endforeach; ?>
                                        </div>
                                    </div>
                                </div>
                        </div>
                </div>

            </div>
        </section>
        <!-- END / DAFTAR HARGA -->


        <!-- DEALS PACKAGE -->
    <!--     <section class="section-deals mt90">-->
    <!--    <div class="container">-->
    <!--        <div class="content">-->
    <!--            <div class="row">-->
    <!--                <div class="col col-xs-12 col-lg-6 col-lg-offset-3">-->
    <!--                <h3 class="shortcode-heading"></h3>-->
    <!--                    <div class="ot-heading row-20 mb30 text-center">-->
    <!--                        <h2 class="shortcode-heading">Layanan Apparell</h2>-->
    <!--                </div>-->
    <!--            </div>-->
    <!--            <div class="row">-->
    <!--                <div class="col-xs-12 col-sm-4">-->
    <!--                    <div class="item item-deal">-->
    <!--                        <div class="img">-->
    <!--                            <img class="img-full" src="<?php echo base_url().'theme/images/apparell4.png'?>" alt="" width="">-->
    <!--                        </div>-->
    <!--                        <div class="info">-->
    <!--                            <a class="title bold f26 font-monserat upper" href="!#">T Shirt</a>-->
    <!--                            <p class="sub font-monserat f12 f-600 upper mt10 mb20">Rp. 100,000,00</p>-->
    <!--                            <a class="awe-btn awe-btn-12 btn-medium font-hind f12 bold" href="<?php echo site_url('contact');?>">Pesan</a>-->
    <!--                        </div>-->
    <!--                    </div>-->
    <!--                </div>-->
    <!--                <div class="col-xs-12 col-sm-4">-->
    <!--                    <div class="item item-deal">-->
    <!--                        <div class="img">-->
    <!--                            <img class="img-full" src="<?php echo base_url().'theme/images/apparell3.png'?>" alt="">-->
    <!--                        </div>-->
    <!--                        <div class="info">-->
    <!--                            <a class="title bold f26 font-monserat upper" href="!#">Pillow</a>-->
    <!--                            <p class="sub font-monserat f12 f-600 upper mt10 mb20">Rp. 45,000,00</p>-->
    <!--                            <a class="awe-btn awe-btn-12 btn-medium font-hind f12 bold" href="<?php echo site_url('contact');?>">Pesan</a>-->
    <!--                        </div>-->
    <!--                    </div>-->
    <!--                </div>-->
    <!--                <div class="col-xs-12 col-sm-4">-->
    <!--                    <div class="item item-deal">-->
    <!--                        <div class="img">-->
    <!--                            <img class="img-full" src="<?php echo base_url().'theme/images/apparell1.png'?>" alt="">-->
    <!--                        </div>-->
    <!--                        <div class="info">-->
    <!--                            <a class="title bold f26 font-monserat upper" href="!#">Mug</a>-->
    <!--                            <p class="sub font-monserat f12 f-600 upper mt10 mb20">Rp. 35,000,00</p>-->
    <!--                            <a class="awe-btn awe-btn-12 btn-medium font-hind f12 bold" href="<?php echo site_url('contact');?>">Pesan</a>-->
    <!--                        </div>-->
    <!--                    </div>-->
    <!--                </div>-->
                    
                    
    <!--            </div>-->
    <!--        </div>-->
    <!--    </div>-->
    <!--</section>-->
    <!--<br><br><br><br><br><br><br>-->
        <!-- END / DEALS PACKAGE -->

        <!-- BLOG -->
        <section class="section-blog bg-white">
            <div class="container">
                <div class="blog">
                    <div class="row">

                        <div class="col-md-12">
                            <div class="blog-content events-content">

                                <div class="content">
                                    <div class="row">
                                        <div class="col-xs-12 col-lg-6 col-lg-offset-3">
                                            <div class="ot-heading row-20 mb40 text-center">
                                                <h2 class="shortcode-heading">Berita Terkini</h2>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">

                                        <?php foreach($blog->result() as $blog):?>
                                        <div class="col-xs-12 col-sm-3">
                                            <div class="item">
                                                <div class="img">
                                                    <a href="<?php echo site_url('user/blog/detail/'.$blog->tulisan_slug);?>"><img
                                                            class="img-responsive img-full"
                                                            src="<?php echo base_url().'assets/images/'.$blog->tulisan_gambar;?>"
                                                            alt="<?php echo $blog->tulisan_judul;?>"></a>
                                                </div>
                                                <div class="info"><br>
                                                    <a class="title font-monserat f14 mb20 block bold upper"
                                                        href="<?php echo site_url('user/blog/detail/'.$blog->tulisan_slug);?>"><?php echo $blog->tulisan_judul;?></a>
                                                </div>
                                            </div>
                                        </div>
                                        <?php endforeach;?>

                                    </div>
                                    <div class="text-center">
                                        <a href="<?php echo site_url('user/blog');?>"
                                            class="awe-btn awe-btn-default font-hind f12 bold btn-medium mt15">Lihat
                                            Selanjutnya</a>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- <div class="col-md-4 col-md-pull-8">
                            <div class="sidebar">
                                <div class="widget widget_upcoming_events">
                                    <h4 class="widget-title">Upcoming Events</h4>
                                    <ul>
                                        <?php
                                            $query_event=$this->db->query("SELECT events.*,DAY(event_post) AS hari,LEFT(DATE_FORMAT(event_post,'%M'),3) AS bulan FROM events ORDER BY event_id DESC LIMIT 3");
                                            foreach ($query_event->result() as $i) :
                                        ?>
                                        <li>
                                            <span class="event-date">
                                                <strong><?php echo $i->hari;?></strong>
                                                <?php echo $i->bulan;?>
                                            </span>
                                            <div class="text">
                                                <a href="#"><?php echo $i->event_nama;?></a>
                                                <span class="date"><?php echo $i->event_jadwal;?></span>
                                            </div>
                                        </li>
                                        <?php endforeach;?>

                                    </ul>
                                </div>
                            </div>
                        </div> -->

                    </div>
                </div>
            </div>
        </section>
        <!-- END / BLOG -->
        <!-- FOOTER -->
        <?php $this->load->view('frontend/footer');?>
        <!-- END / FOOTER -->



    </div>
    <!-- END / PAGE WRAP -->
    <!-- Modal Intro -->
    <div class="modal" id="ModalIntro" tabindex="-1" role="dialog"
        aria-labelledby="modal-normal" aria-hidden="true" >
        <div class="modal-lg" role="document" >
            <div class="modal-content">
                <div class="block block-themed block-transparent mb-0" style="padding:20px;">
                    <div class="block-header bg-primary-dark">
                            <button type="button" class="btn pull-right" data-dismiss="modal" aria-label="Close">
                                <i class="fa fa-remove"></i>
                            </button>
                    </div></br>
                    <div class="block-content" >
                        <center>
                            <h4 style="font: 100 45px/1.3 'Oleo Script', Helvetica, sans-serif;color: #2b2b2b;text-shadow: 4px 4px 0px rgba(0,0,0,0.1);">Selamat Datang di Potrello.id</h4>
                            <div class="slider">
                            <div id="slider">
                                <a href="#" class="control_next"><i class="fa fa-chevron-circle-right"></i></a>
                                <a href="#" class="control_prev"><i class="fa fa-chevron-circle-left"></i></a>
                            <ul>
                                <li><img src="<?php echo base_url().'theme/images/tutorAwal.png' ?>" alt=""></li>
                                <li><img src="<?php echo base_url().'theme/images/tutor1.png' ?>" alt=""></li>
                                <li><img src="<?php echo base_url().'theme/images/tutor2.png' ?>" alt=""></li>
                                <li>
                                    <img src="<?php echo base_url().'theme/images/tutor3.png' ?>" alt="">
                                    
                                </li>
                            </ul>  
                            </div>
                        </center>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END Normal Modal -->
   




    <!-- LOAD JQUERY -->
    <script type="text/javascript" src="<?php echo base_url().'theme/js/lib/jquery-1.11.0.min.js'?>"></script>
    <script type="text/javascript" src="<?php echo base_url().'theme/js/lib/jquery-ui.min.js'?>"></script>
    <script type="text/javascript" src="<?php echo base_url().'theme/js/lib/bootstrap.min.js'?>"></script>
    <script type="text/javascript" src="<?php echo base_url().'theme/js/lib/bootstrap-select.js'?>"></script>
    <script type="text/javascript" src="<?php echo base_url().'theme/js/lib/isotope.pkgd.min.js'?>"></script>
    <script type="text/javascript" src="<?php echo base_url().'theme/js/lib/jquery.themepunch.revolution.min.js'?>">
    </script>
    <script type="text/javascript" src="<?php echo base_url().'theme/js/lib/jquery.themepunch.tools.min.js'?>"></script>
    <script type="text/javascript" src="<?php echo base_url().'theme/js/lib/owl.carousel.js'?>"></script>
    <script type="text/javascript" src="<?php echo base_url().'theme/js/lib/jquery.appear.min.js'?>"></script>
    <script type="text/javascript" src="<?php echo base_url().'theme/js/lib/jquery.countTo.js'?>"></script>
    <script type="text/javascript" src="<?php echo base_url().'theme/js/lib/jquery.countdown.min.js'?>"></script>
    <script type="text/javascript" src="<?php echo base_url().'theme/js/lib/jquery.parallax-1.1.3.js'?>"></script>
    <script type="text/javascript" src="<?php echo base_url().'theme/js/lib/jquery.magnific-popup.min.js'?>"></script>

    <script type="text/javascript" src="<?php echo base_url().'theme/js/lib/jquery.form.min.js'?>"></script>
    <script type="text/javascript" src="<?php echo base_url().'theme/js/lib/jquery.validate.min.js'?>"></script>
    <script type="text/javascript" src="<?php echo base_url().'theme/js/introSlider.js'?>"></script>


    <script type="text/javascript" src="<?php echo base_url().'theme/js/scripts.js'?>"></script>
    <script src="<?php echo base_url().'assets/js/core/jquery.min.js'?>"></script>
    <script src="<?php echo base_url().'assets/js/core/popper.min.js'?>"></script>
    <script src="<?php echo base_url().'assets/js/core/bootstrap.min.js'?>"></script>
    <script src="<?php echo base_url().'assets/js/core/jquery.slimscroll.min.js'?>"></script>
    <script src="<?php echo base_url().'assets/js/core/jquery.scrollLock.min.js'?>"></script>
    <script src="<?php echo base_url().'assets/js/core/jquery.appear.min.js'?>"></script>
    <script src="<?php echo base_url().'assets/js/core/jquery.countTo.min.js'?>"></script>
    <script src="<?php echo base_url().'assets/js/core/js.cookie.min.js'?>"></script>
    <script src="<?php echo base_url().'assets/js/codebase.js'?>"></script>
    <script src="<?php echo base_url().'assets/ckeditor/ckeditor.js'?>"></script>
    <script src="<?php echo base_url().'assets/js/plugins/datatables/jquery.dataTables.min.js'?>"></script>
    <script src="<?php echo base_url().'assets/js/plugins/datatables/dataTables.bootstrap4.min.js'?>"></script>
    <script src="<?php echo base_url().'assets/js/dropify.min.js'?>"></script>
</body>

</html>