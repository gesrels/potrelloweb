 <div class="header_content" id="header_content">

     <div class="container">
         <!-- HEADER LOGO -->
         <div class="header_logo">
             <a href="<?php echo site_url('');?>"><img draggable="false" onContextMenu="return false;"
                     src="<?php echo base_url().'theme/images/logo1.png'?>" alt=""></a>
         </div>
         <!-- END / HEADER LOGO -->

         <!-- HEADER MENU -->
         <nav class="header_menu">
             <ul class="menu">
                 <li><a href="<?php echo site_url('user/home');?>">Beranda</a></li>
                 <li><a href="<?php echo site_url('user/service');?>">Layanan</a></li>
                 <li><a href="<?php echo site_url('user/blog');?>">Berita</a></li>
                 <li><a href="<?php echo site_url('user/order');?>">Pemesanan</a></li>
                 <li><a href="<?php echo site_url('user/profile');?>">Pesanan Saya</a></li>
                 <li class="dropdown user user-menu">
                     <a style="font-family: 'Montserrat', sans-serif;" href="#" class="dropdown-toggle"
                         data-toggle="dropdown">
                         <img style="margin-top:0px" src="<?php echo base_url().'theme/images/user2.png' ?>"
                             width="20px" class="user-image" alt="">
                         <small><?php echo $this->session->userdata('nama'); ?></small>
                     </a>
                     <ul class="dropdown-menu" style="width:100%">
                         <!-- Menu Body -->
                         <br>
                         <center><img src="<?php echo base_url().'theme/images/user3.png';?>"
                                 class="cursor-circle-photo" width="80px"></center></br>
                         <center>
                             <center>
                                 <small
                                     style="font-family: 'Montserrat', sans-serif;"><?php echo $this->session->userdata('nama'); ?></small></br>
                                 <small
                                     style="font-family: 'Montserrat', sans-serif;"><?php echo $this->session->userdata('email'); ?></small><br></br>
                                     <?php $iduser = $this->session->userdata('iduser') ?>
                                <?php $quser = $this->db->query("SELECT * FROM user where user_id='$iduser'"); ?>
                                <?php foreach($quser->result() as $i): ?>                                
                                <small
                                     style="font-family: 'Montserrat', sans-serif;">ELLOPAY : Rp <?php echo number_format($i->user_saldo); ?>,00</small>
                                <?php endforeach; ?>
                             </center></br>
                             <!-- Menu Footer-->
                             <li class="user-footer">
                                 <script>
                                 function ConfirmLogOut() {
                                     confirm("Are you sure want to Logout?");
                                 }
                                 </script>
                                 <div class="row">
                                     <div class="col-xs-6">
                                         <a class="btn btn" style="background-color:#fff;color:#000"
                                             style="font-family: 'Montserrat', sans-serif;"
                                             href="<?php echo base_url().'user/profile' ?>"><i class="fa fa-user"></i>
                                             Profil</a>
                                     </div>
                                     <div class="col-xs-6">
                                         <a class="btn btn" style="background-color:#fff;color:#000"
                                             onclick="ConfirmLogOut()" style="font-family: 'Montserrat', sans-serif;"
                                             href="<?php echo base_url().'login/logout' ?>"><i
                                                 class="fa fa-power-off"></i> Keluar</a>
                                     </div>
                                 </div>
                             </li>
                     </ul>
                 </li>
                 <li class="dropdown user user-menu">
                     <a style="font-family: 'Montserrat', sans-serif;" href="#" class="dropdown-toggle"
                         data-toggle="dropdown">
                         <img style="margin-top:0px" src="<?php echo base_url().'theme/images/bell.png' ?>" width="20px"
                             class="user-image" alt="">
                     </a>
                     <ul class="dropdown-menu" style="width:300px;">
                         <?php 
                        $userId = $this->session->userdata('iduser');
                        $qnotifikasi = $this->db->query("SELECT * FROM notifikasi where notifikasi_user_id='$userId'  order by notifikasi_id DESC limit 5"); ?>
                         <?php if($qnotifikasi->num_rows() == 0): ?>
                         <ul>
                         <li> Tidak ada pemberitahuan</li>

                         </ul>
                         <?php else: ?>
                         
                         <div style="margin:10px;" class="pull-right">
                               <?php foreach($qnotifikasi->result() as $notif): ?>
                                <li style="margin-bottom:10px">
                                    <a style="color:#000;background-color:#fff" href="<?php echo base_url().'user/profile';?>">
                                        <small><?php echo $notif->notifikasi_isi; ?>
                                        <b>[<?php echo date("d M Y h:i A", strtotime($notif->notifikasi_tgl)) ?>]</b></small>
                                    </a>
                                </li>
                                <?php endforeach; ?>
                                <center><a style="color:#000;background-color:#fff"><small>View All</small></a></center>
                         </div>
                         
                         
                         <?php endif; ?>
                     </ul>
                 </li>
             </ul>
         </nav>
         <!-- END / HEADER MENU -->

         <!-- MENU BAR -->
         <span class="menu-bars">
             <span></span>
         </span>
         <!-- END / MENU BAR -->
         
         

     </div>
 </div>