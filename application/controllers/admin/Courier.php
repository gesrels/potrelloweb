<?php
class courier extends CI_Controller{
	function __construct(){
		parent::__construct();
		if($this->session->userdata('masuk') !=TRUE){
            $url=base_url('administrator');
            redirect($url);
        };
		$this->load->model('M_courier','m_courier');
		$this->load->library('upload');
	}


	function index(){
		$x['courier']=$this->m_courier->get_all_courier();
		$this->load->view('admin/v_courier',$x);
	}

	function simpan_courier(){
        $nama=strip_tags($this->input->post('xnama'));
        $harga=strip_tags($this->input->post('xharga'));
        $config['upload_path'] = './assets/images/'; //path folder
        $config['allowed_types'] = 'jpg|png|jpeg|bmp'; //type yang dapat diakses bisa anda sesuaikan
        $config['encrypt_name'] = TRUE; //nama yang terupload nantinya

        $this->upload->initialize($config);

        if ($this->upload->do_upload('filefoto'))
        {     
            $fileData = $this->upload->data();
            $gambar = $fileData['file_name'];
            $this->m_courier->simpan_courier($nama, $harga, $gambar);
            redirect('admin/courier');
        }else{
            redirect('admin/courier');
        }
        
        
	}

	function update_courier(){
        $kode=strip_tags($this->input->post('xkode'));
        $nama=strip_tags($this->input->post('xnama'));
        $harga=strip_tags($this->input->post('xharga'));

        $config['upload_path'] = './assets/images/'; //path folder
        $config['allowed_types'] = 'jpg|png|jpeg|bmp'; //type yang dapat diakses bisa anda sesuaikan
        $config['encrypt_name'] = TRUE; //nama yang terupload nantinya

        $this->upload->initialize($config);

        if ($this->upload->do_upload('filefoto'))
        {     
            $fileData = $this->upload->data();
            $gambar = $fileData['file_name'];
            
            $this->m_courier->update_courier($kode,$nama, $harga, $gambar);
            redirect('admin/courier');
        }else{
            $this->m_courier->update_courier_nogambar($kode,$nama, $harga);
            redirect('admin/courier');
        }
		
	}
	function hapus_courier(){
			$kode=strip_tags($this->input->post('kode'));
			$this->m_courier->hapus_courier($kode);
			echo $this->session->set_flashdata('msg','success-hapus');
			redirect('admin/courier');
	}

}