<?php
class Order extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        if ($this->session->userdata('masuk') != true) {
            $url = base_url('administrator');
            redirect($url);
        };
        $this->load->model('M_slider', 'm_slider');
        $this->load->model('M_tulisan', 'm_tulisan');
        $this->load->model('M_user', 'm_user');
        $this->load->model('M_size', 'm_size');
        $this->load->model('M_room', 'm_room');
        $this->load->model('M_address', 'm_address');
        $this->load->model('M_courier', 'm_courier');
        $this->load->model('M_order', 'm_order');
        $this->load->model('M_notif', 'm_notif');

        $this->load->library('upload');
        $this->load->library('email');

    }

    public function index()
    {
        $x['Allsize'] = $this->m_size->get_all_size();
        $x['Allroom'] = $this->m_room->get_all_room();
        $x['Allcourier'] = $this->m_courier->get_all_courier();
        $x['address'] = $this->m_address->get_address_byid();
        $x['order'] = $this->m_order->get_all_order();
        
        $this->load->view('admin/v_order', $x);
    }

    public function update_order()
    {
        $config['upload_path'] = './assets/images/'; //path folder
        $config['allowed_types'] = 'jpg|png|jpeg|bmp'; //type yang dapat diakses bisa anda sesuaikan
        $config['encrypt_name'] = true; //nama yang terupload nantinya
        $this->upload->initialize($config);
        $kode = strip_tags($this->input->post('xkode'));
        $service = strip_tags($this->input->post('xservice'));
        $size = strip_tags($this->input->post('xsize'));
        $text = strip_tags($this->input->post('xcalligraphy'));
        $deskripsi = strip_tags($this->input->post('xdeskripsi'));
        $courier = strip_tags($this->input->post('xcourier'));
        $alamat = strip_tags($this->input->post('xalamat'));

        if ($this->upload->do_upload('filefoto2')) {
            $fileData = $this->upload->data();
            $gambar = $fileData['file_name'];
            // //Compress Image
            // $config['image_library']='gd2';
            // $config['source_image']='./assets/images/order/'.$gbr;
            // $config['create_thumb']= FALSE;
            // $config['maintain_ratio']= FALSE;
            // $config['quality']= '100%';
            // $config['width']= 700;
            // $config['height']= 700;
            // $config['new_image']= './assets/images/order/'.$gbr;
            // $this->load->library('image_lib', $config);
            // $this->image_lib->resize();
            $this->m_order->update_order($kode, $service, $size, $gambar, $text, $deskripsi, $courier, $alamat);
            echo $this->session->set_flashdata('msg', 'edit-pesanan');
            redirect('admin/order');
        } else {
            $this->m_order->update_order_noimg($kode, $service, $size, $text, $deskripsi, $courier, $alamat);
            echo $this->session->set_flashdata('msg', 'edit-pesanan');
            redirect('admin/order');
        }
    }
    
    public function update_order_confirm()
    {
        $kode = strip_tags($this->input->post('xkode'));
        $service = strip_tags($this->input->post('xservice'));
        $size = strip_tags($this->input->post('xsize'));
        $text = strip_tags($this->input->post('xcalligraphy'));
        $deskripsi = strip_tags($this->input->post('xdeskripsi'));
        $courier = strip_tags($this->input->post('xcourier'));
        $alamat = strip_tags($this->input->post('xalamat'));

            $this->m_order->update_order_confirm($kode, $service, $size, $text, $deskripsi, $courier, $alamat);
            $this->m_order->delete_prev_invoice($kode);
            echo $this->session->set_flashdata('msg', 'edit-pesanan');
            redirect('admin/order');
    }

    public function confirm_order()
    {
        $kode = strip_tags($this->input->post('xkode'));
        $userId = strip_tags($this->input->post('xuserId'));
        $orderNumber = strip_tags($this->input->post('xorderNumber'));
        $namaService = strip_tags($this->input->post('xnamaService'));
        $serviceAmount = strip_tags($this->input->post('xserviceAmount'));
        $namaSize = strip_tags($this->input->post('xnamaSize'));
        $sizeAmount = strip_tags($this->input->post('xsizeAmount'));
        $namaOtherAmount = strip_tags($this->input->post('xnamaOtherAmount'));
        $otherAmount = strip_tags($this->input->post('xotherAmount'));
        $namaShipping = strip_tags($this->input->post('xnamaShipping'));
        $shippingAmount = strip_tags($this->input->post('xshippingAmount'));
        $saldoUser = strip_tags($this->input->post('xsaldoUser'));
        $userEmail = strip_tags($this->input->post('xemailUser'));
        $addressUser = strip_tags($this->input->post('xAddressUser'));

        $orderImg = strip_tags($this->input->post('xorderImg'));

        $ordertgl = strip_tags($this->input->post('xorderTgl'));
        $tglOrder = date("d M Y h:i A", strtotime($ordertgl));

        $subtotal = $serviceAmount + $sizeAmount + $otherAmount + $shippingAmount;
        $this->m_order->newstatus_order($kode);
        if ($saldoUser > $subtotal) {
            $total = $subtotal;
            $sisaSaldoUser = $saldoUser - $subtotal;
            $config = array(
                'protocol' => 'smtp',
                'smtp_host' => 'ssl://mail.potrello.id',
                'smtp_port' => 465,
                'smtp_user' => 'billing.noreply@potrello.id',
                'smtp_pass' => 'potrello08212322',
                'mailtype' => 'html',
                'charset' => 'utf-8');
            $this->email->initialize($config);
            $this->email->from('billing.noreply@potrello.id', 'Potrello');
            $this->email->to($userEmail);
            $this->email->subject("Pembayaran $orderNumber Diterima");

            $this->email->message("
				<html>
				<body
						style='background-color:#e2e1e0;font-family: Open Sans, sans-serif;font-size:100%;font-weight:400;line-height:1.4;color:#000;'>
						<table
								style='max-width:670px;margin:50px auto 10px;background-color:#fff;padding:50px;-webkit-border-radius:3px;-moz-border-radius:3px;border-radius:3px;-webkit-box-shadow:0 1px 3px rgba(0,0,0,.12),0 1px 2px rgba(0,0,0,.24);-moz-box-shadow:0 1px 3px rgba(0,0,0,.12),0 1px 2px rgba(0,0,0,.24);box-shadow:0 1px 3px rgba(0,0,0,.12),0 1px 2px rgba(0,0,0,.24); border-top: solid 10px green;'>
								<thead>
										<tr>
												<th style='text-align:left;'>POTRELLO</th>
												<th style='text-align:right;font-weight:400;'></th>
										</tr>
								</thead>
								<tbody>
										<tr>
												<td style='height:35px;'></td>
										</tr>
										<tr>
												<td colspan='2' style='border: solid 1px #ddd; padding:10px 20px;'>
														<p style='font-size:14px;margin:0 0 6px 0;'><span
																		style='font-weight:bold;display:inline-block;min-width:150px'>Status Pesanan
																</span><b style='color:green;font-weight:normal;margin:0'>PENGERJAAN</b></p>
														<p style='font-size:14px;margin:0 0 6px 0;'><span
																		style='font-weight:bold;display:inline-block;min-width:146px'>No Pesanan</span>
																		$orderNumber</p>
												</td>
										</tr>
										<tr>
												<td style='height:35px;'></td>
										</tr>
										<tr>
												<td colspan='2' style='padding:15px;'>
														<p style='font-size:14px;margin:0;padding:10px;border:solid 1px #ddd;font-weight:bold;'>
																<span style='display:block;font-size:13px;font-weight:normal;'>PENARIKAN ELLOPAY</span> Rp. $subtotal<b
																		style='font-size:12px;font-weight:300;'></b></p>
													<h4 class='pull-right'>LUNAS</h4>
													<a href='http://potrello.id' style='background-color: #4CAF50;
															border: none;
															color: white;
															padding: 15px 32px;
															text-align: center;
															text-decoration: none;
															display: inline-block;
															width:200px;
															font-size: 16px;'>RINCIAN PESANAN</a>
												</td>
										</tr>

								</tbody>

								<tfooter>
										<tr>
												<td colspan='2' style='font-size:14px;padding:50px 15px 0 15px;'>
														<strong style='display:block;margin:0 0 10px 0;'>Best Regards,</strong> Potrello<br> Jakarta Barat, DKI
														Jakarta, IND<br><br>
														<b>Phone:</b> 082123228250<br>
														<b>Email:</b> cs@potrello.id
												</td>
										</tr>
								</tfooter>
						</table>
				</body>
				</html>
				");
            if ($this->email->send()) {
                $this->m_user->kurang_saldo($userId, $sisaSaldoUser);
                $this->m_order->simpan_invoice($kode, $userId, $namaService, $serviceAmount, $namaSize, $sizeAmount, $namaOtherAmount, $otherAmount, $namaShipping, $shippingAmount, $saldoUser, $total);
                $this->m_order->paid_invoice_order($kode, $total);
                $this->m_notif->order_confirm_notif($userId, $orderNumber);
                $this->m_order->progress_order($kode);
                $this->m_notif->order_lunas_notif($userId, $orderNumber);
                echo $this->session->set_flashdata('msg', 'konfirmasi');
                redirect('admin/order');
            } else {
                echo $this->session->set_flashdata('msg', 'gagal-email');
                redirect('admin/order');
            }

        } else {
            $total = $subtotal;
            $totalAmount = number_format($total - $saldoUser);
            $config = array(
                'protocol' => 'smtp',
                'smtp_host' => 'ssl://mail.potrello.id',
                'smtp_port' => 465,
                'smtp_user' => 'billing.noreply@potrello.id',
                'smtp_pass' => 'potrello08212322',
                'mailtype' => 'html',
                'charset' => 'utf-8');
            $this->email->initialize($config);
            $this->email->from('billing.noreply@potrello.id', 'Potrello');
            $this->email->to($userEmail);
            $this->email->subject("Pembayaran $orderNumber");

            $this->email->message("
				<html>
				<body
						style='background-color:#e2e1e0;font-family: Open Sans, sans-serif;font-size:100%;font-weight:400;line-height:1.4;color:#000;'>
						<table
								style='max-width:670px;margin:50px auto 10px;background-color:#fff;padding:50px;-webkit-border-radius:3px;-moz-border-radius:3px;border-radius:3px;-webkit-box-shadow:0 1px 3px rgba(0,0,0,.12),0 1px 2px rgba(0,0,0,.24);-moz-box-shadow:0 1px 3px rgba(0,0,0,.12),0 1px 2px rgba(0,0,0,.24);box-shadow:0 1px 3px rgba(0,0,0,.12),0 1px 2px rgba(0,0,0,.24); border-top: solid 10px green;'>
								<thead>
										<tr>
												<th style='text-align:left;'>POTRELLO</th>
												<th style='text-align:right;font-weight:400;'>$tglOrder</th>
										</tr>
								</thead>
								<tbody>
										<tr>
												<td style='height:35px;'></td>
										</tr>
										<tr>
												<td colspan='2' style='border: solid 1px #ddd; padding:10px 20px;'>
														<p style='font-size:14px;margin:0 0 6px 0;'><span
																		style='font-weight:bold;display:inline-block;min-width:150px'>Status Pesanan
																</span><b style='color:green;font-weight:normal;margin:0'>DIKONFIRMASI</b></p>
														<p style='font-size:14px;margin:0 0 6px 0;'><span
																		style='font-weight:bold;display:inline-block;min-width:146px'>No Pesanan</span>
																		$orderNumber</p>
												</td>
										</tr>
										<tr>
												<td style='height:35px;'></td>
										</tr>

										<tr>
												<td style='width:70%;padding:20px;vertical-align:top'>
														<p style='margin:0 0 10px 0;padding:0;font-size:14px;'><span
																		style='display:block;font-weight:bold;font-size:13px'>Layanan</span> $namaService</p>
														<p style='margin:0 0 10px 0;padding:0;font-size:14px;'><span
																		style='display:block;font-weight:bold;font-size:13px;'>Ukuran</span> $namaSize</p>
													
																		<p style='margin:0 0 10px 0;padding:0;font-size:14px;'><span
																		style='display:block;font-weight:bold;font-size:13px;'>Kurir</span>$namaShipping</p>
														
												</td>
												<td style='width:30%;padding:20px;vertical-align:top'>

														<p style='margin:0 0 10px 0;padding:0;font-size:14px;'> <img
																		src='https://potrello.id/assets/images/$orderImg' alt=''
																		width='180px'></p>
												</td>
												
										</tr>
									
										<tr>
												<td colspan='2' style='padding:15px;'>
														<p style='font-size:14px;margin:0;padding:10px;border:solid 1px #ddd;font-weight:bold;'>
																<span style='display:block;font-size:13px;font-weight:normal;'>Layanan</span> Rp. $serviceAmount<b
																		style='font-size:12px;font-weight:300;'> - $namaService</b></p>

														<p style='font-size:14px;margin:0;padding:10px;border:solid 1px #ddd;font-weight:bold;'>
																<span style='display:block;font-size:13px;font-weight:normal;'>Ukuran</span> Rp. $sizeAmount<b
																		style='font-size:12px;font-weight:300;'> - $namaSize</b></p>

														<p style='font-size:14px;margin:0;padding:10px;border:solid 1px #ddd;font-weight:bold;'>
																<span style='display:block;font-size:13px;font-weight:normal;'>Lainnya</span> Rp. $otherAmount<b
																		style='font-size:12px;font-weight:300;'> - $namaOtherAmount</b></p>
														<p style='font-size:14px;margin:0;padding:10px;border:solid 1px #ddd;font-weight:bold;'>
																		<span style='display:block;font-size:13px;font-weight:normal;'>Pengiriman</span> Rp. $shippingAmount<b
																				style='font-size:12px;font-weight:300;'> - $namaShipping</b></p>
														<p style='font-size:14px;margin:0;padding:10px;border:solid 1px #ddd;font-weight:bold;'>
																				<span style='display:block;font-size:13px;font-weight:normal;'>PENARIKAN ELLOPAY</span> Rp. $saldoUser<b
																						style='font-size:12px;font-weight:300;'></b></p>
														<p style='font-size:14px;margin:0;padding:10px;border:solid 1px #ddd;font-weight:bold;'>
																<span style='display:block;font-size:13px;font-weight:normal;'>Jumlah yang harus dibayar</span> Rp. $totalAmount<b
																		style='font-size:12px;font-weight:300;'></b></p>
									    <p style='font-size:14px;margin:0;padding:10px;border:solid 1px #ddd;font-weight:bold;'>
																<span style='display:block;font-size:13px;font-weight:normal;'>Dikirim ke alamat</span>$addressUser<b
																		style='font-size:12px;font-weight:300;'></b></p>
													<h4 class='pull-right'>BELUM LUNAS</h4>
													<a href='http://potrello.id' style='background-color: #4CAF50;
															border: none;
															color: white;
															padding: 15px 32px;
															margin-bottom : 20px;
															text-align: center;
															text-decoration: none;
															display: inline-block;
															width:200px;
															font-size: 16px;'>BAYAR SEKARANG</a>
															
													<center><img width='200px' src='https://potrello.id/theme/images/BCA.png'>
        										        <h2>No Rek : <b>8670 1891 41</b></h2>
        										        <p>Atas Nama : <b>ELLORA DESYANA PERMATA LATIF</b></p></center>
												</td>
												
        										        
        										
										</tr>
										
										
										

								</tbody>
								

								<tfooter>
										<tr>
												<td colspan='2' style='align-text:center;font-size:14px;padding:50px 15px 0 15px;'>
														<strong style='display:block;margin:0 0 10px 0;'>Best Regards,</strong> Potrello<br>Tomang, Jakarta Barat, DKI
														Jakarta, IND<br><br>
														<b>Phone:</b> 082123228250<br>
														<b>Email:</b> cs@potrello.id
												</td>
										</tr>
								</tfooter>
						</table>
				</body>
				</html>
				");
            if ($this->email->send()) {
                $this->m_user->clear_saldo($userId);
                $this->m_order->confirm_order($kode);
                $this->m_notif->order_confirm_notif($userId, $orderNumber);
                $this->m_order->simpan_invoice($kode, $userId, $namaService, $serviceAmount, $namaSize, $sizeAmount, $namaOtherAmount, $otherAmount, $namaShipping, $shippingAmount, $saldoUser, $total);
                $this->m_order->bayar_invoice_sisasaldo($kode, $saldoUser);
                echo $this->session->set_flashdata('msg', 'konfirmasi');
                redirect('admin/order');

            } else {
                echo $this->session->set_flashdata('msg', 'gagal-email');
                redirect('admin/order');
            }
        }
        $this->m_order->set_exp($kode);
    }

    public function paid_order()
    {
        $kode = strip_tags($this->input->post('xkode'));
        $orderNumber = strip_tags($this->input->post('xorderNumber'));
        $userId = strip_tags($this->input->post('xuserId'));
        $userEmail = strip_tags($this->input->post('xuserEmail'));
        $invoiceId = strip_tags($this->input->post('xInvoiceId'));
        $amount = strip_tags($this->input->post('xAmountPayment'));
        $total = strip_tags($this->input->post('xTotal'));

        if ($amount < $total) {
            echo $this->session->set_flashdata('msg', "gagal-bayar");
            redirect('admin/order');
        } elseif ($amount > $total) {
            $bayarLebih = $amount - $total;
            $config = array(
                'protocol' => 'smtp',
                'smtp_host' => 'ssl://mail.potrello.id',
                'smtp_port' => 465,
                'smtp_user' => 'billing.noreply@potrello.id',
                'smtp_pass' => 'potrello08212322',
                'mailtype' => 'html',
                'charset' => 'utf-8');
            $this->email->initialize($config);
            $this->email->from('billing.noreply@potrello.id', 'Potrello');
            $this->email->to($userEmail);
            $this->email->subject("Pembayaran $orderNumber Diterima");

            $this->email->message("
			<html>
			<body
					style='background-color:#e2e1e0;font-family: Open Sans, sans-serif;font-size:100%;font-weight:400;line-height:1.4;color:#000;'>
					<table
							style='max-width:670px;margin:50px auto 10px;background-color:#fff;padding:50px;-webkit-border-radius:3px;-moz-border-radius:3px;border-radius:3px;-webkit-box-shadow:0 1px 3px rgba(0,0,0,.12),0 1px 2px rgba(0,0,0,.24);-moz-box-shadow:0 1px 3px rgba(0,0,0,.12),0 1px 2px rgba(0,0,0,.24);box-shadow:0 1px 3px rgba(0,0,0,.12),0 1px 2px rgba(0,0,0,.24); border-top: solid 10px green;'>
							<thead>
									<tr>
											<th style='text-align:left;'>POTRELLO</th>
											<th style='text-align:right;font-weight:400;'></th>
									</tr>
							</thead>
							<tbody>
									<tr>
											<td style='height:35px;'></td>
									</tr>
									<tr>
											<td colspan='2' style='border: solid 1px #ddd; padding:10px 20px;'>
													<p style='font-size:14px;margin:0 0 6px 0;'><span
																	style='font-weight:bold;display:inline-block;min-width:150px'>Status Pesanan
															</span><b style='color:green;font-weight:normal;margin:0'>PENGERJAAN</b></p>
													<p style='font-size:14px;margin:0 0 6px 0;'><span
																	style='font-weight:bold;display:inline-block;min-width:146px'>No Pesanan</span>
																	$orderNumber</p>
											</td>
									</tr>
									<tr>
											<td style='height:35px;'></td>
									</tr>
									<tr>
											<td colspan='2' style='padding:15px;'>
											<p style='font-size:14px;margin:0;padding:10px;border:solid 1px #ddd;font-weight:bold;'>
											<span style='display:block;font-size:13px;font-weight:normal;'>Kelebihan Bayar</span> Rp. $bayarLebih<b
													style='font-size:12px;font-weight:300;'></b> - Dicairkan ke saldo ELLOPAY</p>
													<p style='font-size:14px;margin:0;padding:10px;border:solid 1px #ddd;font-weight:bold;'>
															<span style='display:block;font-size:13px;font-weight:normal;'>Total Pembayaran</span> Rp. $amount<b
																	style='font-size:12px;font-weight:300;'></b></p>
												<h4 class='pull-right'>LUNAS</h4>
												<a href='http://potrello.id' style='background-color: #4CAF50;
														border: none;
														color: white;
														padding: 15px 32px;
														text-align: center;
														text-decoration: none;
														display: inline-block;
														width:200px;
														font-size: 16px;'>RINCIAN PESANAN</a>
											</td>
									</tr>

							</tbody>

							<tfooter>
									<tr>
											<td colspan='2' style='font-size:14px;padding:50px 15px 0 15px;'>
													<strong style='display:block;margin:0 0 10px 0;'>Best Regards,</strong> Potrello<br> Jakarta Barat, DKI
													Jakarta, IND<br><br>
													<b>Phone:</b> 082123228250<br>
													<b>Email:</b> cs@potrello.id
											</td>
									</tr>
							</tfooter>
					</table>
			</body>
			</html>
			");
            if ($this->email->send()) {
                $this->m_order->progress_order($kode);
                $this->m_order->paid_invoice($invoiceId, $amount);
                $this->m_notif->order_lunas_notif($userId, $orderNumber);
                if ($bayarLebih > 0) {
                    $this->m_notif->saldo_notif($userId, $bayarLebih);
                }
                $this->m_user->simpan_saldo($userId, $bayarLebih);
                echo $this->session->set_flashdata('msg', 'bayar');
                redirect('admin/order');
            } else {
                echo $this->session->set_flashdata('msg', 'gagal-email');
                redirect('admin/order');
            }

        } else {

            $config = array(
                'protocol' => 'smtp',
                'smtp_host' => 'ssl://mail.potrello.id',
                'smtp_port' => 465,
                'smtp_user' => 'billing.noreply@potrello.id',
                'smtp_pass' => 'potrello08212322',
                'mailtype' => 'html',
                'charset' => 'utf-8');
            $this->email->initialize($config);
            $this->email->from('billing.noreply@potrello.id', 'Potrello');
            $this->email->to($userEmail);
            $this->email->subject("Pembayaran $orderNumber Diterima");

            $this->email->message("
			<html>
			<body
					style='background-color:#e2e1e0;font-family: Open Sans, sans-serif;font-size:100%;font-weight:400;line-height:1.4;color:#000;'>
					<table
							style='max-width:670px;margin:50px auto 10px;background-color:#fff;padding:50px;-webkit-border-radius:3px;-moz-border-radius:3px;border-radius:3px;-webkit-box-shadow:0 1px 3px rgba(0,0,0,.12),0 1px 2px rgba(0,0,0,.24);-moz-box-shadow:0 1px 3px rgba(0,0,0,.12),0 1px 2px rgba(0,0,0,.24);box-shadow:0 1px 3px rgba(0,0,0,.12),0 1px 2px rgba(0,0,0,.24); border-top: solid 10px green;'>
							<thead>
									<tr>
											<th style='text-align:left;'>POTRELLO</th>
											<th style='text-align:right;font-weight:400;'></th>
									</tr>
							</thead>
							<tbody>
									<tr>
											<td style='height:35px;'></td>
									</tr>
									<tr>
											<td colspan='2' style='border: solid 1px #ddd; padding:10px 20px;'>
													<p style='font-size:14px;margin:0 0 6px 0;'><span
																	style='font-weight:bold;display:inline-block;min-width:150px'>Status Pesanan
															</span><b style='color:green;font-weight:normal;margin:0'>PENGERJAAN</b></p>
													<p style='font-size:14px;margin:0 0 6px 0;'><span
																	style='font-weight:bold;display:inline-block;min-width:146px'>No Pesanan</span>
																	$orderNumber</p>
											</td>
									</tr>
									<tr>
											<td style='height:35px;'></td>
									</tr>
									<tr>
											<td colspan='2' style='padding:15px;'>
													<p style='font-size:14px;margin:0;padding:10px;border:solid 1px #ddd;font-weight:bold;'>
															<span style='display:block;font-size:13px;font-weight:normal;'>Total Pembayaran</span> Rp. $amount<b
																	style='font-size:12px;font-weight:300;'></b></p>
												<h4 class='pull-right'>LUNAS</h4>
												<a href='http://potrello.id' style='background-color: #4CAF50;
														border: none;
														color: white;
														padding: 15px 32px;
														text-align: center;
														text-decoration: none;
														display: inline-block;
														width:200px;
														font-size: 16px;'>RINCIAN PESANAN</a>
											</td>
									</tr>

							</tbody>

							<tfooter>
									<tr>
											<td colspan='2' style='font-size:14px;padding:50px 15px 0 15px;'>
													<strong style='display:block;margin:0 0 10px 0;'>Best Regards,</strong> Potrello<br> Jakarta Barat, DKI
													Jakarta, IND<br><br>
													<b>Phone:</b> 082123228250<br>
													<b>Email:</b> cs@potrello.id
											</td>
									</tr>
							</tfooter>
					</table>
			</body>
			</html>
			");
            if ($this->email->send()) {
                $this->m_order->progress_order($kode);
                $this->m_order->paid_invoice($invoiceId, $amount);
                $this->m_notif->order_lunas_notif($userId, $orderNumber);
                echo $this->session->set_flashdata('msg', 'bayar');
                redirect('admin/order');
            } else {
                echo $this->session->set_flashdata('msg', 'gagal-email');
                redirect('admin/order');
            }
        }
    }

    public function cancel_order()
    {
        $kode = strip_tags($this->input->post('xkode'));
        $this->m_order->cancel_order_admin($kode);
        echo $this->session->set_flashdata('msg', 'cancel');
        redirect('admin/order');
    }

    public function kirim_order()
    {
        $kode = strip_tags($this->input->post('xkode'));
        $userId = strip_tags($this->input->post('xuserId'));
        $orderNumber = strip_tags($this->input->post('xorderNumber'));
        $userEmail = strip_tags($this->input->post('xuserEmail'));

        $resi = strip_tags($this->input->post('xresi'));
        $driver = strip_tags($this->input->post('xdriver'));
        $kontak = strip_tags($this->input->post('xkontakdriver'));

        $config = array(
            'protocol' => 'smtp',
            'smtp_host' => 'ssl://mail.potrello.id',
            'smtp_port' => 465,
            'smtp_user' => 'shipping@potrello.id',
            'smtp_pass' => 'potrello08212322',
            'mailtype' => 'html',
            'charset' => 'utf-8');
        $this->email->initialize($config);
        $this->email->from('shipping@potrello.id', 'Potrello');
        $this->email->to($userEmail);
        $this->email->subject("Pengiriman $orderNumber");

        $this->email->message("
	<html>
	<body
			style='background-color:#e2e1e0;font-family: Open Sans, sans-serif;font-size:100%;font-weight:400;line-height:1.4;color:#000;'>
			<table
					style='max-width:670px;margin:50px auto 10px;background-color:#fff;padding:50px;-webkit-border-radius:3px;-moz-border-radius:3px;border-radius:3px;-webkit-box-shadow:0 1px 3px rgba(0,0,0,.12),0 1px 2px rgba(0,0,0,.24);-moz-box-shadow:0 1px 3px rgba(0,0,0,.12),0 1px 2px rgba(0,0,0,.24);box-shadow:0 1px 3px rgba(0,0,0,.12),0 1px 2px rgba(0,0,0,.24); border-top: solid 10px green;'>
					<thead>
							<tr>
									<th style='text-align:left;'>POTRELLO</th>
									<th style='text-align:right;font-weight:400;'></th>
							</tr>
					</thead>
					<tbody>
							<tr>
									<td style='height:35px;'></td>
							</tr>
							<tr>
									<td colspan='2' style='border: solid 1px #ddd; padding:10px 20px;'>
											<p style='font-size:14px;margin:0 0 6px 0;'><span
															style='font-weight:bold;display:inline-block;min-width:150px'>Status Pesanan
													</span><b style='color:green;font-weight:normal;margin:0'>PENGIRIMAN</b></p>
											<p style='font-size:14px;margin:0 0 6px 0;'><span
															style='font-weight:bold;display:inline-block;min-width:146px'>No Pesanan</span>
															$orderNumber</p>
									</td>
							</tr>
							<tr>
									<td style='height:35px;'></td>
							</tr>
							<tr>
									<td colspan='2' style='padding:15px;'>
									    <center><img width='200px' src='https://potrello.id/theme/images/kirim.gif'></center>
											<p style='font-size:14px;margin:0;padding:10px;border:solid 1px #ddd;font-weight:bold;'>
													<span style='display:block;font-size:13px;font-weight:normal;'>No. Resi / Tracking</span> $resi<b
															style='font-size:12px;font-weight:300;'></b></p>
															<p style='font-size:14px;margin:0;padding:10px;border:solid 1px #ddd;font-weight:bold;'>
													<span style='display:block;font-size:13px;font-weight:normal;'>Info Kurir</span> $driver, $kontak<b
															style='font-size:12px;font-weight:300;'></b></p>
										<a href='http://potrello.id' style='background-color: #4CAF50;
												border: none;
												color: white;
												padding: 15px 32px;
												text-align: center;
												text-decoration: none;
												display: inline-block;
												width:200px;
												font-size: 16px;'>LACAK PENGIRIMAN</a>
									</td>
							</tr>

					</tbody>

					<tfooter>
							<tr>
									<td colspan='2' style='font-size:14px;padding:50px 15px 0 15px;'>
											<strong style='display:block;margin:0 0 10px 0;'>Best Regards,</strong> Potrello<br> Jakarta Barat, DKI
											Jakarta, IND<br><br>
											<b>Phone:</b> 082123228250<br>
											<b>Email:</b> cs@potrello.id
									</td>
							</tr>
					</tfooter>
			</table>
	</body>
	</html>
	");
        if ($this->email->send()) {
            $this->m_notif->order_kirim_notif($userId, $orderNumber);
            $this->m_order->kirim_order($kode, $resi, $driver, $kontak);
            echo $this->session->set_flashdata('msg', 'kirim');

            redirect('admin/order');
        } else {
            echo $this->session->set_flashdata('msg', 'gagal-email');
            redirect('admin/order');
        }
    }

    public function Editkirim_order()
    {
        $kode = strip_tags($this->input->post('xkode'));
        $userId = strip_tags($this->input->post('xuserId'));
        $orderNumber = strip_tags($this->input->post('xorderNumber'));
        $userEmail = strip_tags($this->input->post('xuserEmail'));

        $resi = strip_tags($this->input->post('xresi'));
        $driver = strip_tags($this->input->post('xdriver'));

        $config = array(
            'protocol' => 'smtp',
            'smtp_host' => 'ssl://mail.potrello.id',
            'smtp_port' => 465,
            'smtp_user' => 'shipping@potrello.id',
            'smtp_pass' => 'potrello08212322',
            'mailtype' => 'html',
            'charset' => 'utf-8');
        $this->email->initialize($config);
        $this->email->from('shipping@potrello.id', 'Potrello');
        $this->email->to($userEmail);
        $this->email->subject("Perubahan Kurir Pengiriman $orderNumber");

        $this->email->message("
	<html>
	<body
			style='background-color:#e2e1e0;font-family: Open Sans, sans-serif;font-size:100%;font-weight:400;line-height:1.4;color:#000;'>
			<table
					style='max-width:670px;margin:50px auto 10px;background-color:#fff;padding:50px;-webkit-border-radius:3px;-moz-border-radius:3px;border-radius:3px;-webkit-box-shadow:0 1px 3px rgba(0,0,0,.12),0 1px 2px rgba(0,0,0,.24);-moz-box-shadow:0 1px 3px rgba(0,0,0,.12),0 1px 2px rgba(0,0,0,.24);box-shadow:0 1px 3px rgba(0,0,0,.12),0 1px 2px rgba(0,0,0,.24); border-top: solid 10px green;'>
					<thead>
							<tr>
									<th style='text-align:left;'>POTRELLO</th>
									<th style='text-align:right;font-weight:400;'></th>
							</tr>
					</thead>
					<tbody>
							<tr>
									<td style='height:35px;'></td>
							</tr>
							<tr>
									<td colspan='2' style='border: solid 1px #ddd; padding:10px 20px;'>
											<p style='font-size:14px;margin:0 0 6px 0;'><span
															style='font-weight:bold;display:inline-block;min-width:150px'>Status Pesanan
													</span><b style='color:green;font-weight:normal;margin:0'>PENGIRIMAN</b></p>
											<p style='font-size:14px;margin:0 0 6px 0;'><span
															style='font-weight:bold;display:inline-block;min-width:146px'>No Pesanan</span>
															$orderNumber</p>
									</td>
							</tr>
							<tr>
									<td style='height:35px;'></td>
							</tr>
							<tr>
									<td colspan='2' style='padding:15px;'>
									<center><img width='200px' src='https://potrello.id/theme/images/kirim.gif'></center>
											<p style='font-size:14px;margin:0;padding:10px;border:solid 1px #ddd;font-weight:bold;'>
													<span style='display:block;font-size:13px;font-weight:normal;'>No. Resi / Tracking</span> $resi<b
															style='font-size:12px;font-weight:300;'></b></p>
															<p style='font-size:14px;margin:0;padding:10px;border:solid 1px #ddd;font-weight:bold;'>
													<span style='display:block;font-size:13px;font-weight:normal;'>Info Kurir</span> $driver<b
															style='font-size:12px;font-weight:300;'></b></p>
										<a href='http://potrello.id' style='background-color: #4CAF50;
												border: none;
												color: white;
												padding: 15px 32px;
												text-align: center;
												text-decoration: none;
												display: inline-block;
												width:200px;
												font-size: 16px;'>LACAK PENGIRIMAN</a>
									</td>
							</tr>

					</tbody>

					<tfooter>
							<tr>
									<td colspan='2' style='font-size:14px;padding:50px 15px 0 15px;'>
											<strong style='display:block;margin:0 0 10px 0;'>Best Regards,</strong> Potrello<br> Jakarta Barat, DKI
											Jakarta, IND<br><br>
											<b>Phone:</b> 082123228250<br>
											<b>Email:</b> cs@potrello.id
									</td>
							</tr>
					</tfooter>
			</table>
	</body>
	</html>
	");
        if ($this->email->send()) {
            $this->m_notif->order_Editkirim_notif($userId, $orderNumber);
            $this->m_order->Editkirim_order($kode, $resi, $driver);
            echo $this->session->set_flashdata('msg', 'edit-kirim');
            redirect('admin/order');
        } else {
            echo $this->session->set_flashdata('msg', 'gagal-email');
            redirect('admin/order');
        }
    }

    public function selesai_order()
    {
        $kode = strip_tags($this->input->post('xkode'));
        $userId = strip_tags($this->input->post('xuserId'));
        $orderNumber = strip_tags($this->input->post('xorderNumber'));
        $userEmail = strip_tags($this->input->post('xuserEmail'));

        $config = array(
            'protocol' => 'smtp',
            'smtp_host' => 'ssl://mail.potrello.id',
            'smtp_port' => 465,
            'smtp_user' => 'order@potrello.id',
            'smtp_pass' => 'potrello08212322',
            'mailtype' => 'html',
            'charset' => 'utf-8');
        $this->email->initialize($config);
        $this->email->from('order@potrello.id', 'Potrello');
        $this->email->to($userEmail);
        $this->email->subject("Pesanan $orderNumber Selesai");

        $this->email->message("
	<html>
	<body
			style='background-color:#e2e1e0;font-family: Open Sans, sans-serif;font-size:100%;font-weight:400;line-height:1.4;color:#000;'>
			<table
					style='max-width:670px;margin:50px auto 10px;background-color:#fff;padding:50px;-webkit-border-radius:3px;-moz-border-radius:3px;border-radius:3px;-webkit-box-shadow:0 1px 3px rgba(0,0,0,.12),0 1px 2px rgba(0,0,0,.24);-moz-box-shadow:0 1px 3px rgba(0,0,0,.12),0 1px 2px rgba(0,0,0,.24);box-shadow:0 1px 3px rgba(0,0,0,.12),0 1px 2px rgba(0,0,0,.24); border-top: solid 10px green;'>
					<thead>
							<tr>
									<th style='text-align:left;'>POTRELLO</th>
									<th style='text-align:right;font-weight:400;'></th>
							</tr>
					</thead>
					<tbody>
							<tr>
									<td style='height:35px;'></td>
							</tr>
							<tr>
									<td colspan='2' style='border: solid 1px #ddd; padding:10px 20px;'>
											<p style='font-size:14px;margin:0 0 6px 0;'><span
															style='font-weight:bold;display:inline-block;min-width:150px'>Status Pesanan
													</span><b style='color:green;font-weight:normal;margin:0'>SUKSES</b></p>
											<p style='font-size:14px;margin:0 0 6px 0;'><span
															style='font-weight:bold;display:inline-block;min-width:146px'>No Pesanan</span>
															$orderNumber</p>
									</td>
							</tr>
							<tr>
									<td style='height:35px;'></td>
							</tr>
							<tr>
									<td colspan='2' style='padding:15px;'>
											<p style='font-size:14px;margin:0;padding:10px;border:solid 1px #ddd;font-weight:bold;'>
													<span style='display:block;font-size:13px;font-weight:normal;'>

													Terima kasih telah melakukan transaksi dengan Potrello.id, kepuasan anda adalah prioritas kami,
													Kami sangat menantikan feedback / kritik dan saran anda.

													</span></p>
													<a href='http://potrello.id' style='background-color: #4CAF50;
															border: none;
															color: white;
															padding: 15px 32px;
															text-align: center;
															text-decoration: none;
															display: inline-block;
															width:200px;
															font-size: 16px;'>KRITIK & SARAN
													</a>
									</td>
							</tr>
					</tbody>
					<tfooter>
							<tr>
									<td colspan='2' style='font-size:14px;padding:50px 15px 0 15px;'>
											<strong style='display:block;margin:0 0 10px 0;'>Best Regards,</strong> Potrello<br> Jakarta Barat, DKI
											Jakarta, IND<br><br>
											<b>Phone:</b> 082123228250<br>
											<b>Email:</b> cs@potrello.id
									</td>
							</tr>
					</tfooter>
			</table>
	</body>
	</html>
	");
        if ($this->email->send()) {
            $this->m_notif->order_selesai_notif($userId, $orderNumber);
            $this->m_order->selesai_order($kode);
            $this->m_user->update_rating_token($userId);
            echo $this->session->set_flashdata('msg', 'selesai');
            redirect('admin/order');
        } else {
            echo $this->session->set_flashdata('msg', 'gagal-email');
            redirect('admin/order');
        }
    }

}