<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Profile extends CI_Controller {
	function __construct(){
        parent::__construct();
        if($this->session->userdata('masuk') !=TRUE){
            $url=base_url('login');
            redirect($url);
        };
		$this->load->model('M_pengunjung','m_pengunjung');
		$this->load->model('M_slider','m_slider');
		$this->load->model('M_tulisan','m_tulisan');
        $this->load->model('M_user','m_user');
        $this->load->model('M_size','m_size');
        $this->load->model('M_room','m_room');
        $this->load->model('M_address','m_address');
        $this->load->model('M_courier','m_courier');
        $this->load->model('M_order','m_order');
        $this->m_pengunjung->count_visitor();
	}

	public function index(){
        $iduser = $this->session->userdata('iduser');
        $x['slider']=$this->m_slider->get_all_slider();
        $x['user']=$this->m_user->get_user_byid();
        $x['Allsize']=$this->m_size->get_all_size();
        $x['Allroom']=$this->m_room->get_all_room();
        $x['Allcourier']=$this->m_courier->get_all_courier();
        $x['address']=$this->m_address->get_address_byid();
        $x['order']=$this->m_order->get_order_byid_profile();
        $x['history']=$this->m_order->get_history_order_byid_profile();
        $x['invoice']=$this->m_order->get_invoice_by_userid();

        $this->session->set_userdata('isIntro', '');

        $userRatingToken = $this->session->userdata('rating');
        $passOTP = $this->session->userdata('passOTP');
        if ($passOTP === '1'){
            $x['passOTP']= '<script>setTimeout(function(){ $("#ModalPassword").modal("show") }, 2000);</script>';
        }else{
            $x['passOTP']= '<script>setTimeout(function(){ $("#").modal("toggle") }, 2000);</script>';

        }
        if($userRatingToken == 'norate'){
            $x['rating']= '<script>setTimeout(function(){ $("#ModalRating").modal("show") }, 2000);</script>';
        }else{
            $x['rating']= '<script>setTimeout(function(){ $("#").modal("show") }, 2000);</script>';
        }
        $this->load->view('user/profileUser_view',$x); 
    }
   
    function update_profile(){
        $kode=htmlspecialchars($this->input->post('xkode',TRUE),ENT_QUOTES);
        $nama=htmlspecialchars($this->input->post('xnama',TRUE),ENT_QUOTES);
        $telp=htmlspecialchars($this->input->post('xtelp',TRUE),ENT_QUOTES);
        $pass1=htmlspecialchars($this->input->post('xpass1',TRUE),ENT_QUOTES);
        $pass2=htmlspecialchars($this->input->post('xpass2',TRUE),ENT_QUOTES);

        if($pass1 == null || $pass2 == null){
            $this->m_user->update_profile_nopass($kode,$nama,$telp);
            echo $this->session->set_flashdata('msg','<div class="alert alert-success" role="alert"><button type="button" class="close" data-dismiss="alert"><i class="fa fa-close"></i></button> Sukses! Profil anda berhasil diperbarui</div>');
            redirect('user/profile');
        }else{
            if($pass2 == $pass1){
                $this->m_user->update_profile($kode,$nama,$telp, $pass1);
                echo $this->session->set_flashdata('msg','<div class="alert alert-success" role="alert"><button type="button" class="close" data-dismiss="alert"><i class="fa fa-close"></i></button> Sukses! Profil anda berhasil diperbarui</div>');
                redirect('user/profile');
            }else{
                echo $this->session->set_flashdata('msg','<div class="alert alert-danger" role="alert"><button type="button" class="close" data-dismiss="alert"><i class="fa fa-close"></i></button> Password konfirmasi harus sama! silahkan coba lagi.</div>');
                redirect('user/profile');
            }
        }  
    }

    function add_address(){
        $idAddress = rand(123456,999999);
        $nama=htmlspecialchars($this->input->post('xnama',TRUE),ENT_QUOTES);
        $alamat=htmlspecialchars($this->input->post('xalamat',TRUE),ENT_QUOTES);
        $kontak=htmlspecialchars($this->input->post('xkontak',TRUE),ENT_QUOTES);

        $this->m_address->add_address($idAddress,$nama,$alamat,$kontak);
        echo $this->session->set_flashdata('msg','<div class="alert alert-success" role="alert"><button type="button" class="close" data-dismiss="alert"><i class="fa fa-close"></i></button> Sukses! Anda berhasil menyimpan alamat '.$nama.'</div>');
        redirect('user/profile');
    }

    function add_address_order(){
        $idAddress = rand(123456,999999);
        $nama=htmlspecialchars($this->input->post('xnama',TRUE),ENT_QUOTES);
        $alamat=htmlspecialchars($this->input->post('xalamat',TRUE),ENT_QUOTES);
        $kontak=htmlspecialchars($this->input->post('xkontak',TRUE),ENT_QUOTES);

        $this->m_address->add_address($idAddress,$nama,$alamat,$kontak);
        echo $this->session->set_flashdata('msg','<div class="alert alert-info" role="alert"><button type="button" class="close" data-dismiss="alert"><i class="fa fa-close"></i></button> Sukses! Anda berhasil menyimpan alamat '.$nama.'</div>');
        redirect('user/order');
    }

    function update_address(){
        $kode=htmlspecialchars($this->input->post('kode',TRUE),ENT_QUOTES);
        $nama=htmlspecialchars($this->input->post('xnama',TRUE),ENT_QUOTES);
        $alamat=htmlspecialchars($this->input->post('xalamat',TRUE),ENT_QUOTES);
        $this->m_address->update_address($kode,$nama,$alamat);
        echo $this->session->set_flashdata('msg','<div class="alert alert-success" role="alert"><button type="button" class="close" data-dismiss="alert"><i class="fa fa-close"></i></button> Sukses! Alamat '.$nama.' berhasil diperbarui</div>');
        redirect('user/profile');
    }

    function ubah_password(){
        $userId=htmlspecialchars($this->input->post('xuserId',TRUE),ENT_QUOTES);
        $pass1=htmlspecialchars($this->input->post('password1',TRUE),ENT_QUOTES);
        $pass2=htmlspecialchars($this->input->post('password2',TRUE),ENT_QUOTES);

        if($pass1 != $pass2){
            echo $this->session->set_flashdata('msg','<div class="alert alert-danger" role="alert"><button type="button" class="close" data-dismiss="alert"><i class="fa fa-close"></i></button>Gagal! Pastikan konfirmasi password yang sama</div>');
            redirect('user/profile');
        }else{
            $this->m_user->ubah_password($userId,$pass1);
            $this->session->set_userdata('passOTP', '');
            echo $this->session->set_flashdata('msg','<div class="alert alert-success" role="alert"><button type="button" class="close" data-dismiss="alert"><i class="fa fa-close"></i></button> Sukses! Password berhasil diperbarui</div>');
            redirect('user/profile');
        }
        
    }

    function add_rating(){
        $nama = $this->session->userdata('nama');
        $rating = htmlspecialchars($this->input->post('rating',TRUE),ENT_QUOTES);
        $saran = htmlspecialchars($this->input->post('saran',TRUE),ENT_QUOTES);

        $data=array(
			'rating_user' => $nama,
			'rating_amount' => $rating,
			'rating_saran' => $saran
		);
        $this->db->insert('rating',$data);
        $this->session->set_userdata('rating', 'rated');
		echo $this->session->set_flashdata("msg","<div class='alert alert-info'>Terima kasih untuk penilaian anda, penilaian anda sangat membantu kami.</div>");
		redirect('user/profile');

    }

}