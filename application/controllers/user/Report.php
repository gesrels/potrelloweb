<?php
class report extends CI_Controller{
	
	function __construct(){
		parent::__construct();
        $this->load->model('M_pengunjung','m_pengunjung');
        $this->load->model('M_order','m_order');

        $this->m_pengunjung->count_visitor();
        $this->load->library('upload');
	}

	function index(){
        //$x['data']=$this->m_fasilities->get_all_falilitas();
        $x['order']=$this->m_order->get_order_byid_profile();
        $this->session->set_userdata('isIntro', '');
		$this->load->view('user/report_view',$x);
	}

	function kirim(){
		$nama=htmlspecialchars($this->input->post('name',TRUE),ENT_QUOTES);
		$email=htmlspecialchars($this->input->post('email',TRUE),ENT_QUOTES);
		$subject=htmlspecialchars($this->input->post('subject',TRUE),ENT_QUOTES);
        $pesan=htmlspecialchars($this->input->post('message',TRUE),ENT_QUOTES);
        
        $config = Array(
                        'protocol' => 'smtp',
                        'smtp_host' => 'ssl://mail.potrello.id',
                        'smtp_port' => 465,
                        'smtp_user' => 'report@potrello.id',
                        'smtp_pass' => 'potrello08212322',
                        'mailtype'  => 'html',
                        'charset' => 'utf-8',);
                        $this->load->library('email');
                        $this->email->initialize($config);
                        $this->email->from('report@potrello.id','Potrello New Payment Report');
                        $this->email->to('ellora.latif@gmail.com');
                        $this->email->subject("Laporan Pembayaran Baru");
                        $this->email->message("
                        <html>
                            <head>
                                <title>Laporan Pembayaran Baru!</title>
                            </head>
                            <body>
                            <h2>Laporan Masuk!</h2>
                            <p>Dear <b>ADMIN</b> Silahkan masuk ke akun admin potrello terdapat 1 laporan pembayaran baru!!</br></br>
                            <p>https://potrello.id/administrator</p>
                            <p>This email is sent automatically sent through the system, it is expected not to reply to this email, for further questions, please contact our customer service, Thankyou</p>
                            </body>
                        </html>
                        ");
        if($this->email->send()){
          $config['upload_path'] = './assets/images/'; //path folder
            $config['allowed_types'] = 'jpg|png|jpeg|bmp'; //type yang dapat diakses bisa anda sesuaikan
            $config['encrypt_name'] = TRUE; //nama yang terupload nantinya
            $this->upload->initialize($config);
    
            if ($this->upload->do_upload('userfile'))
            {     
                $fileData = $this->upload->data();
                    $gambar = $fileData['file_name'];
                    $data=array(
                        'inbox_nama' => $nama,
                        'inbox_email' => $email,
                        'inbox_subject' => $subject,
                        'inbox_pesan' => $pesan,
                        'inbox_img' => $gambar
                    );
                    $this->db->insert('inbox',$data);
                    echo $this->session->set_flashdata('msg','<div class="alert alert-info" role="alert"><button type="button" class="close" data-dismiss="alert"><i class="fa fa-close"></i></button> Terimakasih! laporan terkirim, kami akan segera menghubungi anda</div>');
                    redirect('user/profile');
            }else{
                echo $this->session->set_flashdata('msg','<div class="alert alert-warning" role="alert"><button type="button" class="close" data-dismiss="alert"><i class="fa fa-close"></i></button>Oh snap! your image is not qualified!</div>');
                redirect('user/profile');
            }  
        }else{
            echo $this->session->set_flashdata('msg','<div class="alert alert-warning" role="alert"><button type="button" class="close" data-dismiss="alert"><i class="fa fa-close"></i></button>Gagal! Pastikan koneksi internet anda stabil!</div>');
                redirect('user/profile');
        }
        
	}
}