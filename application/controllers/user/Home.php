<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends CI_Controller {
	function __construct(){
        parent::__construct();
        if($this->session->userdata('masuk') !=TRUE){
            $url=base_url('login');
            redirect($url);
        };
		$this->load->model('M_pengunjung','m_pengunjung');
		$this->load->model('M_slider','m_slider');
		$this->load->model('M_tulisan','m_tulisan');
		$this->load->model('M_room','m_room');
        $this->m_pengunjung->count_visitor();
	}

	public function index(){
		$x['slider']=$this->m_slider->get_all_slider();
		$x['rooms']=$this->m_room->get_all_room_home();
		$x['blog']=$this->m_tulisan->get_blog_home();
		$x['testi']=$this->db->query("SELECT * FROM fasilitas limit 4");

		$isIntro = $this->session->userdata('isIntro');
		
        if($isIntro === 'RegIntro'){
            $x['regIntro']= '<script>setTimeout(function(){ $("#ModalIntro").modal("show") }, 2000);</script>';
        }else{
            $x['regIntro']= '<script>setTimeout(function(){ $("#").modal("show") }, 2000);</script>';
        }
		$this->load->view('user/homeUser_view',$x);
		
	}

}
