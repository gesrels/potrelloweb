<?php
class OrderRegister extends CI_Controller{
	
	function __construct(){
		parent::__construct();
        $this->load->model('M_pengunjung','m_pengunjung');
        $this->load->model('M_room','m_room');
        $this->load->model('M_order','m_order');
        $this->load->model('M_address','m_address');
        $this->load->model('M_courier','m_courier');
        $this->load->model('M_user','m_user');
        $this->load->model('M_size','m_size');

        $this->m_pengunjung->count_visitor();
        $this->load->library('upload');
	}

	function index(){
        $x['service']=$this->m_room->get_active_room();
        $x['courier']=$this->m_courier->get_all_courier();
        $x['size']=$this->m_size->get_all_size();
		$this->load->view('frontend/orderRegister_view',$x);
	}
	
	function verifyEmail(){
		$this->load->view('frontend/emailverify_order_view');
	}
	
	function check_otp(){
		$kodeOTP=htmlspecialchars($this->input->post('xotp',TRUE),ENT_QUOTES);
		$checkedOTP = $this->m_user->check_otp($kodeOTP);

		if($checkedOTP === TRUE){
			$this->m_user->update_status_user($kodeOTP);
			echo $this->session->set_flashdata("msg","<div class='alert alert-info'>Sukses, Pesanan terkirim, Registrasi berhasil silahkan login akun</div>");
			redirect('login');
		}else{
			echo $this->session->set_flashdata("msg","<div class='alert alert-danger'>Gagal, Kode OTP tidak sesuai</div>");
			redirect('orderRegister/verifyEmail');
		}
	}

	function order_register(){
        $service=strip_tags($this->input->post('xservice'));
        $size=strip_tags($this->input->post('xsize'));
        $text=strip_tags($this->input->post('xcalligraphy'));
        $courier=strip_tags($this->input->post('xcourier'));
        $nama=htmlspecialchars($this->input->post('xname',TRUE),ENT_QUOTES);
		$gender=htmlspecialchars($this->input->post('xgender',TRUE),ENT_QUOTES);
        $email=htmlspecialchars($this->input->post('xemail',TRUE),ENT_QUOTES);
        $telp=htmlspecialchars($this->input->post('xtelp',TRUE),ENT_QUOTES);
        $deskripsi=htmlspecialchars($this->input->post('xdeskripsi',TRUE),ENT_QUOTES);
        $nama_alamat=htmlspecialchars($this->input->post('xnamaAlamat',TRUE),ENT_QUOTES);
        $alamat=htmlspecialchars($this->input->post('xalamat',TRUE),ENT_QUOTES);
        $kontak=htmlspecialchars($this->input->post('xkontak',TRUE),ENT_QUOTES);


        $config['upload_path'] = './assets/images/'; //path folder
        $config['allowed_types'] = 'jpg|png|jpeg|bmp'; //type yang dapat diakses bisa anda sesuaikan
        $config['encrypt_name'] = TRUE; //nama yang terupload nantinya

        $this->upload->initialize($config);
        
		$checkEmail = $this->m_user->checkEmail($email);
		if($checkEmail === TRUE){
			echo $this->session->set_flashdata("msg","<div class='alert alert-danger'>Email has been taken, try another email</div>");
			redirect('OrderRegister');
		}else{
            if ($this->upload->do_upload('userfile'))
            {
                    $fileData = $this->upload->data();
                    $gambar = $fileData['file_name'];
                    $idAddress = rand(123456,999999);
                    $userid=rand(123456,999999);
                    $orderNumber=rand(123456,99999);
                    $otp=rand(123456,999999);
                    $password1 = $otp;
                    
                    $config = Array(
                        'protocol' => 'smtp',
                        'smtp_host' => 'ssl://mail.potrello.id',
                        'smtp_port' => 465,
                        'smtp_user' => 'otpcode.email@potrello.id',
                        'smtp_pass' => 'potrello08212322',
                        'mailtype'  => 'html',
                        'charset' => 'utf-8',);
                        $this->load->library('email');
                        $this->email->initialize($config);
                        $this->email->from('otpcode.email@potrello.id','Potrello');
                        $this->email->to($email);
                        $this->email->subject("Verifikasi Email");
                        $this->email->message("
                        <html>
                            <head>
                                <title>Verifikasi Email Potrello</title>
                            </head>
                            <body>
                            <h2>HATI HATI PENIPUAN, JANGAN SEBARKAN KODE OTP KE SIAPAPUN</h2>
                            <p>Dear <b>$nama</b> berikut adalah kode OTP anda : <b>$otp</b> </p></br></br>
                            <p>Gunakan kode OTP sebagai password anda untuk LOGIN</p>
                            <p>This email is sent automatically sent through the system, it is expected not to reply to this email, for further questions, please contact our customer service, Thankyou</p>
                            </body>
                        </html>
                        ");
                    if($this->email->send()){
                        $config = Array(
                        'protocol' => 'smtp',
                        'smtp_host' => 'ssl://mail.potrello.id',
                        'smtp_port' => 465,
                        'smtp_user' => 'neworder@potrello.id',
                        'smtp_pass' => 'potrello08212322',
                        'mailtype'  => 'html',
                        'charset' => 'utf-8',);
                        $this->load->library('email');
                        $this->email->initialize($config);
                        $this->email->from('neworder@potrello.id','Potrello New Order');
                        $this->email->to('ellora.latif@gmail.com');
                        $this->email->subject("Pesanan Masuk");
                        $this->email->message("
                        <html>
                            <head>
                                <title>Pesanan Masuk!</title>
                            </head>
                            <body>
                            <h2>Pesanan Masuk!</h2>
                            <p>Dear <b>ADMIN</b> Silahkan masuk ke akun admin potrello terdapat 1 order baru!!</br></br>
                            <p>https://potrello.id/administrator</p>
                            <p>This email is sent automatically sent through the system, it is expected not to reply to this email, for further questions, please contact our customer service, Thankyou</p>
                            </body>
                        </html>
                        ");
                        $this->email->send();
                        $data=array(
                            'user_id' => $userid,
                            'user_nama' => $nama,
                            'user_gender' => $gender,
                            'user_email' => $email,
                            'user_telp' => $telp,
                            'user_password' => md5($password1),
                            'user_otp' => $otp
                        );
                        $this->db->insert('user',$data);
                        $this->m_address->add_address_register($idAddress,$userid,$nama_alamat, $alamat,$kontak);
                        $this->m_order->simpan_order_register($orderNumber,$userid,$service, $size, $gambar ,$text, $deskripsi,$courier, $idAddress);
                        $OtpCode=array(
                            'register_user_email' => $email,
                            'register_otp_code' => $otp
                        );                  
                        $this->db->insert('register',$OtpCode);
                        $this->session->set_userdata('email', $email);
                        echo $this->session->set_flashdata("msg","<div class='alert alert-info'>Kode OTP dikirimkan ke email anda.</div>");
                        redirect('orderRegister/verifyEmail');
                    }else{
                        echo $this->session->set_flashdata("msg","<div class='alert alert-info'>Kirim OTP Gagal! Pastikan koneksi internet anda stabil</div>");
					    redirect('orderRegister');
                    }          
            }else{
                echo $this->session->set_flashdata('msg','<div class="alert alert-warning" role="alert"><button type="button" class="close" data-dismiss="alert"><i class="fa fa-close"></i></button>Oh snap! your image is not qualified!</div>');
                redirect('OrderRegister');
            }

        }

	}
	function kirim_ulangOTP(){
		$email=htmlspecialchars($this->input->post('xemail',TRUE),ENT_QUOTES);

				$otp=rand(123456,999999);

				$config = Array(
					'protocol' => 'smtp',
					'smtp_host' => 'ssl://mail.potrello.id',
					'smtp_port' => 465,
					'smtp_user' => 'otpcode.email@potrello.id',
					'smtp_pass' => 'potrello08212322',
					'mailtype'  => 'html',
					'charset' => 'utf-8',);
					$this->load->library('email');
					$this->email->initialize($config);
					$this->email->from('otpcode.email@potrello.id','Potrello');
					$this->email->to($email);
					$this->email->subject("Verifikasi Email Kode OTP Potrello");
					$this->email->message("
					<html>
						<head>
							<title>Verifikasi Email Potrello</title>
						</head>
						<body>
						<h2>HATI HATI PENIPUAN, JANGAN SEBARKAN KODE OTP KE SIAPAPUN</h2>
						<p>Dear <b>$email</b> berikut adalah kode OTP anda : <b>$otp</b> </p>
						<p>gunakan kode OTP sebagai Password anda</p>

						<p>This email is sent automatically sent through the system, it is expected not to reply to this email, for further questions, please contact our customer service, Thankyou</p>
						</body>
					</html>
					");

				if($this->email->send()){
					$data = array(
						'user_otp' => $otp,
						'user_password' => md5($otp)
					);	
					$this->db->where('user_email', $email);
					$this->db->update('user', $data);
			
					$OtpCode=array(
						'register_user_email' => $email,
						'register_otp_code' => $otp);
					$this->db->insert('register',$OtpCode);
					
					
					$this->session->set_userdata('email', $email);
					echo $this->session->set_flashdata("msg","<div class='alert alert-info'>Kode OTP dikirimkan ke email anda</div>");
					redirect('orderRegister/verifyEmail');
				}else{	
					echo $this->session->set_flashdata("msg","<div class='alert alert-info'>Kirim OTP Gagal! Pastikan koneksi internet anda stabil</div>");
					redirect('login');
				
				}
		}
}